/*
 *  File: /pubSub.ts
 *  Project: api
 *  File Created: 19-01-2024 11:36:19
 *  Author: Clay Risser
 *  -----
 *  BitSpur (c) Copyright 2024
 *
 *  Licensed under the GNU Affero General Public License (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://www.gnu.org/licenses/agpl-3.0.en.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *  You can be released from the requirements of the license by purchasing
 *  a commercial license. Buying such a license is mandatory as soon as you
 *  develop commercial activities involving this software without disclosing
 *  the source code of your own applications.
 */

import { CronJobPayload, DockerEvent, DockerServiceExecPayload, JobCompletedPayload } from './docker';
import { createPubSub } from '@graphql-yoga/subscription';

export const CRON_JOB = 'CRON_JOB';
export const DOCKER_EVENTS = 'DOCKER_EVENTS';
export const DOCKER_SERVICE_EXEC = 'DOCKER_SERVICE_EXEC';
export const JOB_COMPLETED = 'JOB_COMPLETED';

export const pubSub = createPubSub<{
  CRON_JOB: [CronJobPayload];
  DOCKER_EVENTS: [DockerEvent];
  DOCKER_SERVICE_EXEC: [DockerServiceExecPayload];
  JOB_COMPLETED: [JobCompletedPayload];
}>();
