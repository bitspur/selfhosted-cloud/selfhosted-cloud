/*
 *  File: /docker/dockerService.ts
 *  Project: api
 *  File Created: 24-02-2024 09:27:50
 *  Author: Clay Risser
 *  -----
 *  BitSpur (c) Copyright 2024
 *
 *  Licensed under the GNU Affero General Public License (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://www.gnu.org/licenses/agpl-3.0.en.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *  You can be released from the requirements of the license by purchasing
 *  a commercial license. Buying such a license is mandatory as soon as you
 *  develop commercial activities involving this software without disclosing
 *  the source code of your own applications.
 */

import kebabCase from 'lodash.kebabcase';
import { DockerContainerCreateArgs, DockerContainerCreateResult, DockerContainerStatsResult } from './container';
import { ExecaError, execa } from 'execa';
import { Flag } from './types';
import { GraphQLError } from 'graphql';
import { Logger, Injectable } from '@multiplatform.one/typegraphql';
import { camelCaseKeys } from './utils';
import { DOCKER_SERVICE_EXEC, pubSub } from '../pubSub';
import {
  DockerAttachArgs,
  DockerAttachResult,
  DockerBuildArgs,
  DockerBuildResult,
  DockerCommitArgs,
  DockerCpArgs,
  DockerCpResult,
  DockerCreateArgs,
  DockerCreateResult,
  DockerDiffArgs,
  DockerDiffResult,
  DockerEventsArgs,
  DockerEventsResult,
  DockerExecArgs,
  DockerExecResult,
  DockerExportArgs,
  DockerExportResult,
  DockerHistoryArgs,
  DockerHistoryResult,
  DockerImagesArgs,
  DockerImagesResult,
  DockerImportArgs,
  DockerImportResult,
  DockerInfoResult,
  DockerInspectArgs,
  DockerInspectResult,
  DockerKillArgs,
  DockerKillResult,
  DockerLoadArgs,
  DockerLoadResult,
  DockerLoginArgs,
  DockerLoginResult,
  DockerLogoutArgs,
  DockerLogoutResult,
  DockerLogsArgs,
  DockerLogsResult,
  DockerPauseArgs,
  DockerPauseResult,
  DockerPortArgs,
  DockerPortResult,
  DockerPsArgs,
  DockerPsResult,
  DockerPullArgs,
  DockerPullResult,
  DockerRenameArgs,
  DockerRenameResult,
  DockerRestartArgs,
  DockerRestartResult,
  DockerRmArgs,
  DockerRmResult,
  DockerRmiArgs,
  DockerRmiResult,
  DockerSaveArgs,
  DockerSaveResult,
  DockerSearchArgs,
  DockerSearchResult,
  DockerStartArgs,
  DockerStartResult,
  DockerStatsArgs,
  DockerStopArgs,
  DockerStopResult,
  DockerTagArgs,
  DockerTagResult,
  DockerTopArgs,
  DockerTopResult,
  DockerUnpauseArgs,
  DockerUnpauseResult,
  DockerUpdateArgs,
  DockerUpdateResult,
  DockerVersionResult,
  DockerWaitArgs,
  DockerWaitResult,
} from './dto';

@Injectable()
export class DockerService {
  constructor(private readonly logger: Logger) {}

  async create(args: DockerCreateArgs): Promise<DockerCreateResult> {
    const { stderr, stdout } = await this.docker('create', {
      addHost: args.addHost,
      annotation: args.annotation,
      attach: args.attach,
      blkioWeight: args.blkioWeight,
      blkioWeightDevice: args.blkioWeightDevice,
      capAdd: args.capAdd,
      capDrop: args.capDrop,
      cgroupParent: args.cgroupParent,
      cgroupns: args.cgroupns,
      cidfile: args.cidfile,
      cpuPeriod: args.cpuPeriod,
      cpuQuota: args.cpuQuota,
      cpuRtPeriod: args.cpuRtPeriod,
      cpuRtRuntime: args.cpuRtRuntime,
      cpuShares: args.cpuShares,
      cpus: args.cpus,
      cpusetCpus: args.cpusetCpus,
      cpusetMems: args.cpusetMems,
      device: args.device,
      deviceCgroupRule: args.deviceCgroupRule,
      deviceReadBps: args.deviceReadBps,
      deviceReadIops: args.deviceReadIops,
      deviceWriteBps: args.deviceWriteBps,
      deviceWriteIops: args.deviceWriteIops,
      disableContentTrust: args.disableContentTrust,
      dns: args.dns,
      dnsOption: args.dnsOption,
      dnsSearch: args.dnsSearch,
      domainname: args.domainname,
      entrypoint: args.entrypoint,
      env: args.env,
      envFile: args.envFile,
      expose: args.expose,
      groupAdd: args.groupAdd,
      healthCmd: args.healthCmd,
      healthInterval: args.healthInterval,
      healthRetries: args.healthRetries,
      healthStartPeriod: args.healthStartPeriod,
      healthTimeout: args.healthTimeout,
      help: args.help,
      hostname: args.hostname,
      init: args.init,
      interactive: args.interactive,
      ip: args.ip,
      ipc: args.ipc,
      isolation: args.isolation,
      kernelMemory: args.kernelMemory,
      label: args.label,
      labelFile: args.labelFile,
      link: args.link,
      linkLocalIp: args.linkLocalIp,
      logDriver: args.logDriver,
      logOpt: args.logOpt,
      macAddress: args.macAddress,
      memory: args.memory,
      memoryReservation: args.memoryReservation,
      memorySwap: args.memorySwap,
      memorySwappiness: args.memorySwappiness,
      mount: args.mount,
      name: args.name,
      network: args.network,
      networkAlias: args.networkAlias,
      noHealthcheck: args.noHealthcheck,
      oomKillDisable: args.oomKillDisable,
      oomScoreAdj: args.oomScoreAdj,
      pid: args.pid,
      pidsLimit: args.pidsLimit,
      platform: args.platform,
      privileged: args.privileged,
      publish: args.publish,
      publishAll: args.publishAll,
      pull: args.pull,
      readOnly: args.readOnly,
      restart: args.restart,
      rm: args.rm,
      runtime: args.runtime,
      securityOpt: args.securityOpt,
      shmSize: args.shmSize,
      stopSignal: args.stopSignal,
      stopTimeout: args.stopTimeout,
      storageOpt: args.storageOpt,
      sysctl: args.sysctl,
      tmpfs: args.tmpfs,
      tty: args.tty,
      ulimit: args.ulimit,
      user: args.user,
      userns: args.userns,
      uts: args.uts,
      volume: args.volume,
      volumeDriver: args.volumeDriver,
      volumesFrom: args.volumesFrom,
      workdir: args.workdir,
    });
    if (stderr) throw new GraphQLError(stderr);
    return {
      stdout: stdout,
      stderr: stderr,
    };
  }

  async info(): Promise<[DockerInfoResult]> {
    const { stderr, stdout } = await this.docker('info', { format: '{{json .}}' });
    if (stderr) throw new GraphQLError(stderr);

    const res = camelCaseKeys(JSON.parse(`[${stdout.split('\n').join(',')}]`));
    return res;
  }

  async build(args: DockerBuildArgs): Promise<DockerBuildResult> {
    const { stderr, stdout } = await this.docker(
      'build',
      {
        ...args.options,
      },
      [args.source],
    );
    if (stderr) throw new GraphQLError(stderr);
    return {
      stdout: stdout,
      stderr: stderr,
    };
  }

  async import(args: DockerImportArgs): Promise<DockerImportResult> {
    const { stderr, stdout } = await this.docker(
      'import',
      {
        change: args.change,
        message: args.message,
      },
      [args.source, args.repository],
    );
    if (stderr) throw new GraphQLError(stderr);
    return {
      stdout: stdout,
      stderr: stderr,
    };
  }

  async load(args: DockerLoadArgs): Promise<DockerLoadResult> {
    const { stderr, stdout } = await this.docker(
      'load',
      {
        input: args.input,
      },
      [args.input],
    );
    if (stderr) throw new GraphQLError(stderr);
    return {
      stdout: stdout,
      stderr: stderr,
    };
  }

  async update(args: DockerUpdateArgs): Promise<DockerUpdateResult> {
    const { stderr, stdout } = await this.docker('update', {
      blkioWeight: args.blkioWeight,
      cpuPeriod: args.cpuPeriod,
      cpuQuota: args.cpuQuota,
      cpuRtPeriod: args.cpuRtPeriod,
      cpuRtRuntime: args.cpuRtRuntime,
      cpuShares: args.cpuShares,
      cpus: args.cpus,
      cpusetCpus: args.cpusetCpus,
      cpusetMems: args.cpusetMems,
      memory: args.memory,
      memoryReservation: args.memoryReservation,
      memorySwap: args.memorySwap,
      pidsLimit: args.pidsLimit,
      restart: args.restart,
    });
    if (stderr) throw new GraphQLError(stderr);
    return {
      stdout: stdout,
      stderr: stderr,
    };
  }

  async save(args: DockerSaveArgs): Promise<DockerSaveResult> {
    const { stderr, stdout } = await this.docker('save', {
      output: args.output,
    });
    if (stderr) throw new GraphQLError(stderr);
    return {
      stdout: stdout,
      stderr: stderr,
    };
  }

  async top(args: DockerTopArgs): Promise<DockerTopResult[]> {
    const { stderr, stdout } = await this.docker('top', {}, [args.container]);
    if (stderr) throw new GraphQLError(stderr);
    return stdout
      .split('\n')
      .slice(1)
      .map((line) => {
        const [uId, pId, ppId, c, sTime, tty, time, cmd] = line.split(/\s{2,}/).map((part) => part.trim());
        return {
          uId,
          pId,
          ppId,
          c,
          sTime,
          tty,
          time,
          cmd,
        } as DockerTopResult;
      });
  }

  async tag(args: DockerTagArgs): Promise<DockerTagResult> {
    const { stderr, stdout } = await this.docker('tag', {}, [args.source, args.target]);
    if (stderr) throw new GraphQLError(stderr);
    return {
      stdout: stdout,
      stderr: stderr,
    };
  }

  async run(args: DockerContainerCreateArgs): Promise<DockerContainerCreateResult> {
    const { stderr, stdout } = await this.docker(
      'run',
      {
        ...args.options,
        detach: args.options?.detach ? args.options.detach : true,
      },
      [args.image, args.command, ...(args.args || [])],
    );
    if (stderr) throw new GraphQLError(stderr);
    return {
      id: stdout,
    };
  }

  async start(args: DockerStartArgs): Promise<DockerStartResult> {
    const { stderr, stdout } = await this.docker(
      'start',
      {
        attach: args.attach,
        detachKeys: args.detachKeys,
        interactive: args.interactive,
      },
      [args.container],
    );
    if (stderr) throw new GraphQLError(stderr);
    return {
      stdout: stdout,
      stderr: stderr,
    };
  }

  async stop(args: DockerStopArgs): Promise<DockerStopResult> {
    const { stderr, stdout } = await this.docker(
      'stop',
      {
        ...args.options,
      },
      args.containers,
    );
    if (stderr) throw new GraphQLError(stderr);
    return {
      containers: stdout
        .trim()
        .split('\n')
        .filter((c) => c),
    };
  }

  async kill(args: DockerKillArgs): Promise<DockerKillResult> {
    const { stderr, stdout } = await this.docker('kill', {
      signal: args.signal,
    });
    if (stderr) throw new GraphQLError(stderr);
    return {
      stdout: stdout,
      stderr: stderr,
    };
  }

  async search(args: DockerSearchArgs): Promise<DockerSearchResult[]> {
    const { stderr, stdout } = await this.docker(
      'search',
      {
        filter: args.filter,
        format: args.format,
        limit: args.limit,
        noTrunc: args.noTrunc,
        orderBy: args.orderBy,
        stars: args.stars,
      },
      [args.term],
    );
    if (stderr) throw new GraphQLError(stderr);
    return stdout
      .trim()
      .split('\n')
      .slice(1)
      .map((line) => {
        const [name, description, stars, official] = line.split(/\s{2,}/);
        return {
          name,
          description,
          starCount: stars,
          isOfficial: official === '[OK]',
        } as DockerSearchResult;
      });
  }

  async attach(args: DockerAttachArgs): Promise<DockerAttachResult> {
    const { stderr, stdout } = await this.docker(
      'attach',
      {
        detachKeys: args.detachKeys,
        noStdin: args.noStdin,
        sigProxy: args.sigProxy,
      },
      [args.nameOrId],
    );
    if (stderr) throw new GraphQLError(stderr);
    return {
      stdout: stdout,
      stderr: stderr,
    };
  }

  async stats(args: DockerStatsArgs): Promise<DockerContainerStatsResult[]> {
    const { stderr, stdout } = await this.docker(
      'stats',
      {
        all: args.all,
        format: '{{json .}}',
        noStream: args.noStream ? args.noStream : true,
        noTrunc: args.noTrunc,
      },
      [args.container],
    );
    if (stderr) throw new GraphQLError(stderr);
    return camelCaseKeys(JSON.parse(`[${stdout.split('\n').join(',')}]`));
  }

  async inspect(args: DockerInspectArgs): Promise<DockerInspectResult> {
    const { stderr, stdout } = await this.docker(
      'inspect',
      {
        format: '{{json .}}',
        size: args.size,
      },
      [args.nameOrId],
    );
    if (stderr) throw new GraphQLError(stderr);
    return camelCaseKeys(JSON.parse(stdout));
  }

  async ps(args: DockerPsArgs = {}): Promise<DockerPsResult[]> {
    const { stderr, stdout } = await this.docker('ps', {
      filter: args.filter,
      format: '{{json .}}',
      noResolve: args.noResolve,
      noTrunc: args.noTrunc,
      all: args.all,
      last: args.last,
      latest: args.latest,
      size: args.size,
    });
    if (stderr) throw new GraphQLError(stderr);
    return camelCaseKeys(JSON.parse(`[${stdout.split('\n').join(',')}]`));
  }

  async commit(args: DockerCommitArgs): Promise<string> {
    const { stderr, stdout } = await this.docker(
      'commit',
      {
        ...args.options,
      },
      [args.container, args.repository],
    );

    if (stderr) throw new GraphQLError(stderr);
    return stdout;
  }

  async exec(
    args: DockerExecArgs & {
      serviceExecId?: string;
    },
  ): Promise<DockerExecResult> {
    const { stderr, stdout, exitCode } = await this.docker(
      'exec',
      {
        detach: args.detach,
        env: args.env,
        interactive: args.interactive,
        privileged: args.privileged,
        tty: args.tty,
        user: args.user,
        workdir: args.workdir,
      },
      [args.nameOrId, args.command, ...(args.args || [])],
    );
    if (args.serviceExecId) {
      pubSub.publish(DOCKER_SERVICE_EXEC, {
        containerId: args.nameOrId,
        exitCode,
        serviceExecId: args.serviceExecId,
        stderr,
        stdout,
      });
    }
    return {
      exitCode,
      stderr: stderr,
      stdout: stdout,
    };
  }

  async cp(args: DockerCpArgs): Promise<DockerCpResult> {
    const { stderr, stdout } = await this.docker(
      'cp',
      {
        ...args.options,
      },
      [args.source, args.destination],
    );
    if (stderr) throw new GraphQLError(stderr);
    return {
      stdout: stdout,
      stderr: stderr,
    };
  }

  async events(args: DockerEventsArgs): Promise<DockerEventsResult> {
    const { stderr, stdout } = await this.docker('events', {
      since: args.since,
      until: args.until,
      filter: args.filter,
      format: args.format,
    });
    if (stderr) throw new GraphQLError(stderr);
    return {
      stdout: stdout,
      stderr: stderr,
    };
  }

  async rm(args: DockerRmArgs): Promise<DockerRmResult> {
    const { stderr, stdout } = await this.docker(
      'rm',
      {
        force: args.force,
        link: args.link,
        volumes: args.volumes,
      },
      [args.nameOrId],
    );
    if (stderr) throw new GraphQLError(stderr);
    return {
      stdout,
      stderr,
    };
  }

  async rmi(args: DockerRmiArgs): Promise<DockerRmiResult> {
    const { stderr, stdout } = await this.docker(
      'rmi',
      {
        force: args.force,
        noPrune: args.noPrune,
      },
      [args.image],
    );
    if (stderr) throw new GraphQLError(stderr);
    return {
      stdout: stdout,
      stderr: stderr,
    };
  }

  async images(args: DockerImagesArgs): Promise<DockerImagesResult[]> {
    const { stderr, stdout } = await this.docker('images', {
      all: args.all,
      digest: args.digests,
      noTrunc: args.noTrunc,
      format: '{{json .}}',
    });
    if (stderr) throw new GraphQLError(stderr);
    return camelCaseKeys(JSON.parse(`[${stdout.split('\n').join(',')}]`));
  }

  async wait(args: DockerWaitArgs): Promise<DockerWaitResult> {
    const { stderr, stdout } = await this.docker('wait', {}, [args.nameOrId]);
    if (stderr) throw new GraphQLError(stderr);
    return {
      stdout: stdout,
      stderr: stderr,
      statusCode: stdout,
    };
  }

  async diff(args: DockerDiffArgs): Promise<DockerDiffResult[]> {
    const { stderr, stdout } = await this.docker('diff', {}, [args.container]);
    if (stderr) {
      throw new GraphQLError(stderr);
    }
    return stdout.split('\n').map((line) => {
      const [changeType, filePath] = line.split(' ');
      return { changeType, filePath } as DockerDiffResult;
    });
  }

  async export(args: DockerExportArgs): Promise<DockerExportResult> {
    const { stderr, stdout } = await this.docker(
      'export',
      {
        output: args.output,
      },
      [args.nameOrId],
    );
    if (stderr) throw new GraphQLError(stderr);
    return {
      stdout: stdout,
      stderr: stderr,
    };
  }

  async port(args: DockerPortArgs): Promise<DockerPortResult> {
    const { stderr, stdout } = await this.docker('port', {}, [args.nameOrId, args.port]);
    if (stderr) throw new GraphQLError(stderr);
    return {
      stdout: stdout,
      stderr: stderr,
    };
  }

  async version(): Promise<DockerVersionResult> {
    const { stderr, stdout } = await this.docker('version', {
      format: '{{json .}}',
    });
    if (stderr) throw new GraphQLError(stderr);
    return camelCaseKeys(JSON.parse(stdout));
  }

  async pull(args: DockerPullArgs): Promise<DockerPullResult> {
    const { stderr, stdout } = await this.docker(
      'pull',
      {
        allTags: args.allTags,
        disableContentTrust: args.disableContentTrust,
        platform: args.platform,
      },
      [args.name],
    );
    if (stderr) throw new GraphQLError(stderr);
    return {
      stdout: stdout,
      stderr: stderr,
    };
  }

  async push(args: DockerPullArgs): Promise<DockerPullResult> {
    const { stderr, stdout } = await this.docker(
      'push',
      {
        allTags: args.allTags,
        disableContentTrust: args.disableContentTrust,
      },
      [args.name],
    );
    if (stderr) throw new GraphQLError(stderr);
    return {
      stdout: stdout,
      stderr: stderr,
    };
  }

  async rename(args: DockerRenameArgs): Promise<DockerRenameResult> {
    const { stderr, stdout } = await this.docker('rename', {}, [args.nameOrId, args.newName]);
    if (stderr) throw new GraphQLError(stderr);
    return {
      stdout: stdout,
      stderr: stderr,
    };
  }

  async restart(args: DockerRestartArgs): Promise<DockerRestartResult> {
    const { stderr, stdout } = await this.docker('restart', {}, [args.nameOrId]);
    if (stderr) throw new GraphQLError(stderr);
    return {
      stdout: stdout,
      stderr: stderr,
    };
  }

  async login(args: DockerLoginArgs): Promise<DockerLoginResult> {
    const { stderr, stdout } = await this.docker(
      'login',
      {
        password: args.password,
        username: args.username,
      },
      [args.serverAddress],
    );
    if (stderr) throw new GraphQLError(stderr);
    return {
      stdout: stdout,
      stderr: stderr,
      status: stdout,
    };
  }

  async logout(args: DockerLogoutArgs): Promise<DockerLogoutResult> {
    const { stderr, stdout } = await this.docker('logout', {}, [args.serverAddress]);
    if (stderr) throw new GraphQLError(stderr);
    return {
      stdout: stdout,
      stderr: stderr,
      status: stdout,
    };
  }

  async pause(args: DockerPauseArgs): Promise<DockerPauseResult> {
    const { stderr, stdout } = await this.docker('pause', {}, args.containers);
    if (stderr) throw new GraphQLError(stderr);
    return {
      stdout: stdout,
      stderr: stderr,
      status: stdout,
    };
  }

  async unpause(args: DockerUnpauseArgs): Promise<DockerUnpauseResult> {
    const { stderr, stdout } = await this.docker('unpause', {}, [args.nameOrId]);
    if (stderr) throw new GraphQLError(stderr);
    return {
      stdout: stdout,
      stderr: stderr,
      status: stdout,
    };
  }

  async logs(args: DockerLogsArgs): Promise<DockerLogsResult> {
    const { stderr, stdout } = await this.docker(
      'logs',
      {
        details: args.details,
        follow: args.follow,
        since: args.since,
        tail: args.tail,
        timestamps: args.timestamps,
        until: args.until,
      },
      [args.nameOrId],
    );
    return {
      stdout: stdout,
      stderr: stderr,
    };
  }

  async history(args: DockerHistoryArgs): Promise<DockerHistoryResult[]> {
    const { stderr, stdout } = await this.docker(
      'history',
      {
        human: args.human,
        noTrunc: args.noTrunc,
        format: '{{json .}}',
      },
      [args.nameOrId],
    );
    if (stderr) throw new GraphQLError(stderr);
    return camelCaseKeys(JSON.parse(`[${stdout.split('\n').join(',')}]`));
  }

  private async docker(
    command: string,
    options: Record<string, Flag> = {},
    args: (string | undefined)[] = [],
  ): Promise<DockerResult> {
    try {
      const p = execa('docker', [
        command,
        ...Object.entries(options).reduce<string[]>((acc, [key, value]: [string, Flag]) => {
          if (value === true) {
            acc.push(`--${kebabCase(key)}`);
          } else if (typeof value === 'string' || typeof value === 'number') {
            acc.push(`--${kebabCase(key)}`, value.toString());
          } else if (Array.isArray(value)) {
            value.forEach((v) => {
              acc.push(`--${kebabCase(key)}`, v.toString());
            });
          }
          return acc;
        }, []),
        ...((args || []).filter(Boolean) as string[]),
      ]);
      if (process.env.DEBUG !== '0') {
        p.stdout?.pipe(process.stdout);
        p.stderr?.pipe(process.stderr);
      }
      const proc = await p;
      return {
        exitCode: proc.exitCode,
        stderr: proc.stderr,
        stdout: proc.stdout,
      };
    } catch (err) {
      const error = err as ExecaError;
      if (error.exitCode) {
        return {
          exitCode: error.exitCode,
          stderr: error.stderr,
          stdout: error.stdout,
        };
      }
      throw err;
    }
  }
}

export interface DockerResult {
  exitCode: number;
  stderr: string;
  stdout: string;
  [key: string]: any;
}
