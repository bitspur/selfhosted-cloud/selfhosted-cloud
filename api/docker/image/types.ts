/*
 *  File: /docker/image/types.ts
 *  Project: api
 *  File Created: 24-02-2024 09:27:50
 *  Author: Clay Risser
 *  -----
 *  BitSpur (c) Copyright 2024
 *
 *  Licensed under the GNU Affero General Public License (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://www.gnu.org/licenses/agpl-3.0.en.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *  You can be released from the requirements of the license by purchasing
 *  a commercial license. Buying such a license is mandatory as soon as you
 *  develop commercial activities involving this software without disclosing
 *  the source code of your own applications.
 */

export interface IDockerImageLs {
  Containers?: string;
  CreatedAt?: string;
  CreatedSince?: string;
  Digest?: string;
  Id?: string;
  Repository?: string;
  SharedSize?: string;
  Size?: string;
  Tag?: string;
  UniqueSize?: string;
  VirtualSize?: string;
}

export interface IDockerImageInspect {
  Id: string;
  RepoTags: string[];
  RepoDigests: string[];
  Parent: string;
  Comment: string;
  Created: string;
  Container: string;
  ContainerConfig: {
    Hostname: string;
    Domainname: string;
    User: string;
    AttachStdin: boolean;
    AttachStdout: boolean;
    AttachStderr: boolean;
    Tty: boolean;
    OpenStdin: boolean;
    StdinOnce: boolean;
    Env: string[] | null;
    Cmd: string[] | null;
    Image: string;
    Volumes: any | null;
    WorkingDir: string;
    Entrypoint: string[] | null;
    OnBuild: any | null;
    Labels: any | null;
  };
  DockerVersion: string;
  Author: string;
  Config: {
    Hostname: string;
    Domainname: string;
    User: string;
    AttachStdin: boolean;
    AttachStdout: boolean;
    AttachStderr: boolean;
    ExposedPorts: {
      [Key: string]: any;
    };
    Tty: boolean;
    OpenStdin: boolean;
    StdinOnce: boolean;
    Env: string[];
    Cmd: string[];
    ArgsEscaped: boolean;
    Image: string;
    Volumes: {
      [Key: string]: any;
    };
    WorkingDir: string;
    Entrypoint: string[];
    OnBuild: any | null;
    Labels: any | null;
  };
  Architecture: string;
  Os: string;
  Size: number;
  GraphDriver: {
    Data: {
      LowerDir: string;
      MergedDir: string;
      UpperDir: string;
      WorkDir: string;
    };
    Name: string;
  };
  RootFS: {
    Type: string;
    Layers: string[];
  };
  Metadata: {
    LastTagTime: string;
  };
}
