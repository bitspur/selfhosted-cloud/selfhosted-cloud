/*
 *  File: /docker/image/service.ts
 *  Project: api
 *  File Created: 12-02-2024 07:38:25
 *  Author: Clay Risser
 *  -----
 *  BitSpur (c) Copyright 2024
 *
 *  Licensed under the GNU Affero General Public License (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://www.gnu.org/licenses/agpl-3.0.en.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *  You can be released from the requirements of the license by purchasing
 *  a commercial license. Buying such a license is mandatory as soon as you
 *  develop commercial activities involving this software without disclosing
 *  the source code of your own applications.
 */

import kebabCase from 'lodash.kebabcase';
import { DockerImageLsArgs, DockerImageLsResult, DockerImagePruneResult, DockerImagePruneArgs } from './dto';
import { ExecaError, execa } from 'execa';
import { Flag } from '../types';
import { GraphQLError } from 'graphql';
import { Logger, Injectable } from '@multiplatform.one/typegraphql';
import { camelCaseKeys } from '../utils';
import {
  DockerBuildArgs,
  DockerBuildResult,
  DockerHistoryArgs,
  DockerHistoryResult,
  DockerImportArgs,
  DockerImportResult,
  DockerInspectArgs,
  DockerInspectResult,
  DockerLoadArgs,
  DockerLoadResult,
  DockerPullArgs,
  DockerPullResult,
  DockerRmArgs,
  DockerRmResult,
  DockerSaveArgs,
  DockerSaveResult,
  DockerTagArgs,
  DockerTagResult,
} from '../dto';

@Injectable()
export class DockerImageService {
  constructor(private readonly logger: Logger) {}

  async ls(args: DockerImageLsArgs = {}): Promise<DockerImageLsResult[]> {
    const { stderr, stdout } = await this.image('ls', {
      all: args.all,
      digests: args.digests,
      filter: args.filter,
      format: '{{json .}}',
      noTrunc: args.noTrunc,
    });
    if (stderr) throw new GraphQLError(stderr);
    return camelCaseKeys(JSON.parse(`[${stdout.split('\n').join(',')}]`));
  }

  async rm(args: DockerRmArgs): Promise<DockerRmResult> {
    const { stderr, stdout } = await this.image(
      'rm',
      {
        force: args.force,
        link: args.link,
        volumes: args.volumes,
      },
      [args.nameOrId],
    );
    if (stderr) throw new GraphQLError(stderr);
    return {
      stdout,
      stderr,
    };
  }

  async history(args: DockerHistoryArgs): Promise<DockerHistoryResult[]> {
    const { stderr, stdout } = await this.image(
      'history',
      {
        format: '{{json .}}',
        human: args.human,
        noTrunc: args.noTrunc,
      },
      [args.nameOrId],
    );
    if (stderr) throw new GraphQLError(stderr);
    return stdout
      .trim()
      .split('\n')
      .slice(1)
      .map((line) => {
        const [image, created, createdBy, size, comment] = line.split(/\s{2,}/);
        return {
          image,
          created,
          createdBy,
          size,
          comment,
        } as DockerHistoryResult;
      });
  }

  async pull(args: DockerPullArgs): Promise<DockerPullResult> {
    const { stderr, stdout } = await this.image(
      'pull',
      {
        allTags: args.allTags,
        disableContentTrust: args.disableContentTrust,
        platform: args.platform,
      },
      [args.name],
    );
    if (stderr) throw new GraphQLError(stderr);
    return {
      stdout: stdout,
      stderr: stderr,
    };
  }

  async push(args: DockerPullArgs): Promise<DockerPullResult> {
    const { stderr, stdout } = await this.image(
      'push',
      {
        allTags: args.allTags,
        disableContentTrust: args.disableContentTrust,
      },
      [args.name],
    );
    if (stderr) throw new GraphQLError(stderr);
    return {
      stdout: stdout,
      stderr: stderr,
    };
  }

  async tag(args: DockerTagArgs): Promise<DockerTagResult> {
    const { stderr, stdout } = await this.image('tag', {}, [args.source, args.target]);
    if (stderr) throw new GraphQLError(stderr);
    return {
      stdout: stdout,
      stderr: stderr,
    };
  }

  // TODO: add missing args
  async build(_args: DockerBuildArgs): Promise<DockerBuildResult> {
    const { stderr, stdout } = await this.image(
      'build',
      {
        // addHost: args.addHost,
        // annotation: args.annotation,
        // attest: args.attest,
        // buildArg: args.buildArg,
        // buildContext: args.buildContext,
        // builder: args.builder,
        // cacheFrom: args.cacheFrom,
        // cacheTo: args.cacheTo,
        // cgroupParent: args.cgroupParent,
        // file: args.file,
        // iidfile: args.iidfile,
        // label: args.label,
        // load: args.load,
        // metadataFile: args.metadataFile,
        // network: args.network,
        // noCache: args.noCache,
        // noCacheFilter: args.noCacheFilter,
        // output: args.output,
        // platform: args.platform,
        // progress: args.progress,
        // provenance: args.provenance,
        // pull: args.pull,
        // push: args.push,
        // sbom: args.sbom,
        // secret: args.secret,
        // shmSize: args.shmSize,
        // ssh: args.ssh,
        // tag: args.tag,
        // target: args.target,
        // ulimit: args.ulimit,
      },
      // [args.path],
    );
    if (stderr) throw new GraphQLError(stderr);
    return {
      stdout: stdout,
      stderr: stderr,
    };
  }

  async import(args: DockerImportArgs): Promise<DockerImportResult> {
    const { stderr, stdout } = await this.image(
      'import',
      {
        change: args.change,
        message: args.message,
        platform: args.platform,
      },
      [args.source, args.repository],
    );
    if (stderr) throw new GraphQLError(stderr);
    return {
      stdout: stdout,
      stderr: stderr,
    };
  }

  async load(args: DockerLoadArgs): Promise<DockerLoadResult> {
    const { stderr, stdout } = await this.image(
      'load',
      {
        input: args.input,
      },
      // [args.input],
    );
    if (stderr) throw new GraphQLError(stderr);
    return {
      stdout: stdout,
      stderr: stderr,
    };
  }

  async inspect(args: DockerInspectArgs): Promise<DockerInspectResult> {
    const { stderr, stdout } = await this.image(
      'inspect',
      {
        format: '{{json .}}',
        size: args.size,
      },
      [args.nameOrId],
    );
    if (stderr) throw new GraphQLError(stderr);
    const result = JSON.parse(stdout);
    return camelCaseKeys(result);
  }

  async save(args: DockerSaveArgs): Promise<DockerSaveResult> {
    const { stderr, stdout } = await this.image('save', {
      output: args.output,
    });
    if (stderr) throw new GraphQLError(stderr);
    return {
      stdout: stdout,
      stderr: stderr,
    };
  }

  async prune(args: DockerImagePruneArgs): Promise<DockerImagePruneResult> {
    const { stderr, stdout } = await this.image('prune', {
      all: args.all,
      force: args.force,
      pruneFilter: args.pruneFilter,
    });
    if (stderr) throw new GraphQLError(stderr);
    return {
      stdout: stdout,
      stderr: stderr,
    };
  }

  private async image(command: string, options: Record<string, Flag> = {}, args: string[] = []): Promise<ImageResult> {
    try {
      const p = execa('docker', [
        'image',
        command,
        ...Object.entries(options).reduce<string[]>((acc, [key, value]: [string, Flag]) => {
          if (value === true) {
            acc.push(`--${kebabCase(key)}`);
          } else if (typeof value === 'string' || typeof value === 'number') {
            acc.push(`--${kebabCase(key)}`, value.toString());
          } else if (Array.isArray(value)) {
            value.forEach((v) => {
              acc.push(`--${kebabCase(key)}`, v.toString());
            });
          }
          return acc;
        }, []),
        ...((args || []).filter(Boolean) as string[]),
      ]);
      if (process.env.DEBUG !== '0') {
        p.stdout?.pipe(process.stdout);
        p.stderr?.pipe(process.stderr);
      }
      const proc = await p;
      return {
        stderr: proc.stderr,
        stdout: proc.stdout,
      };
    } catch (err) {
      const error = err as ExecaError;
      if (error.stderr) {
        return {
          stderr: error.stderr,
          stdout: error.stdout,
        };
      }
      throw err;
    }
  }
}

export interface ImageResult {
  stderr: string;
  stdout: string;
  [key: string]: any;
}
