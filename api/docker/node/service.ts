/*
 *  File: /docker/node/service.ts
 *  Project: api
 *  File Created: 23-02-2024 06:02:57
 *  Author: Clay Risser
 *  -----
 *  BitSpur (c) Copyright 2024
 *
 *  Licensed under the GNU Affero General Public License (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://www.gnu.org/licenses/agpl-3.0.en.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *  You can be released from the requirements of the license by purchasing
 *  a commercial license. Buying such a license is mandatory as soon as you
 *  develop commercial activities involving this software without disclosing
 *  the source code of your own applications.
 */

import kebabCase from 'lodash.kebabcase';
import {
  DockerNodeInspectArgs,
  DockerNodeInspectResult,
  DockerNodeLsResult,
  DockerNodePsArgs,
  DockerNodePsResult,
  DockerNodeRmArgs,
  DockerNodeUpdateArgs,
} from './dto';
import { ExecaError, execa } from 'execa';
import { Flag } from '../types';
import { GraphQLError } from 'graphql';
import { Logger, Injectable } from '@multiplatform.one/typegraphql';
import { camelCaseKeys } from '../utils';

@Injectable()
export class DockerNodeService {
  constructor(private readonly logger: Logger) {}

  async demote(nodes: string[]): Promise<string> {
    const { stderr, stdout } = await this.node('demote', {}, nodes);
    if (stderr) throw new GraphQLError(stderr);
    return stdout;
  }

  async inspect(args: DockerNodeInspectArgs): Promise<DockerNodeInspectResult> {
    const { stdout, stderr } = await this.node(
      'inspect',
      {
        format: '{{json .}}',
      },
      [args.hostname],
    );
    if (stderr) throw new GraphQLError(stderr);
    return camelCaseKeys(JSON.parse(stdout));
  }

  async ls(): Promise<DockerNodeLsResult[]> {
    const { stderr, stdout } = await this.node('ls', {
      format: '{{json .}}',
    });
    if (stderr) {
      throw new GraphQLError(stderr);
    }
    return camelCaseKeys(JSON.parse(`[${stdout.split('\n').join(',')}]`));
  }

  async promote(nodes: string[]): Promise<string> {
    const { stderr, stdout } = await this.node('promote', {}, nodes);
    if (stderr) throw new GraphQLError(stderr);
    return stdout;
  }

  async ps(args: DockerNodePsArgs): Promise<DockerNodePsResult[]> {
    const { stdout, stderr } = await this.node('ps', {
      ...args.options,
      format: '{{json .}}',
    });
    if (stderr) throw new GraphQLError(stderr);
    return stdout.split('\n').map((line) => {
      const [id, name, image, node, desiredState, currentState, error, ports] = line.split(/\s{2,}/);
      return {
        id,
        name,
        image,
        node,
        desiredState,
        currentState,
        error,
        ports,
      } as DockerNodePsResult;
    });
  }

  async rm(args: DockerNodeRmArgs): Promise<string> {
    const { stderr, stdout } = await this.node('rm', { force: args.force }, args.nodes);
    if (stderr) throw new GraphQLError(stderr);
    return stdout;
  }

  async update(args: DockerNodeUpdateArgs): Promise<string> {
    const { stderr, stdout } = await this.node(
      'update',
      {
        ...args.options,
      },
      [args.node],
    );
    if (stderr) throw new GraphQLError(stderr);
    return stdout;
  }

  private async node(
    command: string,
    options: Record<string, Flag> = {},
    args: (string | undefined)[] = [],
  ): Promise<NodeResult> {
    try {
      const p = execa('docker', [
        'node',
        command,
        ...Object.entries(options).reduce<string[]>((acc, [key, value]: [string, Flag]) => {
          if (value === true) {
            acc.push(`--${kebabCase(key)}`);
          } else if (typeof value === 'string' || typeof value === 'number') {
            acc.push(`--${kebabCase(key)}`, value.toString());
          } else if (Array.isArray(value)) {
            value.forEach((v) => {
              acc.push(`--${kebabCase(key)}`, v.toString());
            });
          }
          return acc;
        }, []),
        ...((args || []).filter(Boolean) as string[]),
      ]);
      if (process.env.DEBUG !== '0') {
        p.stdout?.pipe(process.stdout);
        p.stderr?.pipe(process.stderr);
      }
      const proc = await p;
      return {
        stderr: proc.stderr,
        stdout: proc.stdout,
      };
    } catch (err) {
      const error = err as ExecaError;
      if (error.stderr) {
        return {
          stderr: error.stderr,
          stdout: error.stdout,
        };
      }
      throw err;
    }
  }
}

export interface NodeResult {
  stderr: string;
  stdout: string;
  [key: string]: any;
}
