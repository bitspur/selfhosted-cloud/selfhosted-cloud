/*
 *  File: /docker/container/types.ts
 *  Project: api
 *  File Created: 14-02-2024 06:10:56
 *  Author: Clay Risser
 *  -----
 *  BitSpur (c) Copyright 2024
 *
 *  Licensed under the GNU Affero General Public License (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://www.gnu.org/licenses/agpl-3.0.en.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *  You can be released from the requirements of the license by purchasing
 *  a commercial license. Buying such a license is mandatory as soon as you
 *  develop commercial activities involving this software without disclosing
 *  the source code of your own applications.
 */

export class DockerContainerCreateArgsType {
  image: string;
  options?: DockerContainerCreateOptionsType;
  commands?: string[];
  args?: string[];
}

export class DockerContainerCommitArgsType {
  container: string;
  options?: any;
  repository: string;
}

export class DockerContainerCommitOptionsType {
  author?: string;
  change?: string;
  message?: string;
  pause?: boolean;
}

export class DockerContainerCpArgsType {
  source: string;
  destination: string;
  options?: any;
}

export interface DockerContainerCreateOptionsType {
  addHost?: string[];
  attach?: string[];
  blkioWeight?: number;
  blkioWeightDevice?: string[];
  capAdd?: string[];
  capDrop?: string[];
  cgroupParent?: string;
  cgroupns?: string;
  cidfile?: string;
  cpuPeriod?: number;
  cpuQuota?: number;
  cpuRtPeriod?: number;
  cpuRtRuntime?: number;
  cpuShares?: number;
  cpus?: number;
  cpusetCpus?: string;
  cpusetMems?: string;
  device?: string[];
  deviceCgroupRule?: string[];
  deviceReadBps?: string[];
  deviceReadIops?: string[];
  deviceWriteBps?: string[];
  deviceWriteIops?: string[];
  disableContentTrust?: boolean;
  dns?: string[];
  dnsOption?: string[];
  dnsSearch?: string[];
  domainname?: string;
  entrypoint?: string;
  env?: string;
  envFile?: string[];
  expose?: string[];
  gpus?: string;
  groupAdd?: string[];
  healthCmd?: string;
  healthInterval?: string;
  healthRetries?: number;
  healthStartPeriod?: string;
  healthTimeout?: string;
  help?: boolean;
  hostname?: string;
  init?: boolean;
  interactive?: boolean;
  ip?: string;
  ip6?: string;
  ipc?: string;
  isolation?: string;
  kernelMemory?: string;
  label?: string[];
  labelFile?: string[];
  link?: string[];
  linkLocalIp?: string[];
  logDriver?: string;
  logOpt?: string[];
  macAddress?: string;
  publish?: string;
  publishAll?: boolean;
  pull?: string;
  readOnly?: boolean;
  restart?: string;
  rm?: boolean;
  runtime?: string;
  securityOpt?: string[];
  shmSize?: string;
  stopSignal?: string;
  stopTimeout?: number;
  storageOpt?: string[];
  sysctl?: string[];
  tmpfs?: string[];
  tty?: boolean;
  ulimit?: string[];
  user?: string;
  userns?: string;
  uts?: string;
  volume?: string[];
  volumeDriver?: string;
  volumesFrom?: string[];
  workdir?: string;
}
