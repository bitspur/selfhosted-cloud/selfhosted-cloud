/*
 *  File: /docker/container/service.ts
 *  Project: api
 *  File Created: 24-02-2024 09:27:50
 *  Author: Clay Risser
 *  -----
 *  BitSpur (c) Copyright 2024
 *
 *  Licensed under the GNU Affero General Public License (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://www.gnu.org/licenses/agpl-3.0.en.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *  You can be released from the requirements of the license by purchasing
 *  a commercial license. Buying such a license is mandatory as soon as you
 *  develop commercial activities involving this software without disclosing
 *  the source code of your own applications.
 */

import kebabCase from 'lodash.kebabcase';
import { DockerInspectResult } from '../dto';
import { ExecaError, execa } from 'execa';
import { Flag } from '../types';
import { GraphQLError } from 'graphql';
import { camelCaseKeys } from '../utils';
import {
  DockerContainerAttachArgs,
  DockerContainerCommitArgs,
  DockerContainerCpArgs,
  DockerContainerCreateArgs,
  DockerContainerCreateResult,
  DockerContainerDiffResult,
  DockerContainerExecArgs,
  DockerContainerExportArgs,
  DockerContainerInspectArgs,
  DockerContainerKillArgs,
  DockerContainerKillResult,
  DockerContainerLogsArgs,
  DockerContainerLsArgs,
  DockerContainerLsResult,
  DockerContainerPauseArgs,
  DockerContainerPortArgs,
  DockerContainerPortResult,
  DockerContainerPruneArgs,
  DockerContainerRenameArgs,
  DockerContainerRenameResult,
  DockerContainerRestartArgs,
  DockerContainerRmArgs,
  DockerContainerStartArgs,
  DockerContainerStartResult,
  DockerContainerStatsArgs,
  DockerContainerStatsResult,
  DockerContainerStopArgs,
  DockerContainerTopArgs,
  DockerContainerTopResult,
  DockerContainerUpdateArgs,
  DockerContainerWaitArgs,
} from './dto';

export class DockerContainerService {
  async create(args: DockerContainerCreateArgs): Promise<DockerContainerCreateResult> {
    const result = await this.container(
      'create',
      {
        ...args.options,
      },
      [args.image, ...(args.command || []), ...(args.args || [])],
    );
    if (result.stderr) {
      throw new GraphQLError(result.stderr);
    }
    return {
      id: result.stdout,
    };
  }

  async attach(args: DockerContainerAttachArgs): Promise<string> {
    const { stderr, stdout } = await this.container(
      'attach',
      {
        ...args.options,
      },
      [args.container],
    );
    if (stderr) {
      throw new GraphQLError(stderr);
    }
    return stdout;
  }

  async commit(args: DockerContainerCommitArgs): Promise<string> {
    const { stderr, stdout } = await this.container(
      'commit',
      {
        ...args.options,
      },
      [args.container, args.repository],
    );

    if (stderr) {
      throw new GraphQLError(stderr);
    }
    return stdout;
  }

  async cp(args: DockerContainerCpArgs): Promise<string> {
    const { stderr, stdout } = await this.container(
      'cp',
      {
        ...args.options,
      },
      [args.source, args.destination],
    );
    if (stderr) {
      throw new GraphQLError(stderr);
    }
    return stdout;
  }

  async diff(container: string): Promise<DockerContainerDiffResult[]> {
    const { stderr, stdout } = await this.container('diff', {}, [container]);
    if (stderr) {
      throw new GraphQLError(stderr);
    }
    return stdout.split('\n').map((line) => {
      const [changeType, filePath] = line.split(' ');
      return { changeType, filePath } as DockerContainerDiffResult;
    });
  }

  // TODO implement completely
  async exec(args: DockerContainerExecArgs): Promise<string> {
    const { stderr, stdout } = await this.container(
      'exec',
      {
        ...args.options,
      },
      [args.container, ...args.commands],
    );
    if (stderr) {
      throw new GraphQLError(stderr);
    }
    return stdout;
  }

  async export(args: DockerContainerExportArgs): Promise<string> {
    const { stderr, stdout } = await this.container(
      'export',
      {
        output: args.output,
      },
      [args.container],
    );
    if (stderr) {
      throw new GraphQLError(stderr);
    }
    return stdout;
  }

  async inspect(args: DockerContainerInspectArgs): Promise<DockerInspectResult> {
    const { stderr, stdout } = await this.container(
      'inspect',
      {
        format: '{{json .}}',
        ...args?.options,
      },
      [args.container],
    );
    if (stderr) throw new GraphQLError(stderr);
    return camelCaseKeys(JSON.parse(stdout));
  }

  async kill(args: DockerContainerKillArgs): Promise<DockerContainerKillResult> {
    const { stderr, stdout } = await this.container(
      'kill',
      {
        ...args.options,
      },
      args.containers,
    );
    if (stderr) {
      throw new GraphQLError(stderr);
    }
    return {
      containers: stdout.split('\n').filter((c) => c),
    };
  }

  async logs(args: DockerContainerLogsArgs): Promise<string> {
    const { stderr, stdout } = await this.container(
      'logs',
      {
        ...args.options,
      },
      [args.container],
    );
    if (stderr) {
      throw new GraphQLError(stderr);
    }
    return stdout;
  }

  async ls(args: DockerContainerLsArgs = {}): Promise<DockerContainerLsResult[]> {
    const { stderr, stdout } = await this.container('ls', {
      all: args.all,
      filter: args.filter,
      last: args.last,
      latest: args.latest,
      format: '{{json .}}',
      noTrunc: args.noTrunc,
      size: args.size,
    });
    if (stderr) {
      throw new GraphQLError(stderr);
    }

    return camelCaseKeys(JSON.parse(`[${stdout.split('\n').join(',')}]`));
  }

  async pause(args: DockerContainerPauseArgs): Promise<string[]> {
    const { stderr, stdout } = await this.container('pause', {}, args.containers);
    if (stderr) throw new GraphQLError(stderr);
    return stdout.split('\n').filter((c) => c);
  }

  async port(args: DockerContainerPortArgs): Promise<DockerContainerPortResult> {
    const { stderr, stdout } = await this.container('port', {}, [args.container, args.privatePort]);
    if (stderr) throw new GraphQLError(stderr);
    return {
      publicPorts: stdout.split('\n').filter((c) => c),
    };
  }

  async prune(args: DockerContainerPruneArgs): Promise<string> {
    const { stderr, stdout } = await this.container('prune', {
      ...args,
      force: args.force ? args.force : true,
    });
    if (stderr) throw new GraphQLError(stderr);
    return stdout;
  }

  async rename(args: DockerContainerRenameArgs): Promise<DockerContainerRenameResult> {
    const { stderr, stdout } = await this.container('rename', {}, [args.nameOrId, args.newName]);
    if (stderr) throw new GraphQLError(stderr);
    return {
      stdout: stdout,
      stderr: stderr,
    };
  }

  async restart(args: DockerContainerRestartArgs): Promise<string[]> {
    const { stderr, stdout } = await this.container(
      'restart',
      {
        ...args.options,
      },
      args.containers,
    );
    if (stderr) throw new GraphQLError(stderr);
    return stdout.split('\n').filter((c) => c);
  }

  async rm(args: DockerContainerRmArgs): Promise<string[]> {
    const { stderr, stdout } = await this.container(
      'rm',
      {
        ...args.options,
      },
      args.containers,
    );
    if (stderr) throw new GraphQLError(stderr);
    return stdout.split('\n').filter((c) => c);
  }

  async run(args: DockerContainerCreateArgs): Promise<DockerContainerCreateResult> {
    const { stderr, stdout } = await this.container(
      'run',
      {
        ...args.options,
        detach: args.options?.detach ? args.options.detach : true,
      },
      [args.image, ...(args.command || []), ...(args.args || [])],
    );
    if (stderr) throw new GraphQLError(stderr);
    return {
      id: stdout,
    };
  }

  async start(args: DockerContainerStartArgs): Promise<DockerContainerStartResult> {
    const { stderr, stdout } = await this.container(
      'start',
      {
        ...args.options,
      },
      args.containers,
    );
    if (stderr) throw new GraphQLError(stderr);
    return {
      containers: stdout.split('/n'),
    };
  }

  async stats(args: DockerContainerStatsArgs): Promise<DockerContainerStatsResult[]> {
    const { stderr, stdout } = await this.container(
      'stats',
      {
        ...args.options,
        noStream: args.options?.noStream ? args.options.noStream : true,
        format: '{{json .}}',
      },
      args.containers,
    );
    if (stderr) throw new GraphQLError(stderr);
    return camelCaseKeys(JSON.parse(`[${stdout.split('\n').join(',')}]`));
  }

  async stop(args: DockerContainerStopArgs): Promise<DockerContainerStartResult> {
    const { stderr, stdout } = await this.container(
      'stop',
      {
        ...args.options,
      },
      args.containers,
    );
    if (stderr) throw new GraphQLError(stderr);
    return {
      containers: stdout.split('\n').filter((c) => c),
    };
  }

  async top(args: DockerContainerTopArgs): Promise<DockerContainerTopResult[]> {
    const { stderr, stdout } = await this.container('top', {}, [args.container]);
    if (stderr) throw new GraphQLError(stderr);
    return stdout
      .split('\n')
      .slice(1)
      .map((line) => {
        const [uId, pId, ppId, c, sTime, tty, time, cmd] = line.split(/\s{2,}/).map((part) => part.trim());
        return {
          uId,
          pId,
          ppId,
          c,
          sTime,
          tty,
          time,
          cmd,
        } as DockerContainerTopResult;
      });
  }

  async update(args: DockerContainerUpdateArgs): Promise<DockerContainerStartResult> {
    const { stderr, stdout } = await this.container(
      'update',
      {
        ...args.options,
      },
      args.containers,
    );
    if (stderr) throw new GraphQLError(stderr);
    return {
      containers: stdout.split('\n'),
    };
  }

  async unpause(args: DockerContainerPauseArgs): Promise<String[]> {
    const { stderr, stdout } = await this.container('unpause', {}, args.containers);
    if (stderr) throw new GraphQLError(stderr);
    return stdout.split('\n').filter((c) => c);
  }

  async wait(args: DockerContainerWaitArgs): Promise<string> {
    const { stderr, stdout } = await this.container('update', {}, args.containers);
    if (stderr) throw new GraphQLError(stderr);
    return stdout;
  }

  private async container(
    command: string,
    options: Record<string, Flag> = {},
    args: (string | undefined)[] = [],
  ): Promise<ContainerResult> {
    try {
      const p = execa('docker', [
        'container',
        command,
        ...Object.entries(options).reduce<string[]>((acc, [key, value]: [string, Flag]) => {
          if (value === true) {
            acc.push(`--${kebabCase(key)}`);
          } else if (typeof value === 'string' || typeof value === 'number') {
            acc.push(`--${kebabCase(key)}`, value.toString());
          } else if (Array.isArray(value)) {
            value.forEach((v) => {
              acc.push(`--${kebabCase(key)}`, v.toString());
            });
          }
          return acc;
        }, []),
        ...((args || []).filter(Boolean) as string[]),
      ]);
      if (process.env.DEBUG !== '0') {
        p.stdout?.pipe(process.stdout);
        p.stderr?.pipe(process.stderr);
      }
      const proc = await p;
      if (command === 'logs') {
        return {
          stderr: '',
          stdout: `${proc.stdout}\n${proc.stderr}`,
        };
      }
      return {
        stderr: proc.stderr,
        stdout: proc.stdout,
      };
    } catch (err) {
      const error = err as ExecaError;
      if (error.stderr) {
        return {
          stderr: error.stderr,
          stdout: error.stdout,
        };
      }
      throw err;
    }
  }
}

export interface ContainerResult {
  stderr: string;
  stdout: string;
  [key: string]: any;
}
