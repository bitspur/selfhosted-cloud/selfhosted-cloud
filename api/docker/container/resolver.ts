/*
 *  File: /docker/container/resolver.ts
 *  Project: api
 *  File Created: 24-02-2024 09:27:50
 *  Author: Clay Risser
 *  -----
 *  BitSpur (c) Copyright 2024
 *
 *  Licensed under the GNU Affero General Public License (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://www.gnu.org/licenses/agpl-3.0.en.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *  You can be released from the requirements of the license by purchasing
 *  a commercial license. Buying such a license is mandatory as soon as you
 *  develop commercial activities involving this software without disclosing
 *  the source code of your own applications.
 */

import { Authorized } from '@multiplatform.one/keycloak-typegraphql';
import { Injectable } from '@multiplatform.one/typegraphql';
import { Args, Mutation, Resolver, Query, Arg } from 'type-graphql';
import { DockerContainerService } from './service';
import {
  DockerContainerAttachArgs,
  DockerContainerCommitArgs,
  DockerContainerCpArgs,
  DockerContainerCreateArgs,
  DockerContainerCreateResult,
  DockerContainerDiffResult,
  DockerContainerExecArgs,
  DockerContainerExportArgs,
  DockerContainerInspectArgs,
  DockerContainerKillArgs,
  DockerContainerKillResult,
  DockerContainerLogsArgs,
  DockerContainerLsArgs,
  DockerContainerLsResult,
  DockerContainerPauseArgs,
  DockerContainerPortArgs,
  DockerContainerPortResult,
  DockerContainerPruneArgs,
  DockerContainerRenameArgs,
  DockerContainerRenameResult,
  DockerContainerRestartArgs,
  DockerContainerRmArgs,
  DockerContainerStartArgs,
  DockerContainerStartResult,
  DockerContainerStatsArgs,
  DockerContainerStatsResult,
  DockerContainerStopArgs,
  DockerContainerTopArgs,
  DockerContainerTopResult,
  DockerContainerUpdateArgs,
  DockerContainerWaitArgs,
} from './dto';
import { DockerInspectResult } from '../dto';

@Authorized()
@Injectable()
@Resolver()
export class DockerContainerResolver {
  constructor(private readonly dockerContainer: DockerContainerService) {}

  @Query(() => [DockerContainerLsResult])
  async dockerContainerLs(@Args(() => DockerContainerLsArgs) args: DockerContainerLsArgs) {
    return this.dockerContainer.ls(args);
  }

  @Query(() => [DockerContainerDiffResult])
  async dockerContainerDiff(@Arg('container', () => String) container: string) {
    return this.dockerContainer.diff(container);
  }

  @Query(() => DockerInspectResult)
  async dockerContainerInspect(@Args(() => DockerContainerInspectArgs) args: DockerContainerInspectArgs) {
    return this.dockerContainer.inspect(args);
  }

  @Query(() => String)
  async dockerContainerLogs(@Args(() => DockerContainerLogsArgs) args: DockerContainerLogsArgs) {
    return this.dockerContainer.logs(args);
  }

  @Query(() => DockerContainerPortResult)
  async dockerContainerPort(@Args(() => DockerContainerPortArgs) args: DockerContainerPortArgs) {
    return this.dockerContainer.port(args);
  }

  @Query(() => [DockerContainerStatsResult])
  async dockerContainerStats(@Args(() => DockerContainerStatsArgs) args: DockerContainerStatsArgs) {
    return this.dockerContainer.stats(args);
  }

  @Mutation(() => DockerContainerCreateResult)
  async dockerContainerCreate(@Args(() => DockerContainerCreateArgs) args: DockerContainerCreateArgs) {
    return this.dockerContainer.create(args);
  }

  @Mutation(() => String)
  async dockerContainerAttach(@Args(() => DockerContainerAttachArgs) args: DockerContainerAttachArgs) {
    return this.dockerContainer.attach(args);
  }

  @Mutation(() => String)
  async dockerContainerCommit(@Args(() => DockerContainerCommitArgs) args: DockerContainerCommitArgs) {
    return this.dockerContainer.commit(args);
  }

  @Mutation(() => String)
  async dockerContainerCp(@Args(() => DockerContainerCpArgs) args: DockerContainerCpArgs) {
    return this.dockerContainer.cp(args);
  }

  @Mutation(() => String, { nullable: true })
  async dockerContainerExec(@Args(() => DockerContainerExecArgs) args: DockerContainerExecArgs) {
    return this.dockerContainer.exec(args);
  }

  @Mutation(() => [String])
  async dockerContainerPause(@Args(() => DockerContainerPauseArgs) args: DockerContainerPauseArgs) {
    return this.dockerContainer.pause(args);
  }

  @Mutation(() => [String])
  async dockerContainerUnpause(@Args(() => DockerContainerPauseArgs) args: DockerContainerPauseArgs) {
    return this.dockerContainer.unpause(args);
  }

  @Mutation(() => DockerContainerRenameResult)
  async dockerContainerRename(@Args(() => DockerContainerRenameArgs) args: DockerContainerRenameArgs) {
    return this.dockerContainer.rename(args);
  }

  @Mutation(() => String)
  async dockerContainerExport(@Args(() => DockerContainerExportArgs) args: DockerContainerExportArgs) {
    return this.dockerContainer.export(args);
  }

  @Mutation(() => DockerContainerKillResult)
  async dockerContainerKill(@Args(() => DockerContainerKillArgs) args: DockerContainerKillArgs) {
    return this.dockerContainer.kill(args);
  }

  @Mutation(() => String)
  async dockerContainerPrune(@Args(() => DockerContainerPruneArgs) args: DockerContainerPruneArgs) {
    return this.dockerContainer.prune(args);
  }

  @Mutation(() => [String])
  async dockerContainerRestart(@Args(() => DockerContainerRestartArgs) args: DockerContainerRestartArgs) {
    return this.dockerContainer.restart(args);
  }

  @Mutation(() => [String])
  async dockerContainerRm(@Args(() => DockerContainerRmArgs) args: DockerContainerRmArgs) {
    return this.dockerContainer.rm(args);
  }

  @Mutation(() => DockerContainerCreateResult)
  async dockerContainerRun(@Args(() => DockerContainerCreateArgs) args: DockerContainerCreateArgs) {
    return this.dockerContainer.run(args);
  }

  @Mutation(() => DockerContainerStartResult)
  async dockerContainerStart(@Args(() => DockerContainerStartArgs) args: DockerContainerStartArgs) {
    return this.dockerContainer.start(args);
  }

  @Mutation(() => DockerContainerStartResult)
  async dockerContainerStop(@Args(() => DockerContainerStopArgs) args: DockerContainerStopArgs) {
    return this.dockerContainer.stop(args);
  }

  @Mutation(() => [DockerContainerTopResult])
  async dockerContainerTop(@Args(() => DockerContainerTopArgs) args: DockerContainerTopArgs) {
    return this.dockerContainer.top(args);
  }

  @Mutation(() => DockerContainerStartResult)
  async dockerContainerUpdate(@Args(() => DockerContainerUpdateArgs) args: DockerContainerUpdateArgs) {
    return this.dockerContainer.update(args);
  }

  @Mutation(() => String, { nullable: true })
  async dockerContainerWait(@Args(() => DockerContainerWaitArgs) args: DockerContainerWaitArgs) {
    this.dockerContainer.wait(args);
  }
}
