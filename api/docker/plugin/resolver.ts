/*
 *  File: /docker/plugin/resolver.ts
 *  Project: api
 *  File Created: 23-02-2024 06:02:57
 *  Author: Clay Risser
 *  -----
 *  BitSpur (c) Copyright 2024
 *
 *  Licensed under the GNU Affero General Public License (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://www.gnu.org/licenses/agpl-3.0.en.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *  You can be released from the requirements of the license by purchasing
 *  a commercial license. Buying such a license is mandatory as soon as you
 *  develop commercial activities involving this software without disclosing
 *  the source code of your own applications.
 */

import { Authorized } from '@multiplatform.one/keycloak-typegraphql';
import { DockerPluginService } from './service';
import { Injectable } from '@multiplatform.one/typegraphql';
import { Mutation, Resolver, Args, Query } from 'type-graphql';
import {
  DockerPluginCreateArgs,
  DockerPluginCreateResult,
  DockerPluginInspectArgs,
  DockerPluginInspectResult,
  DockerPluginLsArgs,
  DockerPluginLsResult,
  DockerPluginRmArgs,
  DockerPluginRmResult,
} from './dto';

@Authorized()
@Injectable()
@Resolver()
export class DockerPluginResolver {
  constructor(private readonly dockerPlugin: DockerPluginService) {}

  @Query(() => DockerPluginLsResult)
  async dockerPluginLs(@Args(() => DockerPluginLsArgs) args: DockerPluginLsArgs): Promise<DockerPluginLsResult> {
    return this.dockerPlugin.ls(args);
  }

  @Mutation(() => DockerPluginCreateResult)
  async dockerPluginCreate(
    @Args(() => DockerPluginCreateArgs) args: DockerPluginCreateArgs,
  ): Promise<DockerPluginCreateResult> {
    return this.dockerPlugin.create(args);
  }

  @Mutation(() => DockerPluginRmResult)
  async dockerPluginRm(@Args(() => DockerPluginRmArgs) args: DockerPluginRmArgs) {
    return this.dockerPlugin.rm(args);
  }

  @Query(() => [DockerPluginInspectResult])
  async dockerPluginInspect(@Args(() => DockerPluginInspectArgs) args: DockerPluginInspectArgs) {
    return this.dockerPlugin.inspect(args);
  }
}
