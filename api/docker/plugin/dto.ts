/*
 *  File: /docker/plugin/dto.ts
 *  Project: api
 *  File Created: 23-02-2024 06:02:57
 *  Author: Clay Risser
 *  -----
 *  BitSpur (c) Copyright 2024
 *
 *  Licensed under the GNU Affero General Public License (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://www.gnu.org/licenses/agpl-3.0.en.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *  You can be released from the requirements of the license by purchasing
 *  a commercial license. Buying such a license is mandatory as soon as you
 *  develop commercial activities involving this software without disclosing
 *  the source code of your own applications.
 */

import { Field, ArgsType, ObjectType } from 'type-graphql';

@ArgsType()
export class DockerPluginLsArgs {
  @Field(() => String, { nullable: true, description: 'Provide filter values (e.g. "enabled=true")' })
  filter?: string;

  @Field(() => Boolean, { nullable: true, description: "Don't truncate output" })
  noTrunc?: boolean;
}

@ObjectType()
export class DockerPluginLsResult {
  @Field(() => String, { nullable: true, description: 'A brief description of the Docker plugin' })
  description?: string;

  @Field(() => String, { nullable: true, description: 'Indicates whether the Docker plugin is enabled or not' })
  enabled?: string;

  @Field(() => String, { nullable: true, description: 'The unique identifier of the Docker plugin' })
  id?: string;

  @Field(() => String, { nullable: true, description: 'The name of the Docker plugin' })
  name?: string;

  @Field(() => String, { nullable: true, description: 'A reference string for the Docker plugin' })
  pluginReference?: string;
}

@ArgsType()
export class DockerPluginCreateArgs {
  @Field(() => String, { description: 'The name of the Docker plugin' })
  plugin: string;

  @Field(() => Boolean, { nullable: true, description: 'Compress the context using gzip' })
  compress?: boolean;

  @Field(() => String, { nullable: true, description: 'The Docker plugin data directory' })
  pluginDataDir?: string;
}

@ObjectType()
export class DockerPluginCreateResult {
  @Field(() => String, { nullable: true, description: 'The unique identifier of the Docker plugin' })
  id?: string;
}

@ArgsType()
export class DockerPluginRmArgs {
  @Field(() => [String], { nullable: true, description: 'Name of the plugins' })
  plugin: string[];
}

@ObjectType()
export class DockerPluginRmResult {
  @Field(() => String, { nullable: true })
  stdout?: string;

  @Field(() => String, { nullable: true })
  stderr?: string;
}

@ArgsType()
export class DockerPluginInspectArgs {
  @Field(() => [String], { description: 'Name of the plugin' })
  plugin: string[];
}

@ObjectType()
export class DockerPluginInspectResult {
  @Field(() => DockerPluginInspectConfigResult, {
    nullable: true,
    description: 'The configuration of the Docker plugin',
  })
  config?: DockerPluginInspectConfigResult;

  @Field(() => Boolean, { nullable: true, description: 'Whether the Docker plugin is enabled' })
  enabled: boolean;

  @Field(() => String, { nullable: true, description: 'Unique identifier of the Docker plugin' })
  id: string;

  @Field(() => String, { nullable: true, description: 'Name of the Docker plugin' })
  name: string;

  @Field(() => DockerPluginSettings, { nullable: true, description: 'Settings for the Docker plugin' })
  settings: DockerPluginSettings;
}

@ObjectType()
export class DockerPluginInspectConfigResult {
  @Field(() => DockerPluginConfigArgs, { nullable: true, description: 'The arguments for the Docker plugin' })
  args: DockerPluginConfigArgs;

  @Field(() => String, { nullable: true, description: 'Description of the Docker plugin' })
  description: string;

  @Field(() => String, { nullable: true, description: 'Version of Docker that the plugin is compatible with' })
  dockerVersion: string;

  @Field(() => String, { nullable: true, description: 'Documentation for the Docker plugin' })
  documentation: string;

  @Field(() => [String], { nullable: true, description: 'Entrypoint for the Docker plugin' })
  entrypoint: string[];

  @Field(() => [DockerPluginEnv], {
    nullable: 'itemsAndList',
    description: 'Environment variables for the Docker plugin',
  })
  env: DockerPluginEnv[];

  @Field(() => DockerPluginInterface, { nullable: true, description: 'Interface for the Docker plugin' })
  interface: DockerPluginInterface;

  @Field(() => Boolean, { nullable: true, description: 'Whether the IPC host is enabled for the Docker plugin' })
  ipcHost: boolean;

  @Field(() => DockerPluginLinux, { nullable: true, description: 'Linux-specific configuration for the Docker plugin' })
  linux: DockerPluginLinux;

  @Field(() => String, { nullable: true, description: 'Mounts for the Docker plugin' })
  mounts: string | null;

  @Field(() => DockerPluginNetwork, { nullable: true, description: 'Network configuration for the Docker plugin' })
  network: DockerPluginNetwork;

  @Field(() => Boolean, { nullable: true, description: 'Whether the PID host is enabled for the Docker plugin' })
  pidHost: boolean;

  @Field(() => String, { nullable: true, description: 'Propagated mount for the Docker plugin' })
  propagatedMount: string;

  @Field(() => DockerPluginUser, { nullable: true, description: 'User configuration for the Docker plugin' })
  user: DockerPluginUser;

  @Field(() => String, { nullable: true, description: 'Working directory for the Docker plugin' })
  workDir: string;

  @Field(() => DockerPluginRootfs, { nullable: true, description: 'Root filesystem for the Docker plugin' })
  rootfs: DockerPluginRootfs;
}

@ObjectType()
export class DockerPluginConfigArgs {
  @Field(() => String, { nullable: true, description: 'Description of the argument' })
  description: string;

  @Field(() => String, { nullable: true, description: 'Name of the argument' })
  name: string;

  @Field(() => String, { nullable: true, description: 'Settable value for the argument' })
  settable: string | null;

  @Field(() => String, { nullable: true, description: 'Value of the argument' })
  value: string | null;
}

@ObjectType()
export class DockerPluginEnv {
  @Field(() => String, { nullable: true, description: 'Description of the environment variable' })
  description: string;

  @Field(() => String, { nullable: true, description: 'Name of the environment variable' })
  name: string;

  @Field(() => [String], { nullable: true, description: 'Settable values for the environment variable' })
  settable: string[];

  @Field(() => String, { nullable: true, description: 'Value of the environment variable' })
  value: string;
}

@ObjectType()
export class DockerPluginInterface {
  @Field(() => String, { nullable: true, description: 'Socket for the Docker plugin' })
  socket: string;

  @Field(() => [String], { nullable: true, description: 'Types of the Docker plugin' })
  types: string[];
}

@ObjectType()
export class DockerPluginLinux {
  @Field(() => Boolean, { nullable: true, description: 'Whether all devices are allowed for the Docker plugin' })
  allowAllDevices: boolean;

  @Field(() => [String], { nullable: true, description: 'Capabilities for the Docker plugin' })
  capabilities: string[];

  @Field(() => String, { nullable: true, description: 'Devices for the Docker plugin' })
  devices: string | null;
}

@ObjectType()
export class DockerPluginNetwork {
  @Field(() => String, { nullable: true, description: 'Type of network for the Docker plugin' })
  type: string;
}

@ObjectType()
export class DockerPluginUser {
  @Field(() => String, { nullable: true, description: 'User configuration for the Docker plugin' })
  user: string;
}

@ObjectType()
export class DockerPluginRootfs {
  @Field(() => [String], { nullable: true, description: 'Diff IDs for the Docker plugin' })
  diffIds: string[];

  @Field(() => String, { nullable: true, description: 'Type of the Docker plugin' })
  type: string;
}

@ObjectType()
export class DockerPluginSettings {
  @Field(() => [String], { nullable: true, description: 'Arguments for the Docker plugin' })
  args: string[];

  @Field(() => [String], { nullable: true, description: 'Devices for the Docker plugin' })
  devices: string[];

  @Field(() => [String], { nullable: true, description: 'Environment variables for the Docker plugin' })
  env: string[];

  @Field(() => [String], { nullable: true, description: 'Mounts for the Docker plugin' })
  mounts: string[];
}
