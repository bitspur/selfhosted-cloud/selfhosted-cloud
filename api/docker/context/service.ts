/*
 *  File: /docker/context/service.ts
 *  Project: api
 *  File Created: 23-02-2024 06:02:57
 *  Author: Clay Risser
 *  -----
 *  BitSpur (c) Copyright 2024
 *
 *  Licensed under the GNU Affero General Public License (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://www.gnu.org/licenses/agpl-3.0.en.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *  You can be released from the requirements of the license by purchasing
 *  a commercial license. Buying such a license is mandatory as soon as you
 *  develop commercial activities involving this software without disclosing
 *  the source code of your own applications.
 */

import kebabCase from 'lodash.kebabcase';
import { ExecaError, execa } from 'execa';
import { Flag } from '../types';
import { GraphQLError } from 'graphql';
import { Injectable } from '@multiplatform.one/typegraphql';
import { camelCaseKeys } from '../utils';
import {
  DockerContextCreateArgs,
  DockerContextExportArgs,
  DockerContextImportArgs,
  DockerContextInspectArgs,
  DockerContextInspectResult,
  DockerContextLsResult,
  DockerContextRmArgs,
} from './dto';

@Injectable()
export class DockerContextService {
  async create(args: DockerContextCreateArgs): Promise<string> {
    const { stderr, stdout } = await this.context(
      'create',
      {
        ...args.options,
      },
      [args.context],
    );
    if (stderr) {
      throw new GraphQLError(stderr);
    }
    return stdout;
  }

  async export(args: DockerContextExportArgs): Promise<string> {
    const { stderr, stdout } = await this.context(
      'export',
      {
        kubeconfig: args.kubeconfig,
      },
      [args.context],
    );
    if (stderr) {
      throw new GraphQLError(stderr);
    }
    return stdout;
  }

  async inspect(args: DockerContextInspectArgs): Promise<DockerContextInspectResult[]> {
    const { stderr, stdout } = await this.context('inspect', {}, args.contexts);
    if (stderr) {
      throw new GraphQLError(stderr);
    }
    const res = camelCaseKeys(JSON.parse(stdout)) as DockerContextInspectResult[];
    return res;
  }

  async import(args: DockerContextImportArgs): Promise<string> {
    const { stderr, stdout } = await this.context('import', {}, [args.context]);
    if (stderr) {
      throw new GraphQLError(stderr);
    }
    return stdout;
  }

  async ls(): Promise<DockerContextLsResult[]> {
    const { stderr, stdout } = await this.context('ls', {
      format: '{{json .}}',
    });
    if (stderr) {
      throw new GraphQLError(stderr);
    }
    return camelCaseKeys(JSON.parse(`[${stdout.split('\n').filter(Boolean).join(',')}]`));
  }

  async rm(args: DockerContextRmArgs): Promise<string[]> {
    const { stderr, stdout } = await this.context(
      'rm',
      {
        force: args.force,
      },
      args.contexts,
    );
    if (stderr) {
      throw new GraphQLError(stderr);
    }
    return stdout.split('\n').filter(Boolean);
  }

  async update(args: DockerContextCreateArgs): Promise<string> {
    const { stderr, stdout } = await this.context(
      'update',
      {
        ...args.options,
      },
      [args.context],
    );
    if (stderr) {
      throw new GraphQLError(stderr);
    }
    return stdout;
  }

  async use(context: string): Promise<string> {
    const { stderr, stdout } = await this.context('use', {}, [context]);
    if (stderr) {
      throw new GraphQLError(stderr);
    }
    return stdout;
  }

  private async context(
    command: string,
    options: Record<string, Flag> = {},
    args: (string | undefined)[] = [],
  ): Promise<ContextResult> {
    try {
      const p = execa('docker', [
        'context',
        command,
        ...Object.entries(options).reduce<string[]>((acc, [key, value]: [string, Flag]) => {
          if (value === true) {
            acc.push(`--${kebabCase(key)}`);
          } else if (typeof value === 'string' || typeof value === 'number') {
            acc.push(`--${kebabCase(key)}`, value.toString());
          } else if (Array.isArray(value)) {
            value.forEach((v) => {
              acc.push(`--${kebabCase(key)}`, v.toString());
            });
          }
          return acc;
        }, []),
        ...((args || []).filter(Boolean) as string[]),
      ]);
      if (process.env.DEBUG !== '0') {
        p.stdout?.pipe(process.stdout);
        p.stderr?.pipe(process.stderr);
      }
      const proc = await p;
      return {
        stderr: proc.stderr,
        stdout: proc.stdout,
      };
    } catch (err) {
      const error = err as ExecaError;
      if (error.stderr) {
        return {
          stderr: error.stderr,
          stdout: error.stdout,
        };
      }
      throw err;
    }
  }
}

export interface ContextResult {
  stderr: string;
  stdout: string;
  [key: string]: any;
}
