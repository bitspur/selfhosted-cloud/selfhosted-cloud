/*
 *  File: /docker/context/resolver.ts
 *  Project: api
 *  File Created: 23-02-2024 06:02:57
 *  Author: Clay Risser
 *  -----
 *  BitSpur (c) Copyright 2024
 *
 *  Licensed under the GNU Affero General Public License (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://www.gnu.org/licenses/agpl-3.0.en.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *  You can be released from the requirements of the license by purchasing
 *  a commercial license. Buying such a license is mandatory as soon as you
 *  develop commercial activities involving this software without disclosing
 *  the source code of your own applications.
 */

import { Authorized } from '@multiplatform.one/keycloak-typegraphql';
import { Injectable } from '@multiplatform.one/typegraphql';
import { Resolver, Args, Mutation, Query, Arg } from 'type-graphql';
import { DockerContextService } from './service';
import {
  DockerContextCreateArgs,
  DockerContextExportArgs,
  DockerContextImportArgs,
  DockerContextInspectArgs,
  DockerContextInspectResult,
  DockerContextLsResult,
  DockerContextRmArgs,
} from './dto';

@Authorized()
@Injectable()
@Resolver()
export class DockerContextResolver {
  constructor(private readonly context: DockerContextService) {}

  @Query(() => [DockerContextInspectResult])
  async dockerContextInspect(@Args(() => DockerContextInspectArgs) args: DockerContextInspectArgs) {
    return this.context.inspect(args);
  }

  @Query(() => [DockerContextLsResult])
  async dockerContextLs() {
    return this.context.ls();
  }

  @Mutation(() => String)
  async dockerContextCreate(@Args(() => DockerContextCreateArgs) args: DockerContextCreateArgs) {
    return this.context.create(args);
  }

  @Mutation(() => String)
  async dockerContextExport(@Args(() => DockerContextExportArgs) args: DockerContextExportArgs) {
    return this.context.export(args);
  }

  @Mutation(() => String)
  async dockerContextImport(@Args(() => DockerContextImportArgs) args: DockerContextImportArgs) {
    return this.context.import(args);
  }

  @Mutation(() => String)
  async dockerContextRm(@Args(() => DockerContextRmArgs) args: DockerContextRmArgs) {
    return this.context.rm(args);
  }

  @Mutation(() => String)
  async dockerContextUse(@Arg('context', () => String) context: string) {
    return this.context.use(context);
  }

  @Mutation(() => String)
  async dockerContextUpdate(@Args(() => DockerContextCreateArgs) args: DockerContextCreateArgs) {
    return this.context.update(args);
  }
}
