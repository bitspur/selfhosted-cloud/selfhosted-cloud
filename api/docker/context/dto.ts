/*
 *  File: /docker/context/dto.ts
 *  Project: api
 *  File Created: 23-02-2024 06:02:57
 *  Author: Clay Risser
 *  -----
 *  BitSpur (c) Copyright 2024
 *
 *  Licensed under the GNU Affero General Public License (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://www.gnu.org/licenses/agpl-3.0.en.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *  You can be released from the requirements of the license by purchasing
 *  a commercial license. Buying such a license is mandatory as soon as you
 *  develop commercial activities involving this software without disclosing
 *  the source code of your own applications.
 */

import { GraphQLJSON } from 'graphql-scalars';
import { ArgsType, Field, InputType, ObjectType } from 'type-graphql';

@ArgsType()
export class DockerContextCreateArgs {
  @Field(() => String, { description: 'CONTEXT' })
  context: string;

  @Field(() => DockerContextCreateOptions, { nullable: true, description: 'options for creating context' })
  options: DockerContextCreateOptions;
}

@InputType()
export class DockerContextCreateOptions {
  @Field(() => String, { nullable: true, description: 'Description of the context' })
  description?: string;

  @Field(() => String, { nullable: true, description: 'Create context from a named context' })
  from?: string;

  @Field(() => String, {
    nullable: true,
    description: 'Default orchestrator for stack operations to use with this context (swarm|kubernetes|all)',
  })
  defaultStackOrchestrator?: string;

  @Field(() => [String], { nullable: true, description: 'set the docker endpoint' })
  docker?: string[];

  @Field(() => [String], { nullable: true, description: 'set the kubernetes endpoint' })
  kubernetes?: string[];
}

@ArgsType()
export class DockerContextInspectArgs {
  @Field(() => [String], { description: 'contexts' })
  contexts: string[];
}

@ObjectType()
export class DockerContextInspectResult {
  @Field(() => String, { nullable: true, description: 'Name of the context' })
  name: string;

  @Field(() => GraphQLJSON, { nullable: true, description: 'Metadata of the context' })
  metadata: Record<string, any>;

  @Field(() => DockerContextEndpoints, { nullable: true, description: 'Endpoints of the context' })
  endpoints: DockerContextEndpoints;

  @Field(() => GraphQLJSON, { nullable: true, description: 'TLS Material of the context' })
  tlsMaterial: Record<string, string>;

  @Field(() => GraphQLJSON, { nullable: true, description: 'Storage of the context' })
  storage: Record<string, string>;
}

@ObjectType()
export class DockerContextEndpoints {
  @Field(() => GraphQLJSON, { nullable: true, description: 'Docker endpoint of the context' })
  docker: Record<string, string>;
}

@ArgsType()
export class DockerContextExportArgs {
  @Field(() => String, { description: 'docker context' })
  context: string;

  @Field(() => String, { description: 'export as a file' })
  file: string;

  @Field(() => String, { nullable: true, description: 'export as a kubeconfig file' })
  kubeconfig?: string;
}

@ArgsType()
export class DockerContextImportArgs {
  @Field(() => String, { description: 'docker context' })
  context: string;

  @Field(() => String, { description: 'import from a file' })
  source: string;
}

@ObjectType()
export class DockerContextLsResult {
  @Field(() => String, { nullable: true, description: 'Name of the context' })
  name: string;

  @Field(() => String, { nullable: true, description: 'Description of the context' })
  description: string;

  @Field(() => String, { nullable: true, description: 'Docker endpoint of the context' })
  dockerEndpoint: string;

  @Field(() => String, { nullable: true, description: 'Kubernetes endpoint of the context' })
  kubernetesEndpoint: string;

  @Field(() => String, { nullable: true, description: 'Orchestrator of the context' })
  orchestrator: string;
}

@ArgsType()
export class DockerContextRmArgs {
  @Field(() => [String], { description: 'contexts' })
  contexts: string[];

  @Field(() => Boolean, { nullable: true, description: 'force remove' })
  force?: boolean;
}
