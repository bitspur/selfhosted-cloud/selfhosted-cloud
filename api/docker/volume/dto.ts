/*
 *  File: /docker/volume/dto.ts
 *  Project: api
 *  File Created: 23-02-2024 06:02:57
 *  Author: Clay Risser
 *  -----
 *  BitSpur (c) Copyright 2024
 *
 *  Licensed under the GNU Affero General Public License (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://www.gnu.org/licenses/agpl-3.0.en.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *  You can be released from the requirements of the license by purchasing
 *  a commercial license. Buying such a license is mandatory as soon as you
 *  develop commercial activities involving this software without disclosing
 *  the source code of your own applications.
 */

import { Field, ArgsType, Int, ObjectType } from 'type-graphql';

@ArgsType()
export class DockerVolumeLsArgs {
  @Field(() => Boolean, {
    nullable: true,
    description: 'Display only cluster volumes, and use cluster volume list formatting',
  })
  cluster?: boolean;

  @Field(() => String, { nullable: true, description: 'Provide filter values (e.g. "dangling=true")' })
  filter?: string;
}

@ObjectType()
export class DockerVolumeLsResult {
  @Field(() => String, { nullable: true, description: 'Availability of the volume' })
  availability?: string;

  @Field(() => String, { nullable: true, description: 'Driver of the volume' })
  driver?: string;

  @Field(() => String, { nullable: true, description: 'Group of the volume' })
  group?: string;

  @Field(() => String, { nullable: true, description: 'Labels of the volume' })
  labels?: string;

  @Field(() => String, { nullable: true, description: 'Links of the volume' })
  links?: string;

  @Field(() => String, { nullable: true, description: 'Mount point of the volume' })
  mountpoint?: string;

  @Field(() => String, { nullable: true, description: 'Name of the volume' })
  name?: string;

  @Field(() => String, { nullable: true, description: 'Scope of the volume' })
  scope?: string;

  @Field(() => String, { nullable: true, description: 'Size of the volume' })
  size?: string;

  @Field(() => String, { nullable: true, description: 'Status of the volume' })
  status?: string;
}

@ArgsType()
export class DockerVolumeCreateArgs {
  @Field(() => String, { nullable: true, description: 'Availability of the volume' })
  availability?: string;

  @Field(() => String, { nullable: true, description: 'Driver of the volume' })
  driver?: string;

  @Field(() => String, { nullable: true, description: 'Group of the volume' })
  group?: string;

  @Field(() => String, { nullable: true, description: 'Labels of the volume' })
  labels?: string;

  @Field(() => String, { nullable: true, description: 'Links of the volume' })
  links?: string;

  @Field(() => String, { nullable: true, description: 'Name of the volume' })
  name: string;

  @Field(() => String, { nullable: true, description: 'Scope of the volume' })
  scope?: string;
}

@ObjectType()
export class DockerVolumeCreateResult {
  @Field(() => String, { nullable: true })
  stdout?: string;

  @Field(() => String, { nullable: true })
  stderr?: string;
}

@ArgsType()
export class DockerVolumeInspectArgs {
  @Field(() => [String], { nullable: true, description: 'Name of the volumes' })
  volumes: string[];
}

@ObjectType()
export class DockerVolumeInspectResult {
  @Field(() => String, { nullable: true, description: 'Creation time of the volume' })
  createdAt?: string;

  @Field(() => String, { nullable: true, description: 'Driver of the volume' })
  driver?: string;

  @Field(() => String, { nullable: true, description: 'Labels of the volume' })
  labels?: string;

  @Field(() => String, { nullable: true, description: 'Mount point of the volume' })
  mountpoint?: string;

  @Field(() => String, { nullable: true, description: 'Name of the volume' })
  name?: string;

  @Field(() => String, { nullable: true, description: 'Options of the volume' })
  options?: string;

  @Field(() => String, { nullable: true, description: 'Scope of the volume' })
  scope?: string;
}

@ArgsType()
export class DockerVolumeRmArgs {
  @Field(() => [String], { nullable: true, description: 'Name of the volumes' })
  volumes: string[];

  @Field(() => Boolean, { nullable: true, description: 'Force the removal of the volume' })
  force?: boolean;
}

@ObjectType()
export class DockerVolumeRmResult {
  @Field(() => String, { nullable: true })
  stdout?: string;

  @Field(() => String, { nullable: true })
  stderr?: string;
}

@ArgsType()
export class DockerVolumePruneArgs {
  @Field(() => Boolean, { nullable: true, description: 'Remove all unused local volumes' })
  all?: boolean;

  @Field(() => Int, { nullable: true, description: 'Filters to process' })
  filters?: String;

  @Field(() => Boolean, { nullable: true, description: 'Do not prompt for confirmation' })
  force?: boolean;
}

@ObjectType()
export class DockerVolumePruneResult {
  @Field(() => String, { nullable: true })
  stdout?: string;

  @Field(() => String, { nullable: true })
  stderr?: string;
}
