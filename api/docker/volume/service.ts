/*
 *  File: /docker/volume/service.ts
 *  Project: api
 *  File Created: 12-02-2024 07:38:25
 *  Author: Clay Risser
 *  -----
 *  BitSpur (c) Copyright 2024
 *
 *  Licensed under the GNU Affero General Public License (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://www.gnu.org/licenses/agpl-3.0.en.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *  You can be released from the requirements of the license by purchasing
 *  a commercial license. Buying such a license is mandatory as soon as you
 *  develop commercial activities involving this software without disclosing
 *  the source code of your own applications.
 */

import kebabCase from 'lodash.kebabcase';
import { ExecaError, execa } from 'execa';
import { Flag } from '../types';
import { GraphQLError } from 'graphql';
import { Logger, Injectable } from '@multiplatform.one/typegraphql';
import { camelCaseKeys } from '../utils';
import {
  DockerVolumeCreateArgs,
  DockerVolumeCreateResult,
  DockerVolumeInspectArgs,
  DockerVolumeInspectResult,
  DockerVolumeLsArgs,
  DockerVolumeLsResult,
  DockerVolumePruneArgs,
  DockerVolumePruneResult,
  DockerVolumeRmArgs,
  DockerVolumeRmResult,
} from './dto';

@Injectable()
export class DockerVolumeService {
  constructor(private readonly logger: Logger) {}

  async ls(args: DockerVolumeLsArgs = {}): Promise<DockerVolumeLsResult[]> {
    const { stderr, stdout } = await this.volume('ls', {
      filter: args.filter,
      format: `{{json .}}`,
    });
    if (stderr) {
      throw new GraphQLError(stderr);
    }

    return camelCaseKeys(JSON.parse(`[${stdout.split('\n').join(',')}]`));
  }

  async create(args: DockerVolumeCreateArgs): Promise<DockerVolumeCreateResult> {
    const { stderr, stdout } = await this.volume(
      'create',
      {
        driver: args.driver,
        availability: args.availability,
        Group: args.group,
        links: args.links,
        labels: args.labels,
        scope: args.scope,
      },
      [args.name],
    );
    if (stderr) throw new GraphQLError(stderr);
    return {
      stdout: stdout,
      stderr: stderr,
    };
  }

  async inspect(args: DockerVolumeInspectArgs): Promise<[DockerVolumeInspectResult]> {
    const { stderr, stdout } = await this.volume(
      'inspect',
      {
        format: `json`,
      },
      args.volumes,
    );
    if (stderr) {
      throw new GraphQLError(stderr);
    }
    return camelCaseKeys(JSON.parse(stdout));
  }

  async rm(args: DockerVolumeRmArgs): Promise<DockerVolumeRmResult> {
    const { stderr, stdout } = await this.volume('rm', { force: args.force }, args.volumes);
    if (stderr) {
      throw new GraphQLError(stderr);
    }
    return {
      stdout: stdout,
      stderr: stderr,
    };
  }

  async prune(args: DockerVolumePruneArgs): Promise<DockerVolumePruneResult> {
    const { stderr, stdout } = await this.volume('prune', {
      force: args.force,
    });
    if (stderr) {
      throw new GraphQLError(stderr);
    }
    return {
      stdout: stdout,
      stderr: stderr,
    };
  }

  private async volume(
    command: string,
    options: Record<string, Flag> = {},
    args: (string | undefined)[] = [],
  ): Promise<VolumeResult> {
    try {
      const p = execa('docker', [
        'volume',
        command,
        ...Object.entries(options).reduce<string[]>((acc, [key, value]: [string, Flag]) => {
          if (value === true) {
            acc.push(`--${kebabCase(key)}`);
          } else if (typeof value === 'string' || typeof value === 'number') {
            acc.push(`--${kebabCase(key)}`, value.toString());
          } else if (Array.isArray(value)) {
            value.forEach((v) => {
              acc.push(`--${kebabCase(key)}`, v.toString());
            });
          }
          return acc;
        }, []),
        ...((args || []).filter(Boolean) as string[]),
      ]);
      if (process.env.DEBUG !== '0') {
        p.stdout?.pipe(process.stdout);
        p.stderr?.pipe(process.stderr);
      }
      const proc = await p;
      return {
        stderr: proc.stderr,
        stdout: proc.stdout,
      };
    } catch (err) {
      const error = err as ExecaError;
      if (error.stderr) {
        return {
          stderr: error.stderr,
          stdout: error.stdout,
        };
      }
      throw err;
    }
  }
}

export interface VolumeResult {
  stderr: string;
  stdout: string;
  [key: string]: any;
}
