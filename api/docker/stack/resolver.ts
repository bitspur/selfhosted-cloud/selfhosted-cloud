/*
 *  File: /docker/stack/resolver.ts
 *  Project: api
 *  File Created: 23-02-2024 06:02:57
 *  Author: Clay Risser
 *  -----
 *  BitSpur (c) Copyright 2024
 *
 *  Licensed under the GNU Affero General Public License (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://www.gnu.org/licenses/agpl-3.0.en.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *  You can be released from the requirements of the license by purchasing
 *  a commercial license. Buying such a license is mandatory as soon as you
 *  develop commercial activities involving this software without disclosing
 *  the source code of your own applications.
 */

import { Authorized } from '@multiplatform.one/keycloak-typegraphql';
import { DockerStackService } from './service';
import { Injectable } from '@multiplatform.one/typegraphql';
import { Query, Mutation, Resolver, Args } from 'type-graphql';
import {
  DockerStackConfigArgs,
  DockerStackConfigResult,
  DockerStackDeployArgs,
  DockerStackDeployResult,
  DockerStackLsResult,
  DockerStackPsArgs,
  DockerStackPsResult,
  DockerStackRmArgs,
  DockerStackRmResult,
  DockerStackServicesArgs,
  DockerStackServicesResult,
} from './dto';

@Authorized()
@Injectable()
@Resolver()
export class DockerStackResolver {
  constructor(private readonly dockerStack: DockerStackService) {}

  @Mutation(() => DockerStackDeployResult)
  async dockerStackDeploy(@Args(() => DockerStackDeployArgs) args: DockerStackDeployArgs) {
    return this.dockerStack.deploy(args);
  }

  @Query(() => [DockerStackLsResult])
  async dockerStackLs() {
    return this.dockerStack.ls();
  }

  @Query(() => [DockerStackPsResult])
  async dockerStackPs(@Args(() => DockerStackPsArgs) args: DockerStackPsArgs) {
    return this.dockerStack.ps(args);
  }

  @Mutation(() => DockerStackRmResult)
  async dockerStackRm(@Args(() => DockerStackRmArgs) args: DockerStackRmArgs) {
    return this.dockerStack.rm(args);
  }

  @Query(() => [DockerStackServicesResult])
  async dockerStackServices(@Args(() => DockerStackServicesArgs) args: DockerStackServicesArgs) {
    return this.dockerStack.services(args);
  }

  @Query(() => DockerStackConfigResult)
  async dockerStackConfig(
    @Args(() => DockerStackConfigArgs) args: DockerStackConfigArgs,
  ): Promise<DockerStackConfigResult> {
    return this.dockerStack.config(args);
  }
}
