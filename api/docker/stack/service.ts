/*
 *  File: /docker/stack/service.ts
 *  Project: api
 *  File Created: 23-02-2024 06:02:57
 *  Author: Clay Risser
 *  -----
 *  BitSpur (c) Copyright 2024
 *
 *  Licensed under the GNU Affero General Public License (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://www.gnu.org/licenses/agpl-3.0.en.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *  You can be released from the requirements of the license by purchasing
 *  a commercial license. Buying such a license is mandatory as soon as you
 *  develop commercial activities involving this software without disclosing
 *  the source code of your own applications.
 */

import kebabCase from 'lodash.kebabcase';
import { ExecaError, execa } from 'execa';
import { Flag } from '../types';
import { GraphQLError } from 'graphql';
import { Logger, Injectable } from '@multiplatform.one/typegraphql';
import { camelCaseKeys } from '../utils';
import {
  DockerStackConfigArgs,
  DockerStackConfigResult,
  DockerStackDeployArgs,
  DockerStackLsResult,
  DockerStackPsArgs,
  DockerStackPsResult,
  DockerStackRmArgs,
  DockerStackRmResult,
  DockerStackServicesArgs,
  DockerStackServicesResult,
} from './dto';

@Injectable()
export class DockerStackService {
  constructor(private readonly logger: Logger) {}

  async deploy(args: DockerStackDeployArgs): Promise<StackResult> {
    return this.stack(
      'deploy',
      {
        composeFile: args.composeFile,
        prune: args.prune,
        resolveImage: args.resolveImage,
        withRegistryAuth: args.withRegistryAuth,
      },
      [args.name],
    );
  }

  async ls(): Promise<DockerStackLsResult[]> {
    const { stdout, stderr } = await this.stack('ls', {
      format: '{{json .}}',
    });
    if (stderr) throw new GraphQLError(stderr);
    return camelCaseKeys(JSON.parse(`[${stdout.split('\n').join(',')}]`));
  }

  async ps(args: DockerStackPsArgs = {}): Promise<DockerStackPsResult[]> {
    const { stdout, stderr } = await this.stack(
      'ps',
      {
        filter: args.filter,
        format: '{{json .}}',
        noResolve: args.noResolve,
        noTrunc: args.noTrunc,
      },
      [args.name],
    );
    if (stderr) throw new GraphQLError(stderr);
    return camelCaseKeys(JSON.parse(`[${stdout.split('\n').join(',')}]`));
  }

  async rm(args: DockerStackRmArgs): Promise<DockerStackRmResult> {
    const { stdout, stderr } = await this.stack('rm', {}, [args.name]);
    if (stderr) throw new GraphQLError(stderr);
    return {
      stdout,
      stderr,
    };
  }

  async services(args: DockerStackServicesArgs): Promise<DockerStackServicesResult[]> {
    const { stdout, stderr } = await this.stack(
      'services',
      {
        filter: args.filter,
        format: '{{json .}}',
      },
      [args.name],
    );
    if (stderr) throw new GraphQLError(stderr);
    return camelCaseKeys(JSON.parse(`[${stdout.split('\n').join(',')}]`));
  }

  async config(args: DockerStackConfigArgs): Promise<DockerStackConfigResult> {
    const { stdout, stderr } = await this.stack(
      'config',
      {
        skipInterpolation: args.skipInterpolation,
        composeFile: args.composeFile,
      },
      [],
    );
    if (stderr) throw new GraphQLError(stderr);
    return {
      stdout,
      stderr,
    };
  }

  private async stack(
    command: string,
    options: Record<string, Flag> = {},
    args: (string | undefined)[] = [],
  ): Promise<StackResult> {
    try {
      const p = execa('docker', [
        'stack',
        command,
        ...Object.entries(options).reduce<string[]>((acc, [key, value]: [string, Flag]) => {
          if (value === true) {
            acc.push(`--${kebabCase(key)}`);
          } else if (typeof value === 'string' || typeof value === 'number') {
            acc.push(`--${kebabCase(key)}`, value.toString());
          } else if (Array.isArray(value)) {
            value.forEach((v) => {
              acc.push(`--${kebabCase(key)}`, v.toString());
            });
          }
          return acc;
        }, []),
        ...((args || []).filter(Boolean) as string[]),
      ]);
      if (process.env.DEBUG !== '0') {
        p.stdout?.pipe(process.stdout);
        p.stderr?.pipe(process.stderr);
      }
      const proc = await p;
      return {
        stderr: proc.stderr,
        stdout: proc.stdout,
      };
    } catch (err) {
      const error = err as ExecaError;
      if (error.stderr) {
        return {
          stderr: error.stderr,
          stdout: error.stdout,
        };
      }
      throw err;
    }
  }
}

export interface StackResult {
  stderr: string;
  stdout: string;
  [key: string]: any;
}
