/*
 *  File: /docker/builder/dto.ts
 *  Project: api
 *  File Created: 23-02-2024 06:02:57
 *  Author: Clay Risser
 *  -----
 *  BitSpur (c) Copyright 2024
 *
 *  Licensed under the GNU Affero General Public License (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://www.gnu.org/licenses/agpl-3.0.en.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *  You can be released from the requirements of the license by purchasing
 *  a commercial license. Buying such a license is mandatory as soon as you
 *  develop commercial activities involving this software without disclosing
 *  the source code of your own applications.
 */

import { ArgsType, Field, InputType } from 'type-graphql';

@ArgsType()
export class DockerBuilderBuildArgs {
  @Field(() => String, {
    description: "Name of the Dockerfile. It can be a path, URL, or '-' (Default is 'PATH/Dockerfile')",
  })
  source: string;

  @Field(() => DockerBuilderBuildOptions, { nullable: true, description: 'Build options' })
  options?: DockerBuilderBuildOptions;
}

@InputType()
export class DockerBuilderBuildOptions {
  @Field(() => String, { nullable: true, description: 'Add a custom host-to-IP mapping (host:ip)' })
  addHost?: string;

  @Field(() => String, { nullable: true, description: 'Set build-time variables' })
  buildArg?: string;

  @Field(() => String, { nullable: true, description: 'Images to consider as cache sources' })
  cacheFrom?: string;

  @Field(() => String, { nullable: true, description: 'Optional parent cgroup for the container' })
  cgroupParent?: string;

  @Field(() => String, { nullable: true, description: 'Compress the build context using gzip' })
  compress?: string;

  @Field(() => Number, { nullable: true, description: 'Limit the CPU CFS (Completely Fair Scheduler) period' })
  cpuPeriod?: number;

  @Field(() => Number, { nullable: true, description: 'Limit the CPU CFS (Completely Fair Scheduler) quota' })
  cpuQuota?: number;

  @Field(() => Number, { nullable: true, description: 'CPU shares (relative weight)' })
  cpuShares?: number;

  @Field(() => String, { nullable: true, description: 'CPUs in which to allow execution (0-3, 0,1)' })
  cpusetCpus?: string;

  @Field(() => String, { nullable: true, description: 'MEMs in which to allow execution (0-3, 0,1)' })
  cpusetMems?: string;

  @Field(() => String, { nullable: true, description: 'Skip image verification (default true)' })
  disableContentTrust?: string;

  @Field(() => String, { nullable: true, description: "Name of the Dockerfile (Default is 'PATH/Dockerfile')" })
  file?: string;

  @Field(() => String, { nullable: true, description: 'Always remove intermediate containers' })
  forceRm?: string;

  @Field(() => String, { nullable: true, description: 'Write the image ID to the file' })
  iidfile?: string;

  @Field(() => String, { nullable: true, description: 'Container isolation technology' })
  isolation?: string;

  @Field(() => String, { nullable: true, description: 'Set metadata for an image' })
  label?: string;

  @Field(() => Number, { nullable: true, description: 'Memory limit' })
  memory?: number;

  @Field(() => Number, {
    nullable: true,
    description: "Swap limit equal to memory plus swap: '-1' to enable unlimited swap",
  })
  memorySwap?: number;

  @Field(() => String, {
    nullable: true,
    description: 'Set the networking mode for the RUN instructions during build (default "default")',
  })
  network?: string;

  @Field(() => String, { nullable: true, description: 'Do not use cache when building the image' })
  noCache?: string;

  @Field(() => String, { nullable: true, description: 'Always attempt to pull a newer version of the image' })
  pull?: string;

  @Field(() => String, { nullable: true, description: 'Suppress the build output and print image ID on success' })
  quiet?: string;

  @Field(() => String, {
    nullable: true,
    description: 'Remove intermediate containers after a successful build (default true)',
  })
  rm?: string;

  @Field(() => String, { nullable: true, description: 'Security options' })
  securityOpt?: string;

  @Field(() => Number, { nullable: true, description: 'Size of /dev/shm' })
  shmSize?: number;

  @Field(() => String, { nullable: true, description: "Name and optionally a tag in the 'name:tag' format" })
  tag?: string;

  @Field(() => String, { nullable: true, description: 'Set the target build stage to build.' })
  target?: string;

  @Field(() => String, { nullable: true, description: 'Ulimit options (default [])' })
  ulimit?: string;
}

@ArgsType()
export class DockerBuilderPruneOptions {
  @Field(() => Boolean, { nullable: true, description: 'Remove all unused build cache, not just dangling ones' })
  all?: boolean;

  @Field(() => String, { nullable: true, description: 'Provide filter values (e.g. "until=24h")' })
  filter?: string;

  @Field(() => Boolean, { nullable: true, description: 'Do not prompt for confirmation' })
  force?: boolean;

  @Field(() => Number, { nullable: true, description: 'Amount of disk space to keep for cache' })
  keepStorage?: number;
}
