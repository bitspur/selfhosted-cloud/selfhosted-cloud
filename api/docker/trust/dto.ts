/*
 *  File: /docker/trust/dto.ts
 *  Project: api
 *  File Created: 23-02-2024 06:02:57
 *  Author: Clay Risser
 *  -----
 *  BitSpur (c) Copyright 2024
 *
 *  Licensed under the GNU Affero General Public License (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://www.gnu.org/licenses/agpl-3.0.en.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *  You can be released from the requirements of the license by purchasing
 *  a commercial license. Buying such a license is mandatory as soon as you
 *  develop commercial activities involving this software without disclosing
 *  the source code of your own applications.
 */

import { ArgsType, Field, ObjectType } from 'type-graphql';

@ArgsType()
export class DockerTrustInspectArgs {
  @Field(() => [String], { description: 'NAME' })
  images: string[];
}

@ObjectType()
export class DockerTrustInspectResult {
  @Field(() => String, { description: 'NAME' })
  name: string;

  @Field(() => [DockerTrustSignedTags], { description: 'SignedTags' })
  signedTags: DockerTrustSignedTags[];

  @Field(() => [String], { description: 'Signers' })
  signers: string[];

  @Field(() => [DockerTrustAdministrativeKeys], { description: 'AdministrativeKeys' })
  administrativeKeys: DockerTrustAdministrativeKeys[];
}

@ObjectType()
export class DockerTrustSignedTags {
  @Field(() => String, { description: 'SignedTag' })
  signedTag: string;

  @Field(() => String, { description: 'Digest' })
  digest: string;

  @Field(() => [String], { description: 'Signers' })
  signers: string[];
}

@ObjectType()
export class DockerTrustAdministrativeKeys {
  @Field(() => String, { description: 'Name' })
  name: string;

  @Field(() => [DockerTrustKeys], { description: 'Keys' })
  keys: DockerTrustKeys[];
}

@ObjectType()
export class DockerTrustKeys {
  @Field(() => String, { description: 'ID' })
  id: string;
}
