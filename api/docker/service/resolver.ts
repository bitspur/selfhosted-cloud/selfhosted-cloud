/*
 *  File: /docker/service/resolver.ts
 *  Project: api
 *  File Created: 12-02-2024 07:38:25
 *  Author: Clay Risser
 *  -----
 *  BitSpur (c) Copyright 2024
 *
 *  Licensed under the GNU Affero General Public License (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://www.gnu.org/licenses/agpl-3.0.en.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *  You can be released from the requirements of the license by purchasing
 *  a commercial license. Buying such a license is mandatory as soon as you
 *  develop commercial activities involving this software without disclosing
 *  the source code of your own applications.
 */

import { Authorized } from '@multiplatform.one/keycloak-typegraphql';
import { Ctx, Injectable } from '@multiplatform.one/typegraphql';
import { DOCKER_SERVICE_EXEC, pubSub } from '../../pubSub';
import { DockerServiceService } from './service';
import { Resolver, Args, Query, Mutation, Subscription, Root, SubscribeResolverData } from 'type-graphql';
import {
  DockerServiceInspectArgs,
  DockerServiceInspectResult,
  DockerServicePsArgs,
  DockerServicePsResult,
  DockerServiceLsArgs,
  DockerServiceLsResult,
  DockerServiceRollbackResult,
  DockerServiceScaleArgs,
  DockerServiceScaleResult,
  DockerServiceRollbackArgs,
  DockerServiceRmResult,
  DockerServiceRmArgs,
  DockerServiceLogsArgs,
  DockerServiceLogsResult,
  DockerServiceCreateArgs,
  DockerServiceCreateResult,
  DockerServiceUpdateArgs,
  DockerServiceUpdateResult,
  DockerServiceExecArgs,
  DockerServiceExecResult,
} from './dto';

@Authorized()
@Injectable()
@Resolver()
export class DockerServiceResolver {
  constructor(private readonly dockerService: DockerServiceService) {}

  @Query(() => DockerServiceInspectResult)
  async dockerServiceInspect(@Args(() => DockerServiceInspectArgs) args: DockerServiceInspectArgs) {
    return this.dockerService.inspect(args);
  }

  @Query(() => [DockerServicePsResult])
  async dockerServicePs(@Args(() => DockerServicePsArgs) args: DockerServicePsArgs) {
    return this.dockerService.ps(args);
  }

  @Mutation(() => DockerServiceCreateResult)
  async dockerServiceCreate(@Args(() => DockerServiceCreateArgs) args: DockerServiceCreateArgs) {
    return this.dockerService.create(args);
  }

  @Mutation(() => DockerServiceUpdateResult)
  async dockerServiceUpdate(@Args(() => DockerServiceUpdateArgs) args: DockerServiceUpdateArgs) {
    return this.dockerService.update(args);
  }

  @Query(() => [DockerServiceLsResult])
  async dockerServiceLs(@Args(() => DockerServiceLsArgs) args: DockerServiceLsArgs) {
    return this.dockerService.ls(args);
  }

  @Mutation(() => DockerServiceScaleResult)
  async dockerServiceScale(@Args(() => DockerServiceScaleArgs) args: DockerServiceScaleArgs) {
    return this.dockerService.scale(args);
  }

  @Mutation(() => DockerServiceRollbackResult)
  async dockerServiceRollback(@Args(() => DockerServiceRollbackArgs) args: DockerServiceRollbackArgs) {
    return this.dockerService.rollback(args);
  }

  @Mutation(() => [DockerServiceRmResult])
  async dockerServiceRm(@Args(() => DockerServiceRmArgs) args: DockerServiceRmArgs) {
    return this.dockerService.rm(args);
  }

  @Query(() => DockerServiceLogsResult)
  async dockerServiceLogs(@Args(() => DockerServiceLogsArgs) args: DockerServiceLogsArgs) {
    return this.dockerService.logs(args);
  }

  @Subscription(() => [DockerServiceExecResult], {
    async *subscribe({ args, context }: SubscribeResolverData<DockerServiceResolver, DockerServiceExecArgs>) {
      const ctx = context as Ctx;
      const container = ctx.container;
      const dockerServiceService = container.resolve(DockerServiceService);
      const dockerServiceExecRepeater = pubSub.subscribe(DOCKER_SERVICE_EXEC);
      const { serviceExecId, containers } = await dockerServiceService.exec(args);
      let payload: DockerServiceExecResult[] = containers.map((container) => ({
        containerId: container.id,
      }));
      for await (const dockerServiceExecPayload of dockerServiceExecRepeater) {
        if (dockerServiceExecPayload.serviceExecId !== serviceExecId) continue;
        payload = [
          ...payload.map((payload) => {
            if (payload.containerId !== dockerServiceExecPayload.containerId) return payload;
            return {
              ...payload,
              stderr: dockerServiceExecPayload.stderr,
              stdout: dockerServiceExecPayload.stdout,
              exitCode: dockerServiceExecPayload.exitCode,
            };
          }),
        ];
        yield payload;
        if (payload.every((p) => p.exitCode !== undefined)) return;
      }
    },
  })
  async dockerServiceExec(
    @Args(() => DockerServiceExecArgs) _args: DockerServiceExecArgs,
    @Root() payload: DockerServiceExecResult[],
  ) {
    return payload;
  }
}
