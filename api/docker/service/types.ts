/*
 *  File: /docker/service/types.ts
 *  Project: api
 *  File Created: 20-01-2024 12:09:44
 *  Author: Clay Risser
 *  -----
 *  BitSpur (c) Copyright 2024
 *
 *  Licensed under the GNU Affero General Public License (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://www.gnu.org/licenses/agpl-3.0.en.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *  You can be released from the requirements of the license by purchasing
 *  a commercial license. Buying such a license is mandatory as soon as you
 *  develop commercial activities involving this software without disclosing
 *  the source code of your own applications.
 */

export interface IDockerServiceInspect {
  ID?: string;
  Version?: {
    Index?: number;
  };
  CreatedAt?: string;
  UpdatedAt?: string;
  Spec?: {
    Name?: string;
    Labels?: {
      [key: string]: string;
    };
    TaskTemplate?: {
      ContainerSpec?: {
        Image?: string;
        Labels?: {
          [key: string]: string;
        };
        Env?: string[];
        Privileges?: {
          CredentialSpec?: any;
          SELinuxContext?: any;
        };
        StopGracePeriod?: number;
        DNSConfig?: any;
        Isolation?: string;
      };
      Resources?: any;
      RestartPolicy?: {
        Condition?: string;
        Delay?: number;
        MaxAttempts?: number;
      };
      Placement?: {
        Platforms?: {
          Architecture?: string;
          OS?: string;
        }[];
      };
      Networks?: {
        Target?: string;
        Aliases?: string[];
      }[];
      ForceUpdate?: number;
      Runtime?: string;
    };
    Mode?: {
      Replicated?: {
        Replicas?: number;
      };
    };
    UpdateConfig?: {
      Parallelism?: number;
      FailureAction?: string;
      Monitor?: number;
      MaxFailureRatio?: number;
      Order?: string;
    };
    RollbackConfig?: {
      Parallelism?: number;
      FailureAction?: string;
      Monitor?: number;
      MaxFailureRatio?: number;
      Order?: string;
    };
    EndpointSpec?: {
      Mode?: string;
      Ports?: {
        Protocol?: string;
        TargetPort?: number;
        PublishedPort?: number;
        PublishMode?: string;
      }[];
    };
  };
  Endpoint?: {
    Spec?: {
      Mode?: string;
      Ports?: {
        Protocol?: string;
        TargetPort?: number;
        PublishedPort?: number;
        PublishMode?: string;
      }[];
    };
    Ports?: {
      Protocol?: string;
      TargetPort?: number;
      PublishedPort?: number;
      PublishMode?: string;
    }[];
    VirtualIPs?: {
      NetworkID?: string;
      Addr?: string;
    }[];
  };
}

export interface IDockerServicePsResult {
  CurrentState?: string;
  DesiredState?: string;
  Error?: string;
  ID?: string;
  Image?: string;
  Name?: string;
  Node?: string;
  Ports?: string;
}
