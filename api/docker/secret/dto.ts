/*
 *  File: /docker/secret/dto.ts
 *  Project: api
 *  File Created: 23-02-2024 06:02:57
 *  Author: Clay Risser
 *  -----
 *  BitSpur (c) Copyright 2024
 *
 *  Licensed under the GNU Affero General Public License (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://www.gnu.org/licenses/agpl-3.0.en.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *  You can be released from the requirements of the license by purchasing
 *  a commercial license. Buying such a license is mandatory as soon as you
 *  develop commercial activities involving this software without disclosing
 *  the source code of your own applications.
 */

import { Field, ArgsType, ObjectType } from 'type-graphql';
import { GraphQLJSONObject } from 'graphql-scalars';

@ArgsType()
export class DockerSecretCreateArgs {
  @Field(() => String, { nullable: true, description: 'Name of the Docker secret' })
  name: string;

  @Field(() => String, { nullable: true, description: 'file path of the Docker secret' })
  filePath: string;

  @Field(() => String, { nullable: true, description: 'Secret driver' })
  driver: string;

  @Field(() => [String], { nullable: true, description: 'Secret labels' })
  labels: string[];

  @Field(() => String, { nullable: true, description: 'Template driver' })
  templateDriver: string;
}

@ObjectType()
export class DockerSecretCreateResult {
  @Field(() => String, { description: 'ID of the Docker secret' })
  id: string;
}

@ArgsType()
export class DockerSecretLsArgs {
  @Field(() => String, { nullable: true, description: 'Provide filter values (e.g. "dangling=true")' })
  filter?: string;
}

@ObjectType()
export class DockerSecretLsResult {
  @Field(() => String, { nullable: true, description: 'ID of the secret' })
  id?: string;

  @Field(() => String, { nullable: true, description: 'Name of the secret' })
  name?: string;

  @Field(() => String, { nullable: true, description: 'Driver of the secret' })
  driver?: string;

  @Field(() => String, { nullable: true, description: 'Created time of the secret' })
  createdAt?: string;

  @Field(() => String, { nullable: true, description: 'Updated time of the secret' })
  updatedAt?: string;
}

@ArgsType()
export class DockerSecretRmArgs {
  @Field(() => [String], { nullable: true, description: 'Name of the secrets' })
  secret: string[];
}

@ObjectType()
export class DockerSecretRmResult {
  @Field(() => String, { nullable: true })
  stdout?: string;

  @Field(() => String, { nullable: true })
  stderr?: string;
}

@ArgsType()
export class DockerSecretInspectArgs {
  @Field(() => String, { nullable: true, description: 'Name of the secret' })
  secret: string;
}

@ObjectType()
export class DockerSecretInspectResult {
  @Field(() => String, { nullable: true, description: 'ID of the secret' })
  id: string;

  @Field(() => DockerSecretInspectVersion, { nullable: true, description: 'Version of the secret' })
  version: DockerSecretInspectVersion;

  @Field(() => String, { nullable: true, description: 'Created time of the secret' })
  createdAt: string;

  @Field(() => String, { nullable: true, description: 'Updated time of the secret' })
  updatedAt: string;

  @Field(() => DockerSecretInspectSpec, { nullable: true, description: 'Spec of the secret' })
  spec: DockerSecretInspectSpec;
}

@ObjectType()
export class DockerSecretInspectVersion {
  @Field(() => Number, { nullable: true, description: 'Version of the secret' })
  index?: number;
}

@ObjectType()
export class DockerSecretInspectSpec {
  @Field(() => String, { nullable: true, description: 'Name of the secret' })
  name: string;

  @Field(() => GraphQLJSONObject, { nullable: true, description: 'Labels of the secret' })
  labels: Record<string, string>;
}
