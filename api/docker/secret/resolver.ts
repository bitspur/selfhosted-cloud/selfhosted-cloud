/*
 *  File: /docker/secret/resolver.ts
 *  Project: api
 *  File Created: 23-02-2024 06:02:57
 *  Author: Clay Risser
 *  -----
 *  BitSpur (c) Copyright 2024
 *
 *  Licensed under the GNU Affero General Public License (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://www.gnu.org/licenses/agpl-3.0.en.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *  You can be released from the requirements of the license by purchasing
 *  a commercial license. Buying such a license is mandatory as soon as you
 *  develop commercial activities involving this software without disclosing
 *  the source code of your own applications.
 */

import { Authorized } from '@multiplatform.one/keycloak-typegraphql';
import { DockerSecretService } from './service';
import { Injectable } from '@multiplatform.one/typegraphql';
import { Mutation, Resolver, Args, Query, Arg } from 'type-graphql';
import {
  DockerSecretCreateArgs,
  DockerSecretCreateResult,
  DockerSecretInspectResult,
  DockerSecretLsResult,
  DockerSecretRmArgs,
  DockerSecretRmResult,
} from './dto';

@Authorized()
@Injectable()
@Resolver()
export class DockerSecretResolver {
  constructor(private readonly dockerSecret: DockerSecretService) {}

  @Mutation(() => DockerSecretCreateResult)
  async dockerSecretCreate(@Args(() => DockerSecretCreateArgs) args: DockerSecretCreateArgs) {
    return this.dockerSecret.create(args);
  }

  @Query(() => [DockerSecretLsResult])
  async dockerSecretLs() {
    return this.dockerSecret.ls();
  }

  @Mutation(() => DockerSecretRmResult)
  async dockerSecretRm(@Args(() => DockerSecretRmArgs) args: DockerSecretRmArgs) {
    return this.dockerSecret.rm(args);
  }

  @Query(() => [DockerSecretInspectResult])
  async dockerSecretInspect(@Arg('id', () => String) id: string) {
    return this.dockerSecret.inspect(id);
  }
}
