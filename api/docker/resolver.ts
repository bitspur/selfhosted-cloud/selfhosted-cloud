/*
 *  File: /docker/resolver.ts
 *  Project: api
 *  File Created: 24-02-2024 09:27:50
 *  Author: Clay Risser
 *  -----
 *  BitSpur (c) Copyright 2024
 *
 *  Licensed under the GNU Affero General Public License (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://www.gnu.org/licenses/agpl-3.0.en.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *  You can be released from the requirements of the license by purchasing
 *  a commercial license. Buying such a license is mandatory as soon as you
 *  develop commercial activities involving this software without disclosing
 *  the source code of your own applications.
 */

import { Authorized } from '@multiplatform.one/keycloak-typegraphql';
import { DockerService } from './dockerService';
import { Injectable, Ctx } from '@multiplatform.one/typegraphql';
import { Resolver, Args, Query, Mutation, Subscription, SubscribeResolverData, Root } from 'type-graphql';
import {
  DockerExecArgs,
  DockerExecResult,
  DockerInspectArgs,
  DockerInspectResult,
  DockerPsArgs,
  DockerPsResult,
  DockerCommitArgs,
  DockerAttachArgs,
  DockerAttachResult,
  DockerCreateResult,
  DockerCreateArgs,
  DockerCpResult,
  DockerCpArgs,
  DockerEventsArgs,
  DockerEventsResult,
  DockerRmArgs,
  DockerRmResult,
  DockerInfoResult,
  DockerImagesArgs,
  DockerImagesResult,
  DockerRmiArgs,
  DockerRmiResult,
  DockerVersionResult,
  DockerPullArgs,
  DockerPullResult,
  DockerSearchResult,
  DockerSearchArgs,
  DockerPushResult,
  DockerRenameResult,
  DockerRenameArgs,
  DockerRestartResult,
  DockerRestartArgs,
  DockerPushArgs,
  DockerLogsArgs,
  DockerLogsResult,
  DockerKillArgs,
  DockerKillResult,
  DockerHistoryResult,
  DockerHistoryArgs,
  DockerStopArgs,
  DockerStopResult,
  DockerStartArgs,
  DockerStartResult,
  DockerSaveArgs,
  DockerSaveResult,
  DockerTagArgs,
  DockerTagResult,
  DockerLoginArgs,
  DockerLoginResult,
  DockerLogoutArgs,
  DockerLogoutResult,
  DockerPauseArgs,
  DockerPauseResult,
  DockerTopArgs,
  DockerTopResult,
  DockerUnpauseArgs,
  DockerUnpauseResult,
  DockerWaitArgs,
  DockerWaitResult,
  DockerExportArgs,
  DockerExportResult,
  DockerUpdateArgs,
  DockerUpdateResult,
  DockerImportArgs,
  DockerImportResult,
  DockerDiffArgs,
  DockerDiffResult,
  DockerLoadArgs,
  DockerLoadResult,
  DockerPortArgs,
  DockerPortResult,
  DockerBuildArgs,
  DockerBuildResult,
  DockerExecForServiceResult,
  DockerExecForServiceArgs,
} from './dto';

@Authorized()
@Injectable()
@Resolver()
export class DockerResolver {
  constructor(private readonly docker: DockerService) {}

  @Query(() => DockerInspectResult)
  async dockerInspect(@Args(() => DockerInspectArgs) args: DockerInspectArgs) {
    return this.docker.inspect(args);
  }

  @Query(() => [DockerPsResult])
  async dockerPs(@Args(() => DockerPsArgs) args: DockerPsArgs) {
    return this.docker.ps(args);
  }

  @Mutation(() => DockerLoadResult)
  async dockerLoad(@Args(() => DockerLoadArgs) args: DockerLoadArgs) {
    return this.docker.load(args);
  }

  @Mutation(() => DockerBuildResult)
  async dockerBuild(@Args(() => DockerBuildArgs) args: DockerBuildArgs) {
    return this.docker.build(args);
  }

  @Mutation(() => DockerImportResult)
  async dockerImport(@Args(() => DockerImportArgs) args: DockerImportArgs) {
    return this.docker.import(args);
  }

  @Mutation(() => DockerStartResult)
  async dockerStart(@Args(() => DockerStartArgs) args: DockerStartArgs) {
    return this.docker.start(args);
  }

  @Mutation(() => DockerSaveResult)
  async dockerSave(@Args(() => DockerSaveArgs) args: DockerSaveArgs) {
    return this.docker.save(args);
  }

  @Query(() => [DockerTopResult])
  async dockerTop(@Args(() => DockerTopArgs) args: DockerTopArgs) {
    return this.docker.top(args);
  }

  @Mutation(() => DockerKillResult)
  async dockerKill(@Args(() => DockerKillArgs) args: DockerKillArgs) {
    return this.docker.kill(args);
  }

  @Mutation(() => DockerUpdateResult)
  async dockerUpdate(@Args(() => DockerUpdateArgs) args: DockerUpdateArgs) {
    return this.docker.update(args);
  }

  @Mutation(() => DockerStopResult)
  async dockerStop(@Args(() => DockerStopArgs) args: DockerStopArgs) {
    return this.docker.stop(args);
  }

  @Mutation(() => DockerTagResult)
  async dockerTag(@Args(() => DockerTagArgs) args: DockerTagArgs) {
    return this.docker.tag(args);
  }

  @Mutation(() => DockerAttachResult)
  async dockerAttach(@Args(() => DockerAttachArgs) args: DockerAttachArgs) {
    return this.docker.attach(args);
  }

  @Mutation(() => DockerCreateResult)
  async dockerCreate(@Args(() => DockerCreateArgs) args: DockerCreateArgs) {
    return this.docker.create(args);
  }

  @Query(() => [DockerSearchResult])
  async dockerSearch(@Args(() => DockerSearchArgs) args: DockerSearchArgs) {
    return this.docker.search(args);
  }

  @Query(() => [DockerInfoResult])
  async dockerInfo() {
    return this.docker.info();
  }

  @Subscription(() => DockerExecResult, {
    async *subscribe({ args, context }: SubscribeResolverData<DockerResolver, DockerExecArgs>) {
      const ctx = context as Ctx;
      const container = ctx.container;
      const dockerService = container.resolve(DockerService);
      yield {};
      yield await dockerService.exec(args);
    },
  })
  dockerExec(@Args(() => DockerExecArgs) _args: DockerExecArgs, @Root() payload: DockerExecResult) {
    return payload;
  }

  @Mutation(() => DockerExecForServiceResult)
  async dockerExecForService(
    @Args(() => DockerExecForServiceArgs) args: DockerExecForServiceArgs,
  ): Promise<DockerExecForServiceResult> {
    this.docker.exec(args);
    return {
      serviceExecId: args.serviceExecId,
    };
  }

  @Mutation(() => String)
  async dockerCommit(@Args(() => DockerCommitArgs) args: DockerCommitArgs) {
    return this.docker.commit(args);
  }

  @Mutation(() => DockerCpResult)
  async dockerCp(@Args(() => DockerCpArgs) args: DockerCpArgs) {
    return this.docker.cp(args);
  }

  @Mutation(() => DockerEventsResult)
  async dockerEvents(@Args(() => DockerEventsArgs) args: DockerEventsArgs) {
    return this.docker.events(args);
  }

  @Mutation(() => DockerRmResult)
  async dockerRm(@Args(() => DockerRmArgs) args: DockerRmArgs) {
    return this.docker.rm(args);
  }

  @Mutation(() => DockerRmiResult)
  async dockerRmi(@Args(() => DockerRmiArgs) args: DockerRmiArgs) {
    return this.docker.rmi(args);
  }

  @Query(() => [DockerImagesResult])
  async dockerImages(@Args(() => DockerImagesArgs) args: DockerImagesArgs) {
    return this.docker.images(args);
  }

  @Mutation(() => DockerWaitResult)
  async dockerWait(@Args(() => DockerWaitArgs) args: DockerWaitArgs) {
    return this.docker.wait(args);
  }

  @Query(() => [DockerDiffResult])
  async dockerDiff(@Args(() => DockerDiffArgs) args: DockerDiffArgs) {
    return this.docker.diff(args);
  }

  @Mutation(() => DockerPortResult)
  async dockerPort(@Args(() => DockerPortArgs) args: DockerPortArgs) {
    return this.docker.port(args);
  }

  @Mutation(() => DockerExportResult)
  async dockerExport(@Args(() => DockerExportArgs) args: DockerExportArgs) {
    return this.docker.export(args);
  }

  @Query(() => DockerVersionResult)
  async dockerVersion() {
    return this.docker.version();
  }

  @Mutation(() => DockerPullResult)
  async dockerPull(@Args(() => DockerPullArgs) args: DockerPullArgs) {
    return this.docker.pull(args);
  }

  @Mutation(() => DockerPushResult)
  async dockerPush(@Args(() => DockerPushArgs) args: DockerPushArgs) {
    return this.docker.push(args);
  }

  @Mutation(() => DockerRenameResult)
  async dockerRename(@Args(() => DockerRenameArgs) args: DockerRenameArgs) {
    return this.docker.rename(args);
  }

  @Query(() => DockerRestartResult)
  async dockerRestart(@Args(() => DockerRestartArgs) args: DockerRestartArgs) {
    return this.docker.restart(args);
  }

  @Mutation(() => DockerLoginResult)
  async dockerLogin(@Args(() => DockerLoginArgs) args: DockerLoginArgs) {
    return this.docker.login(args);
  }

  @Mutation(() => DockerLogoutResult)
  async dockerLogout(@Args(() => DockerLogoutArgs) args: DockerLogoutArgs) {
    return this.docker.logout(args);
  }

  @Mutation(() => DockerPauseResult)
  async dockerPause(@Args(() => DockerPauseArgs) args: DockerPauseArgs) {
    return this.docker.pause(args);
  }

  @Mutation(() => DockerUnpauseResult)
  async dockerUnpause(@Args(() => DockerUnpauseArgs) args: DockerUnpauseArgs) {
    return this.docker.unpause(args);
  }

  @Query(() => DockerLogsResult)
  async dockerLogs(@Args(() => DockerLogsArgs) args: DockerLogsArgs) {
    return this.docker.logs(args);
  }

  @Query(() => [DockerHistoryResult])
  async dockerHistory(@Args(() => DockerHistoryArgs) args: DockerHistoryArgs) {
    return this.docker.history(args);
  }

  // @Mutation(() => DockerContainerCreateResult)
  // async dockerRun(@Args(() => DockerContainerCreateArgs) args: DockerContainerCreateArgs) {
  //   return this.docker.run(args);
  // }

  // @Query(() => [DockerContainerStatsResult])
  // async dockerStats(@Args(() => DockerStatsArgs) args: DockerStatsArgs) {
  //   return this.docker.stats(args);
  // }
}
