/*
 *  File: /docker/network/resolver.ts
 *  Project: api
 *  File Created: 12-02-2024 07:38:25
 *  Author: Clay Risser
 *  -----
 *  BitSpur (c) Copyright 2024
 *
 *  Licensed under the GNU Affero General Public License (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://www.gnu.org/licenses/agpl-3.0.en.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *  You can be released from the requirements of the license by purchasing
 *  a commercial license. Buying such a license is mandatory as soon as you
 *  develop commercial activities involving this software without disclosing
 *  the source code of your own applications.
 */

import { Authorized } from '@multiplatform.one/keycloak-typegraphql';
import { DockerNetworkService } from './service';
import { Injectable } from '@multiplatform.one/typegraphql';
import { Mutation, Resolver, Args, Query } from 'type-graphql';
import {
  DockerNetworkConnectArgs,
  DockerNetworkConnectResult,
  DockerNetworkCreateArgs,
  DockerNetworkCreateResult,
  DockerNetworkDisconnectArgs,
  DockerNetworkDisconnectResult,
  DockerNetworkInspectArgs,
  DockerNetworkInspectResult,
  DockerNetworkLsArgs,
  DockerNetworkLsResult,
  DockerNetworkPruneArgs,
  DockerNetworkPruneResult,
  DockerNetworkRmArgs,
  DockerNetworkRmResult,
} from './dto';

@Authorized()
@Injectable()
@Resolver()
export class DockerNetworkResolver {
  constructor(private readonly dockerNetwork: DockerNetworkService) {}

  @Query(() => [DockerNetworkLsResult])
  async dockerNetworkLs(@Args(() => DockerNetworkLsArgs) args: DockerNetworkLsArgs) {
    return this.dockerNetwork.ls(args);
  }

  @Mutation(() => DockerNetworkRmResult)
  async dockerNetworkRm(@Args(() => DockerNetworkRmArgs) args: DockerNetworkRmArgs) {
    return this.dockerNetwork.rm(args);
  }

  @Mutation(() => DockerNetworkCreateResult)
  async dockerNetworkCreate(@Args(() => DockerNetworkCreateArgs) args: DockerNetworkCreateArgs) {
    return this.dockerNetwork.create(args);
  }

  @Query(() => [DockerNetworkInspectResult])
  async dockerNetworkInspect(@Args(() => DockerNetworkInspectArgs) args: DockerNetworkInspectArgs) {
    return this.dockerNetwork.inspect(args);
  }

  @Mutation(() => DockerNetworkPruneResult)
  async dockerNetworkPrune(@Args(() => DockerNetworkPruneArgs) args: DockerNetworkPruneArgs) {
    return this.dockerNetwork.prune(args);
  }

  @Mutation(() => DockerNetworkConnectResult)
  async dockerNetworkConnect(@Args(() => DockerNetworkConnectArgs) args: DockerNetworkConnectArgs) {
    return this.dockerNetwork.connect(args);
  }

  @Mutation(() => DockerNetworkDisconnectResult)
  async dockerNetworkDisconnect(@Args(() => DockerNetworkDisconnectArgs) args: DockerNetworkDisconnectArgs) {
    return this.dockerNetwork.disconnect(args);
  }
}
