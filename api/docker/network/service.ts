/*
 *  File: /docker/network/service.ts
 *  Project: api
 *  File Created: 23-02-2024 06:02:57
 *  Author: Clay Risser
 *  -----
 *  BitSpur (c) Copyright 2024
 *
 *  Licensed under the GNU Affero General Public License (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://www.gnu.org/licenses/agpl-3.0.en.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *  You can be released from the requirements of the license by purchasing
 *  a commercial license. Buying such a license is mandatory as soon as you
 *  develop commercial activities involving this software without disclosing
 *  the source code of your own applications.
 */

import { ExecaError, execa } from 'execa';
import { Flag } from '../types';
import { Logger, Injectable } from '@multiplatform.one/typegraphql';
import kebabCase from 'lodash.kebabcase';
import { GraphQLError } from 'graphql';
import { camelCaseKeys } from '../utils';
import {
  DockerNetworkConnectArgs,
  DockerNetworkConnectResult,
  DockerNetworkCreateArgs,
  DockerNetworkCreateResult,
  DockerNetworkDisconnectArgs,
  DockerNetworkDisconnectResult,
  DockerNetworkInspectArgs,
  DockerNetworkInspectResult,
  DockerNetworkLsArgs,
  DockerNetworkLsResult,
  DockerNetworkPruneArgs,
  DockerNetworkPruneResult,
  DockerNetworkRmArgs,
  DockerNetworkRmResult,
} from './dto';

@Injectable()
export class DockerNetworkService {
  constructor(private readonly logger: Logger) {}

  async ls(args: DockerNetworkLsArgs = {}): Promise<DockerNetworkLsResult[]> {
    const { stderr, stdout } = await this.network('ls', {
      filter: args.filter,
      format: `{{json .}}`,
      noTrunc: args.noTrunc,
    });
    if (stderr) {
      throw new GraphQLError(stderr);
    }
    return camelCaseKeys(JSON.parse(`[${stdout.split('\n').join(',')}]`));
  }

  async rm(args: DockerNetworkRmArgs): Promise<DockerNetworkRmResult> {
    const { stderr, stdout } = await this.network(
      'rm',
      {
        force: args.force,
      },
      args.networks,
    );
    if (stderr) {
      throw new GraphQLError(stderr);
    }
    return {
      stdout: stdout,
      stderr: stderr,
    };
  }

  async create(args: DockerNetworkCreateArgs): Promise<DockerNetworkCreateResult> {
    const { stderr, stdout } = await this.network(
      'create',
      {
        attachable: args.attachable,
        auxAddress: args.auxAddress,
        configFrom: args.configFrom,
        configOnly: args.configOnly,
        driver: args.driver,
        gateway: args.gateway,
        ingress: args.ingress,
        internal: args.internal,
        ipRange: args.ipRange,
        ipamDriver: args.ipamDriver,
        ipamOpt: args.ipamOpt,
        ipv6: args.ipv6,
        labels: args.labels,
        subnet: args.subnet,
      },
      [args.name],
    );
    if (stderr) {
      throw new GraphQLError(stderr);
    }
    return {
      stdout: stdout,
      stderr: stderr,
    };
  }

  async inspect(args: DockerNetworkInspectArgs): Promise<[DockerNetworkInspectResult]> {
    const { stderr, stdout } = await this.network(
      'inspect',
      {
        format: `json`,
      },
      args.networks,
    );
    if (stderr) {
      throw new GraphQLError(stderr);
    }
    return camelCaseKeys(JSON.parse(stdout));
  }

  async prune(args: DockerNetworkPruneArgs): Promise<DockerNetworkPruneResult> {
    const { stderr, stdout } = await this.network('prune', {
      filter: args.filter,
      force: args.force,
    });
    if (stderr) {
      throw new GraphQLError(stderr);
    }
    return {
      stdout: stdout,
      stderr: stderr,
    };
  }

  async connect(args: DockerNetworkConnectArgs): Promise<DockerNetworkConnectResult> {
    const { stderr, stdout } = await this.network(
      'connect',
      {
        alias: args.alias,
        driverOpt: args.driverOpt,
        link: args.link,
        ipv4: args.ipv4,
        ipv6: args.ipv6,
        linkLocalIp: args.linkLocalIp,
      },
      [args.network, args.container],
    );
    if (stderr) {
      throw new GraphQLError(stderr);
    }
    return {
      stdout: stdout,
      stderr: stderr,
    };
  }

  async disconnect(args: DockerNetworkDisconnectArgs): Promise<DockerNetworkDisconnectResult> {
    const { stderr, stdout } = await this.network(
      'disconnect',
      {
        force: args.force,
      },
      [args.network, args.container],
    );
    if (stderr) {
      throw new GraphQLError(stderr);
    }
    return {
      stdout: stdout,
      stderr: stderr,
    };
  }

  private async network(
    command: string,
    options: Record<string, Flag> = {},
    args: (string | undefined)[] = [],
  ): Promise<NetworkResult> {
    try {
      const p = execa('docker', [
        'network',
        command,
        ...Object.entries(options).reduce<string[]>((acc, [key, value]: [string, Flag]) => {
          if (value === true) {
            acc.push(`--${kebabCase(key)}`);
          } else if (typeof value === 'string' || typeof value === 'number') {
            acc.push(`--${kebabCase(key)}`, value.toString());
          } else if (Array.isArray(value)) {
            value.forEach((v) => {
              acc.push(`--${kebabCase(key)}`, v.toString());
            });
          }
          return acc;
        }, []),
        ...((args || []).filter(Boolean) as string[]),
      ]);
      if (process.env.DEBUG !== '0') {
        p.stdout?.pipe(process.stdout);
        p.stderr?.pipe(process.stderr);
      }
      const proc = await p;
      return {
        stderr: proc.stderr,
        stdout: proc.stdout,
      };
    } catch (err) {
      const error = err as ExecaError;
      if (error.stderr) {
        return {
          stderr: error.stderr,
          stdout: error.stdout,
        };
      }
      throw err;
    }
  }
}

export interface NetworkResult {
  stderr: string;
  stdout: string;
  [key: string]: any;
}
