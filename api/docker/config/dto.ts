/*
 *  File: /docker/config/dto.ts
 *  Project: api
 *  File Created: 23-02-2024 06:02:57
 *  Author: Clay Risser
 *  -----
 *  BitSpur (c) Copyright 2024
 *
 *  Licensed under the GNU Affero General Public License (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://www.gnu.org/licenses/agpl-3.0.en.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *  You can be released from the requirements of the license by purchasing
 *  a commercial license. Buying such a license is mandatory as soon as you
 *  develop commercial activities involving this software without disclosing
 *  the source code of your own applications.
 */

import { GraphQLJSONObject } from 'graphql-scalars';
import { ArgsType, Field, InputType, ObjectType } from 'type-graphql';

@ArgsType()
export class DockerConfigCreateArgs {
  @Field(() => String, { description: 'file|-' })
  file: string;

  @Field(() => String, { nullable: true, description: 'Config name' })
  name: string;

  @Field(() => DockerConfigCreateOptions, { nullable: true, description: 'options for creating config' })
  options: DockerConfigCreateOptions;
}

@InputType()
export class DockerConfigCreateOptions {
  @Field(() => [String], { nullable: true, description: 'Config labels' })
  label: string[];

  @Field(() => String, { nullable: true, description: 'Template driver' })
  templateDriver: string;
}

@ObjectType()
export class DockerConfigInspectResult {
  @Field(() => String, { nullable: true, description: 'Config ID' })
  id: string;

  @Field(() => DockerConfigVersion, { nullable: true, description: 'Config Version' })
  version: DockerConfigVersion;

  @Field(() => String, { nullable: true, description: 'Config Created At' })
  createdAt: string;

  @Field(() => String, { nullable: true, description: 'Config Updated At' })
  updatedAt: string;

  @Field(() => DockerConfigInspectSpec, { nullable: true, description: 'Config Spec' })
  spec: DockerConfigInspectSpec;
}

@ObjectType()
export class DockerConfigVersion {
  @Field(() => Number, { nullable: true, description: 'Config Version Index' })
  index: number;
}

@ObjectType()
export class DockerConfigInspectSpec {
  @Field(() => String, { nullable: true, description: 'Config Name' })
  name: string;

  @Field(() => GraphQLJSONObject, { nullable: true, description: 'config labels' })
  labels: Record<string, string>;

  @Field(() => String, { nullable: true, description: 'Config Data' })
  data: string;
}

@ArgsType()
export class DockerConfigInspectArgs {
  @Field(() => [String], { description: 'Config IDs' })
  configs: string[];
}

@ObjectType()
export class DockerConfigLsResult {
  @Field(() => String, { nullable: true, description: 'Config ID' })
  id: string;

  @Field(() => String, { nullable: true, description: 'Config Name' })
  name: string;

  @Field(() => String, { nullable: true, description: 'Config Created At' })
  createdAt: string;

  @Field(() => String, { nullable: true, description: 'Config Updated At' })
  updatedAt: string;
}
