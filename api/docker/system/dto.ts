/*
 *  File: /docker/system/dto.ts
 *  Project: api
 *  File Created: 23-02-2024 06:02:57
 *  Author: Clay Risser
 *  -----
 *  BitSpur (c) Copyright 2024
 *
 *  Licensed under the GNU Affero General Public License (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://www.gnu.org/licenses/agpl-3.0.en.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *  You can be released from the requirements of the license by purchasing
 *  a commercial license. Buying such a license is mandatory as soon as you
 *  develop commercial activities involving this software without disclosing
 *  the source code of your own applications.
 */

import { Field, ArgsType, ObjectType } from 'type-graphql';

@ArgsType()
export class DockerSystemDfArgs {
  @Field(() => Boolean, { nullable: true, description: 'Show verbose output' })
  verbose?: boolean;
}

@ObjectType()
export class DockerSystemDfResult {
  @Field(() => String, { nullable: true, description: 'type of the system' })
  type?: string;

  @Field(() => String, { nullable: true, description: 'total of the system' })
  total?: string;

  @Field(() => String, { nullable: true, description: 'active of the system' })
  active?: string;

  @Field(() => String, { nullable: true, description: 'size of the system' })
  size?: string;

  @Field(() => String, { nullable: true, description: 'reclaimable of the system' })
  reclaimable?: string;
}

@ArgsType()
export class DockerSystemEventsArgs {
  @Field(() => [String], { nullable: true, description: 'Filter values' })
  filter?: string[];

  @Field(() => String, { nullable: true, description: 'Events created since this timestamp' })
  since?: string;

  @Field(() => String, { nullable: true, description: 'Events created until this timestamp' })
  until?: string;
}

@ObjectType()
export class DockerSystemEventsResult {
  @Field(() => String, { nullable: true, description: 'Status of the event' })
  status?: string;

  @Field(() => String, { nullable: true, description: 'ID of the event' })
  id?: string;

  @Field(() => String, { nullable: true, description: 'Source of the event' })
  from?: string;

  @Field(() => String, { nullable: true, description: 'Type of the event' })
  type?: string;

  @Field(() => String, { nullable: true, description: 'Action of the event' })
  action?: string;

  @Field(() => DockerSystemEventsActor, { nullable: true, description: 'Actor of the event' })
  actor?: DockerSystemEventsActor;

  @Field(() => String, { nullable: true, description: 'Scope of the event' })
  scope?: string;

  @Field(() => Number, { nullable: true, description: 'Time of the event' })
  time?: number;

  @Field(() => Number, { nullable: true, description: 'Time with nanosecond precision of the event' })
  timeNano?: number;
}

@ObjectType()
export class DockerSystemEventsActor {
  @Field(() => String, { nullable: true, description: 'ID of the actor' })
  id?: string;

  @Field(() => DockerSystemEventsActorAttributes, { nullable: true, description: 'Attributes of the actor' })
  attributes?: DockerSystemEventsActorAttributes;
}

@ObjectType()
export class DockerSystemEventsActorAttributes {
  @Field(() => String, { nullable: true, description: 'Image of the actor' })
  image?: string;

  @Field(() => String, { nullable: true, description: 'Maintainer of the image' })
  maintainer?: string;

  @Field(() => String, { nullable: true, description: 'Name of the actor' })
  name?: string;
}

@ArgsType()
export class DockerSystemPruneArgs {
  @Field(() => Boolean, { nullable: true, description: 'Remove all unused images not just dangling ones' })
  all?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'Do not prompt for confirmation' })
  force?: boolean;

  @Field(() => [String], { nullable: true, description: 'Filter values' })
  filter?: string[];

  @Field(() => Boolean, { nullable: true, description: 'Remove all unused local volumes' })
  volumes?: boolean;
}

@ObjectType()
export class DockerSystemPruneResult {
  @Field(() => String, { nullable: true })
  stdout?: string;

  @Field(() => String, { nullable: true })
  stderr?: string;
}

@ObjectType()
export class DockerSystemInfoResult {
  @Field(() => String, { nullable: true, description: 'Unique identifier for the Docker system' })
  id?: string;

  @Field(() => Number, { nullable: true, description: 'Total number of containers' })
  containers?: number;

  @Field(() => Number, { nullable: true, description: 'Number of running containers' })
  containersRunning?: number;

  @Field(() => Number, { nullable: true, description: 'Number of paused containers' })
  containersPaused?: number;

  @Field(() => Number, { nullable: true, description: 'Number of stopped containers' })
  containersStopped?: number;

  @Field(() => Number, { nullable: true, description: 'Number of images' })
  images?: number;

  @Field(() => String, { nullable: true, description: 'Driver used by Docker' })
  driver?: string;

  @Field(() => [[String]], { nullable: 'itemsAndList', description: 'Status of the driver' })
  driverStatus?: string[][];

  @Field(() => DockerSystemInfoPlugins, { nullable: true, description: 'Plugins used by Docker' })
  plugins?: DockerSystemInfoPlugins;

  @Field(() => Boolean, { nullable: true, description: 'Whether memory limit is enabled' })
  memoryLimit?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'Whether swap limit is enabled' })
  swapLimit?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'Whether CPU CFS period is enabled' })
  cpuCfsPeriod?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'Whether CPU CFS quota is enabled' })
  cpuCfsQuota?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'Whether CPU shares are enabled' })
  cpuShares?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'Whether CPU set is enabled' })
  cpuSet?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'Whether PIDs limit is enabled' })
  pidsLimit?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'Whether IPv4 forwarding is enabled' })
  iPv4Forwarding?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'Whether bridge NF iptables is enabled' })
  bridgeNfIptables?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'Whether bridge NF ip6tables is enabled' })
  bridgeNfIp6Tables?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'Whether debug mode is enabled' })
  debug?: boolean;

  @Field(() => Number, { nullable: true, description: 'Number of file descriptors' })
  nFd?: number;

  @Field(() => Boolean, { nullable: true, description: 'Whether OOM kill disable is enabled' })
  oomKillDisable?: boolean;

  @Field(() => Number, { nullable: true, description: 'Number of goroutines' })
  nGoroutines?: number;

  @Field(() => String, { nullable: true, description: 'System time' })
  systemTime?: string;

  @Field(() => String, { nullable: true, description: 'Logging driver' })
  loggingDriver?: string;

  @Field(() => String, { nullable: true, description: 'Cgroup driver' })
  cgroupDriver?: string;

  @Field(() => String, { nullable: true, description: 'Cgroup version' })
  cgroupVersion?: string;

  @Field(() => Number, { nullable: true, description: 'Number of event listeners' })
  nEventsListener?: number;

  @Field(() => String, { nullable: true, description: 'Kernel version' })
  kernelVersion?: string;

  @Field(() => String, { nullable: true, description: 'Operating system' })
  operatingSystem?: string;

  @Field(() => String, { nullable: true, description: 'OS version' })
  osVersion?: string;

  @Field(() => String, { nullable: true, description: 'OS type' })
  osType?: string;

  @Field(() => String, { nullable: true, description: 'System architecture' })
  architecture?: string;

  @Field(() => String, { nullable: true, description: 'Index server address' })
  indexServerAddress?: string;

  @Field(() => DockerSystemInfoRegistryConfig, { nullable: true, description: 'Registry configuration' })
  registryConfig?: DockerSystemInfoRegistryConfig;

  @Field(() => Number, { nullable: true, description: 'Number of CPUs' })
  ncpu?: number;

  @Field(() => Number, { nullable: true, description: 'Total memory' })
  memTotal?: number;

  @Field(() => [String], { nullable: 'itemsAndList', description: 'Generic resources' })
  genericResources?: string[];

  @Field(() => String, { nullable: true, description: 'Docker root directory' })
  dockerRootDir?: string;

  @Field(() => String, { nullable: true, description: 'HTTP proxy' })
  httpProxy?: string;

  @Field(() => String, { nullable: true, description: 'HTTPS proxy' })
  httpsProxy?: string;

  @Field(() => String, { nullable: true, description: 'No proxy' })
  noProxy?: string;

  @Field(() => String, { nullable: true, description: 'Name of the Docker system' })
  name?: string;

  @Field(() => [String], { nullable: 'itemsAndList', description: 'Labels' })
  labels?: string[];

  @Field(() => Boolean, { nullable: true, description: 'Whether experimental build is enabled' })
  experimentalBuild?: boolean;

  @Field(() => String, { nullable: true, description: 'Server version' })
  serverVersion?: string;

  @Field(() => DockerSystemInfoRuntimes, { nullable: true, description: 'Runtimes' })
  runtimes?: DockerSystemInfoRuntimes;

  @Field(() => String, { nullable: true, description: 'Default runtime' })
  defaultRuntime?: string;

  @Field(() => DockerSystemInfoSwarm, { nullable: true, description: 'Swarm information' })
  swarm?: DockerSystemInfoSwarm;

  @Field(() => Boolean, { nullable: true, description: 'Whether live restore is enabled' })
  liveRestoreEnabled?: boolean;

  @Field(() => String, { nullable: true, description: 'Isolation' })
  isolation?: string;

  @Field(() => String, { nullable: true, description: 'Init binary' })
  initBinary?: string;

  @Field(() => DockerSystemInfoCommit, { nullable: true, description: 'Containerd commit' })
  containerdCommit?: DockerSystemInfoCommit;

  @Field(() => DockerSystemInfoCommit, { nullable: true, description: 'Runc commit' })
  runcCommit?: DockerSystemInfoCommit;

  @Field(() => DockerSystemInfoCommit, { nullable: true, description: 'Init commit' })
  initCommit?: DockerSystemInfoCommit;

  @Field(() => [String], { nullable: 'itemsAndList', description: 'Security options' })
  securityOptions?: string[];

  @Field(() => [String], { nullable: 'itemsAndList', description: 'CDI spec directories' })
  cdiSpecDirs?: string[];

  @Field(() => [String], { nullable: 'itemsAndList', description: 'Warnings' })
  warnings?: string[];

  @Field(() => DockerSystemInfoClientInfo, { nullable: true, description: 'Client information' })
  clientInfo?: DockerSystemInfoClientInfo;
}

@ObjectType()
export class DockerSystemInfoPlugins {
  @Field(() => [String], { nullable: true })
  volume: string[];

  @Field(() => [String], { nullable: true })
  network: string[];

  @Field(() => String, { nullable: true })
  authorization: string;

  @Field(() => [String], { nullable: true })
  log: string[];
}

@ObjectType()
export class DockerSystemInfoRegistryConfig {
  @Field(() => Boolean, { nullable: true, description: 'Allow nondistributable artifacts CIDRs' })
  allowNondistributableArtifactsCidRs?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'Allow nondistributable artifacts hostnames' })
  allowNondistributableArtifactsHostnames?: boolean;

  @Field(() => [String], { nullable: true, description: 'Insecure registry CIDRs' })
  insecureRegistryCidRs?: string[];

  @Field(() => [String], { nullable: true })
  indexConfigs: string[];

  @Field(() => String, { nullable: true })
  mirrors: string;
}

@ObjectType()
export class DockerSystemInfoRuntimes {
  @Field(() => DockerSystemInfoRuntimeRunc, { nullable: true })
  runc: DockerSystemInfoRuntimeRunc;

  @Field(() => DockerSystemInfoRuntimeRunc, { nullable: true })
  ioContainerdRuncV2: DockerSystemInfoRuntimeRunc;
}

@ObjectType()
export class DockerSystemInfoRuntimeRunc {
  @Field(() => String, { nullable: true })
  path: string;

  @Field(() => DockerSystemInfoRuntimeStatus, { nullable: true })
  status: DockerSystemInfoRuntimeStatus;
}

@ObjectType()
export class DockerSystemInfoRuntimeStatus {
  @Field(() => String, { nullable: true })
  orgOpencontainersRuntimeSpecFeatures: string;
}

@ObjectType()
export class DockerSystemInfoSwarm {
  @Field(() => String, { nullable: true })
  nodeID: string;

  @Field(() => String, { nullable: true })
  nodeAddr: string;

  @Field(() => String, { nullable: true })
  localNodeState: string;

  @Field(() => Boolean, { nullable: true })
  controlAvailable: boolean;

  @Field(() => String, { nullable: true })
  error: string;

  @Field(() => [String], { nullable: true })
  remoteManagers: string[];

  @Field(() => Number, { nullable: true })
  nodes: number;

  @Field(() => Number, { nullable: true })
  managers: number;

  @Field(() => String, { nullable: true })
  cluster: string;
}

@ObjectType()
export class DockerSystemInfoCommit {
  @Field(() => String, { nullable: true })
  id: string;

  @Field(() => String, { nullable: true })
  expected: string;
}

@ObjectType()
export class DockerSystemInfoClientInfo {
  @Field(() => Boolean, { nullable: true })
  debug: boolean;

  @Field(() => String, { nullable: true })
  platform: string;

  @Field(() => String, { nullable: true })
  version: string;

  @Field(() => String, { nullable: true })
  gitCommit: string;

  @Field(() => String, { nullable: true })
  goVersion: string;

  @Field(() => String, { nullable: true })
  os: string;

  @Field(() => String, { nullable: true })
  arch: string;

  @Field(() => String, { nullable: true })
  buildTime: string;

  @Field(() => String, { nullable: true })
  context: string;

  @Field(() => [String], { nullable: true })
  plugins: string[];

  @Field(() => String, { nullable: true })
  warnings: string;
}
