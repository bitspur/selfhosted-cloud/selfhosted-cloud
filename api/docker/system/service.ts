/*
 *  File: /docker/system/service.ts
 *  Project: api
 *  File Created: 23-02-2024 06:02:57
 *  Author: Clay Risser
 *  -----
 *  BitSpur (c) Copyright 2024
 *
 *  Licensed under the GNU Affero General Public License (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://www.gnu.org/licenses/agpl-3.0.en.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *  You can be released from the requirements of the license by purchasing
 *  a commercial license. Buying such a license is mandatory as soon as you
 *  develop commercial activities involving this software without disclosing
 *  the source code of your own applications.
 */

import { ExecaError, execa } from 'execa';
import { Flag } from '../types';
import { Logger, Injectable } from '@multiplatform.one/typegraphql';
import kebabCase from 'lodash.kebabcase';
import { GraphQLError } from 'graphql';
import { camelCaseKeys } from '../utils';
import {
  DockerSystemDfArgs,
  DockerSystemDfResult,
  DockerSystemEventsArgs,
  DockerSystemEventsResult,
  DockerSystemPruneArgs,
  DockerSystemPruneResult,
} from './dto';

@Injectable()
export class DockerSystemService {
  constructor(private readonly logger: Logger) {}

  async df(args: DockerSystemDfArgs): Promise<DockerSystemDfResult[]> {
    const { stderr, stdout } = await this.system('df', {
      format: `{{json .}}`,
      verbose: args.verbose,
    });
    if (stderr) {
      throw new GraphQLError(stderr);
    }
    return camelCaseKeys(JSON.parse(`[${stdout.split('\n').join(',')}]`));
  }

  async info(): Promise<any> {
    const { stderr, stdout } = await this.system('info', {
      format: `{{json .}}`,
    });
    if (stderr) {
      throw new GraphQLError(stderr);
    }

    const res = camelCaseKeys(JSON.parse(stdout));
    return res;
  }

  async events(args: DockerSystemEventsArgs): Promise<DockerSystemEventsResult[]> {
    const { stderr, stdout } = await this.system('events', {
      since: args.since,
      until: args.until,
      filter: args.filter,
      format: `{{json .}}`,
    });
    if (stderr) {
      console.error('stderr', stderr);
      throw new GraphQLError(stderr);
    }
    return camelCaseKeys(JSON.parse(`[${stdout.split('\n').join(',')}]`));
  }

  async prune(args: DockerSystemPruneArgs): Promise<DockerSystemPruneResult> {
    const { stderr, stdout } = await this.system('prune', {
      all: args.all,
      filter: args.filter,
      force: args.force,
      volumes: args.volumes,
    });
    if (stderr) {
      throw new GraphQLError(stderr);
    }
    return {
      stdout,
      stderr,
    };
  }

  private async system(
    command: string,
    options: Record<string, Flag> = {},
    args: (string | undefined)[] = [],
  ): Promise<SystemResult> {
    try {
      const p = execa('docker', [
        'system',
        command,
        ...Object.entries(options).reduce<string[]>((acc, [key, value]: [string, Flag]) => {
          if (value === true) {
            acc.push(`--${kebabCase(key)}`);
          } else if (typeof value === 'string' || typeof value === 'number') {
            acc.push(`--${kebabCase(key)}`, value.toString());
          } else if (Array.isArray(value)) {
            value.forEach((v) => {
              acc.push(`--${kebabCase(key)}`, v.toString());
            });
          }
          return acc;
        }, []),
        ...((args || []).filter(Boolean) as string[]),
      ]);
      if (process.env.DEBUG !== '0') {
        p.stdout?.pipe(process.stdout);
        p.stderr?.pipe(process.stderr);
      }
      const proc = await p;
      return {
        stderr: proc.stderr,
        stdout: proc.stdout,
      };
    } catch (err) {
      const error = err as ExecaError;
      if (error.stderr) {
        return {
          stderr: error.stderr,
          stdout: error.stdout,
        };
      }
      throw err;
    }
  }
}

export interface SystemResult {
  stderr: string;
  stdout: string;
  [key: string]: any;
}
