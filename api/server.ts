/*
 *  File: /server.ts
 *  Project: api
 *  File Created: 19-01-2024 06:33:59
 *  Author: Clay Risser
 *  -----
 *  BitSpur (c) Copyright 2024
 *
 *  Licensed under the GNU Affero General Public License (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://www.gnu.org/licenses/agpl-3.0.en.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *  You can be released from the requirements of the license by purchasing
 *  a commercial license. Buying such a license is mandatory as soon as you
 *  develop commercial activities involving this software without disclosing
 *  the source code of your own applications.
 */

import type { ServerOptions } from '@multiplatform.one/typegraphql';
import type { UserRepresentation } from '@multiplatform.one/keycloak-typegraphql';
import { PrismaClient } from '@prisma/client';
import { pubSub } from './pubSub';
import { resolvers } from './resolvers';

const seedUsers: UserRepresentation[] = [
  {
    username: 'one',
    email: 'one@multiplatform',
    firstName: 'Multiplatform',
    lastName: 'One',
    credentials: [{ value: 'pass' }],
  },
];

export const options: ServerOptions = {
  debug: process.env.DEBUG === '1',
  prisma: new PrismaClient(),
  pubSub,
  resolvers,
  secret: process.env.SECRET,
  logger: {
    axios: {
      requestLogLevel: 'info',
      responseLogLevel: 'info',
      data: false,
      headers: false,
    },
  },
  tracing: {
    apollo: process.env.APOLLO_TRACING === '1',
  },
  keycloak: {
    adminPassword: process.env.KEYCLOAK_ADMIN_PASSWORD || '',
    adminUsername: process.env.KEYCLOAK_ADMIN_USERNAME || '',
    baseUrl: process.env.KEYCLOAK_BASE_URL || '',
    clientId: process.env.KEYCLOAK_CLIENT_ID || '',
    clientSecret: process.env.KEYCLOAK_CLIENT_SECRET || '',
    realm: process.env.KEYCLOAK_REALM || 'master',
    register:
      process.env.KEYCLOAK_REGISTER === '1'
        ? {
            ...(process.env.SEED === '1' ? { users: seedUsers } : {}),
          }
        : false,
  },
};
