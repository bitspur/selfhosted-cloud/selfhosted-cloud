/*
 *  File: /resolvers.ts
 *  Project: api
 *  File Created: 23-02-2024 06:02:57
 *  Author: Clay Risser
 *  -----
 *  BitSpur (c) Copyright 2024
 *
 *  Licensed under the GNU Affero General Public License (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://www.gnu.org/licenses/agpl-3.0.en.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *  You can be released from the requirements of the license by purchasing
 *  a commercial license. Buying such a license is mandatory as soon as you
 *  develop commercial activities involving this software without disclosing
 *  the source code of your own applications.
 */

import { AuthResolver } from './auth';
import { Authorized } from '@multiplatform.one/keycloak-typegraphql';
import { DockerContainerResolver } from './docker/container/resolver';
import { DockerImageResolver } from './docker/image';
import { DockerNetworkResolver } from './docker/network';
import { DockerNodeResolver } from './docker/node';
import { DockerSecretResolver } from './docker/secret';
import { DockerStackResolver, DockerSwarmResolver, DockerResolver, DockerServiceResolver } from './docker';
import { DockerSystemResolver } from './docker/system';
import { DockerVolumeResolver } from './docker/volume';
import { DockerPluginResolver } from './docker/plugin';
import { EnvironmentResolver, DeploymentResolver } from './deployment';
import { JobResolver } from './job';
import { NonEmptyArray } from 'type-graphql';
import {
  PrismaDeploymentCrudResolver,
  PrismaEnvironmentCrudResolver,
  PrismaUserCrudResolver,
  applyArgsTypesEnhanceMap,
  applyInputTypesEnhanceMap,
  applyModelsEnhanceMap,
  applyOutputTypesEnhanceMap,
  applyRelationResolversEnhanceMap,
  applyResolversEnhanceMap,
} from './generated/type-graphql';
import { DockerBuilderResolver } from './docker/builder';
import { DockerConfigResolver } from './docker/config';
import { DockerContextResolver } from './docker/context';
import { DockerManifestResolver } from './docker/manifest';
import { DockerTrustResolver } from './docker/trust';

export const resolvers: NonEmptyArray<Function> | NonEmptyArray<string> = [
  AuthResolver,
  DeploymentResolver,
  DockerContainerResolver,
  DockerImageResolver,
  DockerNetworkResolver,
  DockerNodeResolver,
  DockerResolver,
  DockerServiceResolver,
  DockerStackResolver,
  DockerSwarmResolver,
  DockerSystemResolver,
  DockerVolumeResolver,
  EnvironmentResolver,
  JobResolver,
  PrismaDeploymentCrudResolver,
  PrismaEnvironmentCrudResolver,
  PrismaUserCrudResolver,
  DockerNodeResolver,
  DockerSecretResolver,
  DockerBuilderResolver,
  DockerConfigResolver,
  DockerPluginResolver,
  DockerContextResolver,
  DockerManifestResolver,
  DockerTrustResolver,
];

applyResolversEnhanceMap({
  PrismaEnvironment: {
    _all: [Authorized()],
  },
  PrismaDeployment: {
    _all: [Authorized()],
  },
  PrismaUser: {
    _all: [Authorized()],
  },
});
applyArgsTypesEnhanceMap({});
applyInputTypesEnhanceMap({});
applyModelsEnhanceMap({});
applyOutputTypesEnhanceMap({});
applyRelationResolversEnhanceMap({});
