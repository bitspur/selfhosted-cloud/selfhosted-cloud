/*
 *  File: /main.ts
 *  Project: api
 *  File Created: 19-01-2024 06:33:59
 *  Author: Clay Risser
 *  -----
 *  BitSpur (c) Copyright 2024
 *
 *  Licensed under the GNU Affero General Public License (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://www.gnu.org/licenses/agpl-3.0.en.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *  You can be released from the requirements of the license by purchasing
 *  a commercial license. Buying such a license is mandatory as soon as you
 *  develop commercial activities involving this software without disclosing
 *  the source code of your own applications.
 */

import 'dotenv/config';
import fs from 'fs/promises';
import { DockerNodeService } from './docker/node';
import { JobService } from './job';
import { Logger, createServer } from '@multiplatform.one/typegraphql';
import { config } from './config';
import { options } from './server';
import {
  DockerService,
  DockerServiceService,
  DockerStackService,
  DockerSwarmService,
  listenDockerEvents,
} from './docker';

(async () => {
  await Promise.all(Object.values(config.dirs).map((dir) => fs.mkdir(dir, { recursive: true })));
  const server = await createServer(options, () => {
    const logger = new Logger(options.logger);
    listenDockerEvents(logger);
    if (!options.prisma) return;
    const jobService = new JobService(
      new DockerServiceService(
        new DockerNodeService(logger),
        new DockerService(logger),
        new DockerStackService(logger),
        new DockerSwarmService(logger),
        logger,
      ),
      options.prisma,
    );
    jobService.scheduleCronJobs();
  });
  await server.start();
})();
