/*
 *  File: /job/dto.ts
 *  Project: api
 *  File Created: 13-02-2024 03:09:55
 *  Author: Clay Risser
 *  -----
 *  BitSpur (c) Copyright 2024
 *
 *  Licensed under the GNU Affero General Public License (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://www.gnu.org/licenses/agpl-3.0.en.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *  You can be released from the requirements of the license by purchasing
 *  a commercial license. Buying such a license is mandatory as soon as you
 *  develop commercial activities involving this software without disclosing
 *  the source code of your own applications.
 */

import { ArgsType, Field, ObjectType } from 'type-graphql';

@ArgsType()
export class JobCreateJobArgs {
  @Field(() => String, { description: 'job name' })
  name: string;

  @Field(() => String, { description: 'service name' })
  serviceName: string;

  @Field(() => String, { description: 'deployment name' })
  deploymentName: string;

  @Field(() => String, { description: 'environment name' })
  environmentName: string;

  @Field(() => String, { description: 'command to be executed' })
  command: string;

  @Field(() => [String], { nullable: true, description: 'arguments for the command' })
  args?: string[];

  @Field(() => [String], { nullable: true, description: 'environment variables' })
  env?: string[];

  @Field(() => Boolean, { nullable: true, description: 'keep stdin open even if not attached' })
  interactive?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'give extended privileges to the command' })
  privileged?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'allocate a pseudo-tty' })
  tty?: boolean;

  @Field(() => String, { nullable: true, description: 'username or uid' })
  user?: string;

  @Field(() => String, { nullable: true, description: 'working directory inside the container' })
  workdir?: string;

  @Field(() => Boolean, { nullable: true, description: 'run command on all containers' })
  all?: boolean;

  @Field(() => String, { nullable: true, description: 'Image name' })
  image?: string;

  @Field(() => [String], { nullable: true, description: 'Add Linux capabilities' })
  capAdd?: string[];

  @Field(() => [String], { nullable: true, description: 'Drop Linux capabilities' })
  capDrop?: string[];

  @Field(() => [String], { nullable: true, description: 'Specify configurations to expose to the service' })
  config?: string[];

  @Field(() => [String], { nullable: true, description: 'Placement constraints' })
  constraint?: string[];

  @Field(() => [String], { nullable: true, description: 'Container labels' })
  containerLabel?: string[];

  @Field(() => String, { nullable: true, description: 'Credential spec for managed service account (Windows only)' })
  credentialSpec?: string;

  @Field(() => [String], { nullable: true, description: 'Set custom DNS servers' })
  dns?: string[];

  @Field(() => [String], { nullable: true, description: 'Set DNS options' })
  dnsOption?: string[];

  @Field(() => [String], { nullable: true, description: 'Set custom DNS search domains' })
  dnsSearch?: string[];

  @Field(() => String, { nullable: true, description: 'Endpoint mode (vip or dnsrr) (default "vip")' })
  endpointMode?: string;

  @Field(() => String, { nullable: true, description: 'Overwrite the default ENTRYPOINT of the image' })
  entrypoint?: string;

  @Field(() => [String], { nullable: true, description: 'Read in a file of environment variables' })
  envFile?: string[];

  @Field(() => [String], { nullable: true, description: 'User defined resources' })
  genericResource?: string[];

  @Field(() => [String], { nullable: true, description: 'Set one or more supplementary user groups for the container' })
  group?: string[];

  @Field(() => String, { nullable: true, description: 'Command to run to check health' })
  healthCmd?: string;

  @Field(() => String, { nullable: true, description: 'Time between running the check (ms|s|m|h)' })
  healthInterval?: string;

  @Field(() => Number, { nullable: true, description: 'Consecutive failures needed to report unhealthy' })
  healthRetries?: number;

  @Field(() => String, {
    nullable: true,
    description: 'Time between running the check during the start period (ms|s|m|h)',
  })
  healthStartInterval?: string;

  @Field(() => String, {
    nullable: true,
    description: 'Start period for the container to initialize before counting retries towards unstable (ms|s|m|h)',
  })
  healthStartPeriod?: string;

  @Field(() => String, { nullable: true, description: 'Maximum time to allow one check to run (ms|s|m|h)' })
  healthTimeout?: string;

  @Field(() => [String], { nullable: true, description: 'Set one or more custom host-to-IP mappings (host:ip)' })
  host?: string[];

  @Field(() => String, { nullable: true, description: 'Container hostname' })
  hostname?: string;

  @Field(() => Boolean, {
    nullable: true,
    description: 'Use an init inside each service container to forward signals and reap processes',
  })
  init?: boolean;

  @Field(() => [String], { nullable: true, description: 'Service labels' })
  label?: string[];

  @Field(() => Number, { nullable: true, description: 'Limit CPUs' })
  limitCpu?: number;

  @Field(() => Number, { nullable: true, description: 'Limit Memory' })
  limitMemory?: number;

  @Field(() => Number, { nullable: true, description: 'Limit maximum number of processes (default 0 = unlimited)' })
  limitPids?: number;

  @Field(() => String, { nullable: true, description: 'Logging driver for service' })
  logDriver?: string;

  @Field(() => [String], { nullable: true, description: 'Logging driver options' })
  logOpt?: string[];

  @Field(() => Number, {
    nullable: true,
    description: 'Number of job tasks to run concurrently (default equal to --replicas)',
  })
  maxConcurrent?: number;

  @Field(() => String, {
    nullable: true,
    description: 'Service mode ("replicated", "global", "replicated-job", "global-job") (default "replicated")',
  })
  mode?: string;

  @Field(() => [String], { nullable: true, description: 'Attach a filesystem mount to the service' })
  mount?: string[];

  @Field(() => [String], { nullable: true, description: 'Network attachments' })
  network?: string[];

  @Field(() => Boolean, { nullable: true, description: 'Disable any container-specified HEALTHCHECK' })
  noHealthcheck?: boolean;

  @Field(() => Boolean, {
    nullable: true,
    description: 'Do not query the registry to resolve image digest and supported platforms',
  })
  noResolveImage?: boolean;

  @Field(() => [String], { nullable: true, description: 'Add a placement preference' })
  placementPref?: string[];

  @Field(() => [String], { nullable: true, description: 'Publish a port as a node port' })
  publish?: string[];

  @Field(() => Boolean, { nullable: true, description: 'Suppress progress output' })
  quiet?: boolean;

  @Field(() => Number, { nullable: true, description: 'Replicas' })
  replicas?: number;

  @Field(() => Number, { nullable: true, description: 'Maximum number of tasks per node (default 0 = unlimited)' })
  replicasMaxPerNode?: number;

  @Field(() => Number, { nullable: true, description: 'Reserve CPUs' })
  reserveCpu?: number;

  @Field(() => Number, { nullable: true, description: 'Reserve Memory' })
  reserveMemory?: number;

  @Field(() => String, {
    nullable: true,
    description: 'Restart when condition is met ("none", "on-failure", "any") (default "any")',
  })
  restartCondition?: string;

  @Field(() => String, { nullable: true, description: 'Delay between restart attempts (ns|us|ms|s|m|h) (default 5s)' })
  restartDelay?: string;

  @Field(() => Number, { nullable: true, description: 'Maximum number of restarts before giving up' })
  restartMaxAttempts?: number;

  @Field(() => String, { nullable: true, description: 'Window used to evaluate the restart policy (ns|us|ms|s|m|h)' })
  restartWindow?: string;

  @Field(() => String, { nullable: true, description: 'Delay between task rollbacks (ns|us|ms|s|m|h) (default 0s)' })
  rollbackDelay?: string;

  @Field(() => String, {
    nullable: true,
    description: 'Action on rollback failure ("pause", "continue") (default "pause")',
  })
  rollbackFailureAction?: string;

  @Field(() => Number, { nullable: true, description: 'Failure rate to tolerate during a rollback (default 0)' })
  rollbackMaxFailureRatio?: number;

  @Field(() => String, {
    nullable: true,
    description: 'Duration after each task rollback to monitor for failure (ns|us|ms|s|m|h) (default 5s)',
  })
  rollbackMonitor?: string;

  @Field(() => String, {
    nullable: true,
    description: 'Rollback order ("start-first", "stop-first") (default "stop-first")',
  })
  rollbackOrder?: string;

  @Field(() => Number, {
    nullable: true,
    description: 'Maximum number of tasks rolled back simultaneously (0 to roll back all at once) (default 1)',
  })
  rollbackParallelism?: number;

  @Field(() => [String], { nullable: true, description: 'Specify secrets to expose to the service' })
  secret?: string[];

  @Field(() => String, {
    nullable: true,
    description: 'Time to wait before force killing a container (ns|us|ms|s|m|h) (default 10s)',
  })
  stopGracePeriod?: string;

  @Field(() => String, { nullable: true, description: 'Signal to stop the container' })
  stopSignal?: string;

  @Field(() => [String], { nullable: true, description: 'Sysctl options' })
  sysctl?: string[];

  @Field(() => [String], { nullable: true, description: 'Ulimit options (default [])' })
  ulimit?: string[];

  @Field(() => String, { nullable: true, description: 'Delay between updates (ns|us|ms|s|m|h) (default 0s)' })
  updateDelay?: string;

  @Field(() => String, {
    nullable: true,
    description: 'Action on update failure ("pause", "continue", "rollback") (default "pause")',
  })
  updateFailureAction?: string;

  @Field(() => Number, { nullable: true, description: 'Failure rate to tolerate during an update (default 0)' })
  updateMaxFailureRatio?: number;

  @Field(() => String, {
    nullable: true,
    description: 'Duration after each task update to monitor for failure (ns|us|ms|s|m|h) (default 5s)',
  })
  updateMonitor?: string;

  @Field(() => String, {
    nullable: true,
    description: 'Update order ("start-first", "stop-first") (default "stop-first")',
  })
  updateOrder?: string;

  @Field(() => Number, {
    nullable: true,
    description: 'Maximum number of tasks updated simultaneously (0 to update all at once) (default 1)',
  })
  updateParallelism?: number;

  @Field(() => Boolean, { nullable: true, description: 'Send registry authentication details to swarm agents' })
  withRegistryAuth?: boolean;
}

@ObjectType()
export class JobCreateExecJobResult {
  @Field(() => String, { nullable: true })
  id?: string;

  @Field(() => String, { nullable: true })
  name?: string;

  @Field(() => String, { nullable: true })
  stdout?: string;

  @Field(() => String, { nullable: true })
  stderr?: string;

  @Field(() => Number, { nullable: true })
  exitCode?: number;
}
