/*
 *  File: /job/service.ts
 *  Project: api
 *  File Created: 13-02-2024 05:55:24
 *  Author: Clay Risser
 *  -----
 *  BitSpur (c) Copyright 2024
 *
 *  Licensed under the GNU Affero General Public License (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://www.gnu.org/licenses/agpl-3.0.en.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *  You can be released from the requirements of the license by purchasing
 *  a commercial license. Buying such a license is mandatory as soon as you
 *  develop commercial activities involving this software without disclosing
 *  the source code of your own applications.
 */

import cron, { ScheduledTask } from 'node-cron';
import { CRON_JOB, DOCKER_SERVICE_EXEC, JOB_COMPLETED, pubSub } from '../pubSub';
import { CronJobPayloadOperation, DockerServiceExecResult, DockerServiceService } from '../docker';
import { GraphQLError } from 'graphql';
import { Injectable } from '@multiplatform.one/typegraphql';
import { JobCreateJobArgs } from './dto';
import { PrismaClient } from '@prisma/client';
import { from, lastValueFrom } from 'rxjs';
import { transformCountFieldIntoSelectRelationsCount } from '../generated/type-graphql/helpers';
import {
  CreateOnePrismaCronJobArgs,
  DeleteOnePrismaJobArgs,
  PrismaCronJob,
  PrismaJob,
} from '../generated/type-graphql';

const logger = console;
const tasks: Record<string, ScheduledTask> = {};

@Injectable()
export class JobService {
  constructor(
    private readonly dockerServiceService: DockerServiceService,
    private readonly prismaClient: PrismaClient,
  ) {}

  async createJob(args: JobCreateJobArgs): Promise<PrismaJob> {
    const stackName = `${args.deploymentName}--${args.environmentName}`;
    const serviceName = `${stackName}_${args.serviceName}`;
    const deployment = await this.prismaClient.deployment.findFirst({
      where: {
        name: args.deploymentName,
      },
    });
    if (!deployment) throw new GraphQLError(`deployment ${args.deploymentName} does not exist`);
    const environment = await this.prismaClient.environment.findFirst({
      where: {
        name: args.environmentName,
        deploymentId: deployment.id,
      },
    });
    if (!environment) throw new GraphQLError(`environment ${args.environmentName} does not exist`);
    const createdService =
      (
        await this.dockerServiceService.ls({
          filter: [`name=${serviceName}`],
        })
      ).length <= 0;
    const job = await this.prismaClient.job.create({
      data: {
        createdService,
        deploymentId: deployment.id,
        environmentId: environment.id,
        name: args.name,
        serviceName,
        status: 'RUNNING',
      },
    });
    const handleError = async (err: Error) => {
      if (createdService) {
        await this.dockerServiceService.rm({
          name: serviceName,
        });
      }
      await this.prismaClient.job.update({
        where: {
          id: job.id,
        },
        data: {
          status: 'FAILED',
          errors: [err.message || err.toString()],
        },
      });
      pubSub.publish(JOB_COMPLETED, {
        jobId: job.id,
        failed: true,
        results: [],
      });
    };
    try {
      const dockerServiceExecRepeater = pubSub.subscribe(DOCKER_SERVICE_EXEC);
      if (createdService) {
        if (!args.image) throw new GraphQLError(`image is required or service ${serviceName} must exist`);
        await this.dockerServiceService.create({
          name: serviceName,
          detach: false,
          entrypoint: '/bin/sh',
          args: ['-c', 'while :; do sleep 999999999; done'],
          label: ['job=true', ...(args.label || [])],
          image: args.image,
          capAdd: args.capAdd,
          capDrop: args.capDrop,
          config: args.config,
          constraint: args.constraint,
          containerLabel: args.containerLabel,
          credentialSpec: args.credentialSpec,
          dns: args.dns,
          dnsOption: args.dnsOption,
          dnsSearch: args.dnsSearch,
          envFile: args.envFile,
          genericResource: args.genericResource,
          group: args.group,
          healthCmd: args.healthCmd,
          healthInterval: args.healthInterval,
          healthRetries: args.healthRetries,
          healthStartInterval: args.healthStartInterval,
          healthStartPeriod: args.healthStartPeriod,
          healthTimeout: args.healthTimeout,
          host: args.host,
          hostname: args.hostname,
          init: args.init,
          limitCpu: args.limitCpu,
          limitMemory: args.limitMemory,
          limitPids: args.limitPids,
          logDriver: args.logDriver,
          logOpt: args.logOpt,
          maxConcurrent: args.maxConcurrent,
          mode: args.mode,
          mount: args.mount,
          network: args.network,
          noHealthcheck: args.noHealthcheck,
          noResolveImage: args.noResolveImage,
          placementPref: args.placementPref,
          publish: args.publish,
          quiet: args.quiet,
          replicas: args.replicas,
          replicasMaxPerNode: args.replicasMaxPerNode,
          reserveCpu: args.reserveCpu,
          reserveMemory: args.reserveMemory,
          restartCondition: args.restartCondition,
          restartDelay: args.restartDelay,
          restartMaxAttempts: args.restartMaxAttempts,
          restartWindow: args.restartWindow,
          rollbackDelay: args.rollbackDelay,
          rollbackFailureAction: args.rollbackFailureAction,
          rollbackMaxFailureRatio: args.rollbackMaxFailureRatio,
          rollbackMonitor: args.rollbackMonitor,
          rollbackOrder: args.rollbackOrder,
          rollbackParallelism: args.rollbackParallelism,
          secret: args.secret,
          stopGracePeriod: args.stopGracePeriod,
          stopSignal: args.stopSignal,
          sysctl: args.sysctl,
          ulimit: args.ulimit,
          updateDelay: args.updateDelay,
          updateFailureAction: args.updateFailureAction,
          updateMaxFailureRatio: args.updateMaxFailureRatio,
          updateMonitor: args.updateMonitor,
          updateOrder: args.updateOrder,
          updateParallelism: args.updateParallelism,
          withRegistryAuth: args.withRegistryAuth,
        });
        await new Promise((resolve) => setTimeout(resolve, 1000));
      }
      const { serviceExecId, containers } = await this.dockerServiceService.exec({
        name: serviceName,
        detach: false,
        all: args.all,
        args: args.args,
        command: args.command,
        env: args.env,
        interactive: args.interactive,
        privileged: args.privileged,
        tty: args.tty,
        user: args.user,
        workdir: args.workdir,
      });
      let payload: DockerServiceExecResult[] = containers.map((container) => ({
        containerId: container.id,
      }));
      lastValueFrom(
        from(
          (async function* () {
            for await (const dockerServiceExecPayload of dockerServiceExecRepeater) {
              if (dockerServiceExecPayload.serviceExecId !== serviceExecId) continue;
              payload = [
                ...payload.map((payload) => {
                  if (payload.containerId !== dockerServiceExecPayload.containerId) return payload;
                  return {
                    ...payload,
                    stderr: dockerServiceExecPayload.stderr,
                    stdout: dockerServiceExecPayload.stdout,
                    exitCode: dockerServiceExecPayload.exitCode,
                  };
                }),
              ];
              yield payload;
              if (payload.every((p) => p.exitCode !== undefined)) return;
            }
          })(),
        ),
      )
        .then(async (payload) => {
          if (createdService) {
            await this.dockerServiceService.rm({
              name: serviceName,
            });
          }
          const errors = payload
            .filter(({ exitCode }) => exitCode)
            .map(({ stderr }) => stderr || '')
            .filter(Boolean);
          if (errors.length) {
            await this.prismaClient.job.update({
              where: {
                id: job.id,
              },
              data: {
                errors,
                status: 'FAILED',
              },
            });
          } else {
            await this.prismaClient.job.delete({
              where: {
                id: job.id,
              },
            });
          }
          pubSub.publish(JOB_COMPLETED, {
            jobId: job.id,
            failed: errors.length > 0,
            results: payload.map(({ containerId, stderr, stdout, exitCode }) => ({
              containerId,
              exitCode: exitCode || 0,
              stderr: stderr || '',
              stdout: stdout || '',
            })),
          });
        })
        .catch(async (err) => {
          logger.error(err);
          await handleError(err);
        });
      return job;
    } catch (err) {
      await handleError(err);
      throw err;
    }
  }

  async deleteJob(args: DeleteOnePrismaJobArgs, count: any): Promise<PrismaJob> {
    const job = await this.prismaClient.job.delete({
      ...args,
      ...(count && transformCountFieldIntoSelectRelationsCount(count)),
    });
    const environment = await this.prismaClient.environment.findFirst({
      where: {
        id: job.environmentId,
      },
      include: {
        deployment: true,
      },
    });
    const stackName = `${environment?.deployment.name}--${environment?.name}`;
    const serviceName = `${stackName}_${job.name}`;
    const service = (
      await this.dockerServiceService.ls({
        filter: [`name=${serviceName}`, 'job=true'],
      })
    )?.[0];
    if (service?.id) await this.dockerServiceService.rm({ name: serviceName });
    return job;
  }

  async createCronJob(args: CreateOnePrismaCronJobArgs, count: any): Promise<PrismaCronJob> {
    const { id } = await this.prismaClient.cronJob.create({
      ...args,
      ...(count && transformCountFieldIntoSelectRelationsCount(count)),
    });
    const cronJob = (await this.prismaClient.cronJob.findFirst({
      where: { id },
      include: {
        deployment: true,
        environment: true,
      },
    }))!;
    pubSub.publish(CRON_JOB, {
      deploymentName: cronJob.deployment.name,
      environmentName: cronJob.environment.name,
      expression: cronJob.expression,
      name: cronJob.name,
      operation: CronJobPayloadOperation.Create,
    });
    return cronJob;
  }

  async scheduleCronJobs() {
    const cronJobs = await this.prismaClient.cronJob.findMany({
      where: {
        active: true,
      },
      include: {
        deployment: true,
        environment: true,
      },
    });
    for (const { deployment, environment, expression, name } of cronJobs) {
      const cronJob = {
        deploymentName: deployment.name,
        environmentName: environment.name,
        expression: expression,
        name,
      };
      this.scheduleCronJob(cronJob, () => this.handleCreateJob(cronJob));
    }
    const cronJobRepeater = pubSub.subscribe(CRON_JOB);
    for await (const cronJobPayload of cronJobRepeater) {
      switch (cronJobPayload.operation) {
        case CronJobPayloadOperation.Create: {
          this.scheduleCronJob(cronJobPayload, () => this.handleCreateJob(cronJobPayload));
          break;
        }
        case CronJobPayloadOperation.Update: {
          this.scheduleCronJob(cronJobPayload, () => this.handleCreateJob(cronJobPayload));
          break;
        }
        case CronJobPayloadOperation.Delete: {
          this.unscheduleCronJob(cronJobPayload);
          break;
        }
      }
    }
  }

  async handleCreateJob(cronJob: ICronJob) {
    return this.createJob({
      deploymentName: cronJob.deploymentName,
      environmentName: cronJob.environmentName,
      name: cronJob.name,
      serviceName: cronJob.name,
      command: '',
    });
  }

  private scheduleCronJob(cronJob: ICronJob, fn: (...args: any[]) => any) {
    this.unscheduleCronJob(cronJob);
    tasks[cronJob.name] = cron.schedule(cronJob.expression, fn);
  }

  private unscheduleCronJob(cronJob: ICronJob) {
    const task = tasks[cronJob.name];
    if (task) task.stop();
    delete tasks[cronJob.name];
  }
}

export interface ICronJob {
  deploymentName: string;
  environmentName: string;
  expression: string;
  name: string;
}
