/*
 *  File: /deployment/socket.ts
 *  Project: api
 *  File Created: 19-01-2024 14:36:10
 *  Author: Clay Risser
 *  -----
 *  BitSpur (c) Copyright 2024
 *
 *  Licensed under the GNU Affero General Public License (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://www.gnu.org/licenses/agpl-3.0.en.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *  You can be released from the requirements of the license by purchasing
 *  a commercial license. Buying such a license is mandatory as soon as you
 *  develop commercial activities involving this software without disclosing
 *  the source code of your own applications.
 */

import ejs from 'ejs';
import fs from 'fs/promises';
import jsYaml from 'js-yaml';
import path from 'path';
import { Installer } from './installer';
import { DockerService, DockerServiceService } from '../docker';
import { config } from '../config';
import { PrismaClient } from '@prisma/client';
import { SafeInstaller } from './types';

export class Socket {
  static async new(
    docker: DockerService,
    dockerService: DockerServiceService,
    prisma: PrismaClient,
    name: string,
    deploymentName: string,
    environmentName = 'default',
  ) {
    const socketDeploymentDir = path.resolve(config.dirs.deployments, deploymentName);
    const socketPath = path.join(socketDeploymentDir, `${name}.socket.yaml`);
    try {
      await fs.access(socketPath);
    } catch {
      throw new Error(
        `socket '${name}' not found in the '${environmentName}' environment of the '${deploymentName}' deployment`,
      );
    }
    return new Socket(
      name,
      socketPath,
      await Installer.new(docker, dockerService, prisma, deploymentName, environmentName),
    );
  }

  interface: InterfaceDefinition;

  constructor(
    public readonly name: string,
    public readonly socketPath: string,
    public readonly installer: Installer,
  ) {}

  async config(): Promise<Record<string, string>> {
    const data = {
      ...(await this.getSafe(['config'])),
    };
    const parsed = jsYaml.load(
      await ejs.render(await fs.readFile(this.socketPath, 'utf8'), data, {
        async: true,
        openDelimiter: '#{',
        closeDelimiter: '}#',
        beautify: false,
        escape: (str) => str,
      }),
    ) as SocketSchema;
    return this._validateConfig(parsed.config);
  }

  async getSafe(ignore: string[] = []): Promise<SafeSocket> {
    const safe = {
      ...this.installer.values,
      config: ignore.includes('config') ? undefined : await this.config(),
      // ignores allocatePort because it mutates state
      ...(await this.installer.getSafe(['allocatePort'])),
      name: this.name,
      values: this.installer.values,
    };
    ignore.forEach((key) => delete safe[key]);
    return safe;
  }

  private _validateConfig(config: Record<string, string>) {
    return config;
  }
}

export interface SocketSchema {
  config: Record<string, string>;
}

export interface InterfaceDefinition {
  plug?: Record<string, any>;
  socket?: Record<string, any>;
}

export interface SafeSocket extends SafeInstaller {
  config?: Record<string, string>;
  name: string;
}
