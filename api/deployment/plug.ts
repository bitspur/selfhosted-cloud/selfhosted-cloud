/*
 *  File: /deployment/plug.ts
 *  Project: api
 *  File Created: 19-01-2024 14:36:01
 *  Author: Clay Risser
 *  -----
 *  BitSpur (c) Copyright 2024
 *
 *  Licensed under the GNU Affero General Public License (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://www.gnu.org/licenses/agpl-3.0.en.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *  You can be released from the requirements of the license by purchasing
 *  a commercial license. Buying such a license is mandatory as soon as you
 *  develop commercial activities involving this software without disclosing
 *  the source code of your own applications.
 */

import ejs from 'ejs';
import fs from 'fs/promises';
import jsYaml from 'js-yaml';
import type { SafeSocket } from './socket';
import { Installer } from './installer';
import { DockerService, DockerServiceService } from '../docker';
import { PrismaClient } from '@prisma/client';
import { Socket } from './socket';

export class Plug {
  static async new(
    docker: DockerService,
    dockerService: DockerServiceService,
    prisma: PrismaClient,
    name: string,
    plugPath: string,
    installer: Installer,
  ): Promise<Plug> {
    const socketReference = ((await jsYaml.load(await fs.readFile(plugPath, 'utf8'))) as PlugSchema)?.socket;
    if (!socketReference.deployment) {
      throw new Error(`socket reference in plug '${name}' missing deployment`);
    }
    return new Plug(
      name,
      plugPath,
      installer,
      await Socket.new(docker, dockerService, prisma, name, socketReference.deployment, socketReference.environment),
    );
  }

  constructor(
    public readonly name: string,
    public readonly plugPath: string,
    public readonly installer: Installer,
    public readonly socket: Socket,
  ) {}

  async config(): Promise<Record<string, string>> {
    const data = {
      ...(await this.getSafe(['config'])),
    };
    const parsed = jsYaml.load(
      await ejs.render(await fs.readFile(this.plugPath, 'utf8'), data, {
        async: true,
        openDelimiter: '#{',
        closeDelimiter: '}#',
        beautify: false,
        escape: (str) => str,
      }),
    ) as PlugSchema;
    return this.validateConfig(parsed.config);
  }

  async getSafe(ignore: string[] = []): Promise<SafePlug> {
    const safe = {
      ...this.installer.values,
      config: ignore.includes('config') ? undefined : await this.config(),
      name: this.name,
      socket: await this.socket.getSafe(),
      values: this.installer.values,
    };
    ignore.forEach((key) => delete safe[key]);
    return safe;
  }

  private validateConfig(config: Record<string, string>) {
    return config;
  }
}

export interface PlugSchema {
  socket: SocketReference;
  config: Record<string, string>;
}

export interface SocketReference {
  deployment: string;
  environment?: string;
}

export interface SafePlug {
  config?: Record<string, string>;
  name: string;
  socket: SafeSocket;
}
