/*
 *  File: /deployment/types.ts
 *  Project: api
 *  File Created: 20-01-2024 03:29:33
 *  Author: Clay Risser
 *  -----
 *  BitSpur (c) Copyright 2024
 *
 *  Licensed under the GNU Affero General Public License (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://www.gnu.org/licenses/agpl-3.0.en.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *  You can be released from the requirements of the license by purchasing
 *  a commercial license. Buying such a license is mandatory as soon as you
 *  develop commercial activities involving this software without disclosing
 *  the source code of your own applications.
 */

import type { DockerServiceExecResult } from '../docker';
import type { Environment as PrismaEnvironment, Deployment as PrismaDeployment } from '@prisma/client';
import type { ExecOptions, Format } from './installer';

export interface DockerCompose {
  version: string;
  services: Services;
  networks?: Networks;
  volumes?: Volumes;
  configs?: Configs;
}

export interface Services {
  [key: string]: Service;
}

export interface Service {
  image?: string;
  build?: string | Build;
  ports?: string[];
  volumes?: string[];
  environment?: Environment;
  networks?: string[];
  deploy?: Deploy;
}

export interface Environment {
  [key: string]: string;
}

export interface Build {
  context: string;
  dockerfile?: string;
  args?: Args;
}

export interface Args {
  [key: string]: string;
}

export interface Networks {
  [key: string]: Network;
}

export interface Network {
  driver?: string;
  external?: boolean;
  ipam?: Ipam;
}

export interface Ipam {
  driver?: string;
  config?: Config[];
}

export interface Config {
  subnet: string;
}

export interface Volumes {
  [key: string]: Volume;
}

export interface Volume {
  driver?: string;
  driver_opts?: VolumeDriverOpts;
}

export interface VolumeDriverOpts {
  type?: string;
  o?: string;
  device?: string;
}

export interface Deploy {
  replicas?: number;
  update_config?: UpdateConfig;
  resources?: Resources;
}

export interface UpdateConfig {
  parallelism?: number;
  delay?: string;
  failure_action?: string;
  monitor?: string;
  max_failure_ratio?: number;
  order?: string;
}

export interface Resources {
  limits?: Limits;
  reservations?: Reservations;
}

export interface Limits {
  cpus?: string;
  memory?: string;
}

export interface Reservations {
  cpus?: string;
  memory?: string;
}

export interface Configs {
  [key: string]: Config;
}

export interface App {
  name: string;
  overlays?: Overlays;
  primaryService?: string;
  services?: Record<string, AppService>;
  version?: string;
}

export type Overlays = Record<string, OverlayConfig | undefined>;

export interface OverlayConfig {
  networks?: OverlayConfigBase;
  services?: OverlayConfigBase;
  volumes?: OverlayConfigBase;
}

export interface OverlayConfigBase {
  include?: string[];
  ignore?: string[];
}

export interface DeploymentPaths {
  app?: string;
  compose: string;
  plugs: string[];
  questions?: string;
  readme?: string;
  sockets: string[];
  values: string[];
}

export type ValuePrimitive = string | number | boolean | null | undefined;

export interface ValueArray extends Array<ValuePrimitive | ValueObject | ValueArray> {}

export interface ValueObject extends Record<string, ValuePrimitive | ValueObject | ValueArray> {}

export type Values = Record<string, ValuesService>;

export interface ValuesService {
  replicas?: number;
  image?: string;
  networking?: ValuesServiceNetworking;
  persistence?: ValuesServicePersistence;
  config?: Record<string, string>;
}

export interface ValuesServiceNetworking {
  hostname: string;
  ports: Record<string, boolean | number>;
  tls: boolean;
}

export interface ValuesServicePersistence {
  enabled: boolean;
}

export interface SafeInstaller extends PrismaDeployment {
  app: App;
  baseUrl: GetBaseUrlMethods;
  compose?: (format?: Format.Object) => Promise<DockerCompose | undefined>;
  env: (serviceName: string, key: string) => Promise<string | undefined>;
  environment: PrismaEnvironment;
  host: GetHostMethods;
  hostname: GetHostnameMethods;
  inspect: (serviceName: string) => Promise<any | undefined>;
  name: string;
  paths: DeploymentPaths;
  publishedPorts: Record<string, number>;
  service: (serviceName: string) => Promise<any | undefined>;
  stackName: string;
  values: Values;
  exec: (
    serviceName: string,
    command: string,
    args?: string[],
    options?: ExecOptions,
  ) => Promise<DockerServiceExecResult | DockerServiceExecResult[]>;
}

export interface GetBaseUrlMethods {
  get: (serviceName: string, portName?: string) => string;
  getPublic: (serviceName: string, portName?: string) => string | undefined;
  getPrivate: (serviceName: string, portName?: string) => string;
}

export interface GetHostMethods {
  get: (serviceName: string, portName?: string) => string;
  getPublic: (serviceName: string, portName?: string) => string | undefined;
  getPrivate: (serviceName: string, portName?: string) => string;
}

export interface GetHostnameMethods {
  get: (serviceName: string) => string;
  getPublic: (serviceName: string) => string | undefined;
  getPrivate: (serviceName: string) => string;
}

export interface AppService {
  containerPorts?: Record<string, number>;
}
