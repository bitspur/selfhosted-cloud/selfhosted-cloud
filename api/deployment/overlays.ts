/*
 *  File: /deployment/overlays.ts
 *  Project: api
 *  File Created: 15-02-2024 15:46:56
 *  Author: Clay Risser
 *  -----
 *  BitSpur (c) Copyright 2024
 *
 *  Licensed under the GNU Affero General Public License (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://www.gnu.org/licenses/agpl-3.0.en.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *  You can be released from the requirements of the license by purchasing
 *  a commercial license. Buying such a license is mandatory as soon as you
 *  develop commercial activities involving this software without disclosing
 *  the source code of your own applications.
 */

import fs from 'fs/promises';
import path from 'path';
import type { DockerCompose, OverlayConfig } from './types';
import { Installer } from './installer';

export type OverlayFunction = (
  compose: DockerCompose,
  config: OverlayConfig,
  installer: Installer,
) => Promise<DockerCompose> | DockerCompose;

export const overlays: Record<string, OverlayFunction> = {
  privateNetwork(compose: DockerCompose, config: OverlayConfig): DockerCompose {
    Object.entries(compose.services)
      .filter(([serviceName]) => (config.services?.include ? config.services.include.includes(serviceName) : true))
      .forEach(([serviceName, service]) => {
        if (config.services?.ignore?.includes(serviceName)) return;
        if (!service.networks) service.networks = [];
        if (!service.networks.includes('private')) service.networks.push('private');
      });
    return {
      ...compose,
      networks: {
        ...compose.networks,
        private: {
          external: true,
        },
      },
    };
  },
  publicNetwork(compose: DockerCompose, config: OverlayConfig): DockerCompose {
    Object.entries(compose.services)
      .filter(([serviceName]) => (config.services?.include ? config.services.include.includes(serviceName) : true))
      .forEach(([serviceName, service]) => {
        if (config.services?.ignore?.includes(serviceName)) return;
        if (!service.networks) service.networks = [];
        if (!service.networks.includes('public')) service.networks.push('public');
      });
    return {
      ...compose,
      networks: {
        ...compose.networks,
        public: {
          external: true,
        },
      },
    };
  },
  async pathVolumes(compose: DockerCompose, config: OverlayConfig, installer: Installer): Promise<DockerCompose> {
    const volumesDir = path.resolve(
      installer.config.dirs.runtime,
      installer.deployment.name,
      installer.environment.name,
      'volumes',
    );
    const volumePaths: string[] = [];
    Object.entries(compose.services)
      .filter(([serviceName]) => (config.services?.include ? config.services.include.includes(serviceName) : true))
      .forEach(([serviceName, service]) => {
        if (config.services?.ignore?.includes(serviceName)) return;
        if (service.volumes) {
          service.volumes.forEach((volume, i) => {
            const volumeParts = volume.split(':') as [string, ...string[]];
            const volumeName = volumeParts[0];
            if (volumeParts.length > 1 && !volumeName.includes('/') && !volumeName.startsWith('.')) {
              if (config.volumes?.ignore?.includes(volumeParts[0])) return;
              const volumePath = path.resolve(volumesDir, volumeName);
              volumePaths.push(volumePath);
              service.volumes![i] = `${volumePath}:${volumeParts[1]}`;
            }
          });
        }
      });
    await Promise.all(
      volumePaths.map(async (volumePath) => {
        try {
          await fs.access(volumePath);
        } catch {
          await fs.mkdir(volumePath, { recursive: true });
        }
      }),
    );
    return compose;
  },
  async localVolumes(compose: DockerCompose, config: OverlayConfig, installer: Installer): Promise<DockerCompose> {
    const volumeNames = new Set<string>();
    const volumesDir = path.resolve(
      installer.config.dirs.runtime,
      installer.deployment.name,
      installer.environment.name,
      'volumes',
    );
    Object.entries(compose.services)
      .filter(([serviceName]) => (config.services?.include ? config.services.include.includes(serviceName) : true))
      .forEach(([serviceName, service]) => {
        if (config.services?.ignore?.includes(serviceName)) return;
        if (service.volumes) {
          service.volumes.forEach((volume) => {
            const volumeParts = volume.split(':') as [string, ...string[]];
            if (!volumeParts[0].includes('/') && !volumeParts[0].startsWith('.')) {
              if (config.volumes?.ignore?.includes(volumeParts[0])) return;
              if (!config.volumes?.include || config.volumes.include.includes(volumeParts[0])) {
                volumeNames.add(volumeParts[0]);
              }
            }
          });
        }
      });
    await Promise.all(
      [...volumeNames].map(async (volumeName) => {
        const volumePath = path.join(volumesDir, volumeName);
        try {
          await fs.access(volumePath);
        } catch {
          await fs.mkdir(volumePath, { recursive: true });
        }
      }),
    );
    return {
      ...compose,
      volumes: {
        ...[...volumeNames].reduce(
          (volumes, volumeName) => ({
            ...volumes,
            [volumeName]: {
              driver: 'local',
              driver_opts: {
                type: 'none',
                o: 'bind',
                device: path.resolve(
                  installer.config.dirs.runtime,
                  installer.deployment.name,
                  installer.environment.name,
                  'volumes',
                  volumeName,
                ),
              },
            },
          }),
          {},
        ),
        ...compose.volumes,
      },
    };
  },
  async ports(compose: DockerCompose, config: OverlayConfig, installer: Installer): Promise<DockerCompose> {
    Object.entries(installer.app.services || {})
      .filter(([serviceName]) => (config.services?.include ? config.services.include.includes(serviceName) : true))
      .forEach(([serviceName, appService]) => {
        if (config.services?.ignore?.includes(serviceName)) return;
        Object.entries(appService.containerPorts || {}).forEach(([portName, containerPort]) => {
          const service = compose.services[serviceName];
          if (!service) return;
          if (!service.ports) service.ports = [];
          const publishedPort = installer.publishedPorts[portName];
          if (!publishedPort) return;
          service.ports = [...new Set([...service.ports, `${publishedPort}:${containerPort}`])];
        });
      });
    return compose;
  },
};
