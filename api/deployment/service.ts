/*
 *  File: /deployment/service.ts
 *  Project: api
 *  File Created: 19-01-2024 13:15:56
 *  Author: Clay Risser
 *  -----
 *  BitSpur (c) Copyright 2024
 *
 *  Licensed under the GNU Affero General Public License (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://www.gnu.org/licenses/agpl-3.0.en.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *  You can be released from the requirements of the license by purchasing
 *  a commercial license. Buying such a license is mandatory as soon as you
 *  develop commercial activities involving this software without disclosing
 *  the source code of your own applications.
 */

import fs from 'fs/promises';
import jsYaml from 'js-yaml';
import path from 'path';
import tar from 'tar';
import unzipper from 'unzipper';
import { AppReference, InstallAppArgs, InstallAppResult, RemoveAppArgs } from './dto';
import { EnvironmentService } from './environment/service';
import { GraphQLError } from 'graphql';
import { Injectable } from '@multiplatform.one/typegraphql';
import { PrismaClient } from '@prisma/client';
import { config } from '../config';
import { transformCountFieldIntoSelectRelationsCount } from '../generated/type-graphql/helpers';

@Injectable()
export class DeploymentService {
  constructor(
    private readonly prisma: PrismaClient,
    private readonly environmentService: EnvironmentService,
  ) {}

  async installApp(data: InstallAppArgs, count?: any): Promise<InstallAppResult> {
    if (
      await this.prisma.deployment.findFirst({
        where: {
          name: data.name,
        },
      })
    ) {
      throw new GraphQLError(`deployment ${data.name} already exists`);
    }
    const deploymentDir = this.getDeploymentDir(data.name);
    try {
      const appName = await this.downloadApp(data.app, deploymentDir);
      const deployment = await this.prisma.deployment.create({
        data: {
          appName,
          name: data.name,
        },
        ...(count && transformCountFieldIntoSelectRelationsCount(count)),
      });
      return this.environmentService.installEnvironment({
        name: 'default',
        deployment: { connect: { id: deployment.id } },
        values: data.values,
      });
    } catch (err) {
      const deployment = await this.prisma.deployment.findUnique({
        where: {
          name: data.name,
        },
      });
      if (deployment) {
        await this.prisma.deployment.delete({
          where: {
            name: data.name,
          },
        });
      }
      try {
        await fs.rm(deploymentDir, { recursive: true, force: true });
      } catch (err) {
        if (err.code !== 'ENOENT') throw err;
      }
      throw err;
    }
  }

  async removeApp(args: RemoveAppArgs) {
    const deployment = await this.prisma.deployment.findUnique({
      where: {
        name: args.name,
      },
      include: {
        environments: true,
      },
    });
    if (!deployment) throw new GraphQLError(`deployment ${args.name} does not exist`);
    await Promise.all(
      deployment.environments.map((environment) =>
        this.environmentService.removeEnvironment(args.name, environment.name),
      ),
    );
    try {
      await fs.rm(this.getDeploymentDir(deployment.name), { recursive: true });
    } catch (err) {
      if (err.code !== 'ENOENT') throw err;
    }
    return this.prisma.deployment.delete({
      where: {
        id: deployment.id,
      },
    });
  }

  private async downloadApp(app: AppReference, deploymentDir: string) {
    await fs.mkdir(deploymentDir, { recursive: true });
    // TODO: support repository and version
    if (app.url) {
      const url = new URL(app.url);
      switch (url.protocol) {
        case 'file:': {
          const appPath = path.resolve(process.cwd(), url.hostname + url.pathname);
          const stats = await fs.stat(appPath);
          if (stats.isDirectory()) {
            await fs.cp(appPath, deploymentDir, { recursive: true });
          } else if (stats.isFile()) {
            if (appPath.endsWith('.tar') || appPath.endsWith('.tar.gz')) {
              await tar.x({ file: appPath, C: deploymentDir });
            } else if (appPath.endsWith('.zip')) {
              await unzipper.Extract({ path: deploymentDir }).promise();
            }
          }
          break;
        }
        default: {
          throw new GraphQLError('only file protocol is supported');
        }
      }
      return Promise.resolve(
        (jsYaml.load((await fs.readFile(path.join(deploymentDir, 'app.yaml'))).toString()) as { name?: string })?.name,
      )
        .catch(() => undefined)
        .then(
          (name?: string) =>
            name || path.basename(url.href, url.href.endsWith('.tar.gz') ? '.tar.gz' : path.extname(url.href)),
        );
    }
    throw new GraphQLError('only url is supported');
  }

  private getDeploymentDir(deploymentName: string) {
    return path.join(config.dirs.deployments, deploymentName);
  }
}
