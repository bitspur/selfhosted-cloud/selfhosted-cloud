/**
 * File: /pages/graphql.tsx
 * Project: @platform/next
 * File Created: 19-01-2024 06:33:59
 * Author: Clay Risser
 * -----
 * BitSpur (c) Copyright 2024
 *
 * Licensed under the GNU Affero General Public License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.gnu.org/licenses/agpl-3.0.en.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * You can be released from the requirements of the license by purchasing
 * a commercial license. Buying such a license is mandatory as soon as you
 * develop commercial activities involving this software without disclosing
 * the source code of your own applications.
 */

import 'graphiql/graphiql.css';
import React, { useEffect, useState } from 'react';
import { GraphiQL, GraphiQLInterface, GraphiQLProvider } from 'graphiql';
import { config } from 'app/config';
import { createClient } from 'graphql-ws';
import { createGraphiQLFetcher } from '@graphiql/toolkit';
import { explorerPlugin } from '@graphiql/plugin-explorer';
import { useUrlSearchParams } from 'use-url-search-params';
import { v4 as uuid } from 'uuid';
import { withAuthenticated, useKeycloak } from '@multiplatform.one/keycloak';

const httpUrl = `${config.get('API_BASE_URL') || 'http://localhost:5001'}/graphql`;
const wsUrl = httpUrl.replace(/^http/, 'ws');

function GraphiQLPage() {
  const [ready, setReady] = useState(false);
  const keycloak = useKeycloak();
  const [params, setParams] = useUrlSearchParams(
    {
      query: `query {
  accessToken
}`,
    },
    { query: String },
    false,
  );
  const [query, setQuery] = useState(params.query?.toString());

  useEffect(() => {
    if (!keycloak?.authenticated) return;
    setReady(true);
  }, [keycloak?.authenticated]);

  if (!ready) return <>{}</>;
  return (
    <div
      style={{
        width: '100vw',
        height: '100vh',
      }}
    >
      <GraphiQLProvider
        schemaDescription
        query={query}
        headers={params.headers?.toString()}
        plugins={[
          explorerPlugin({
            query,
            onEdit: setQuery,
            showAttribution: true,
          } as any),
        ]}
        fetcher={createGraphiQLFetcher({
          url: httpUrl,
          async fetch(input: RequestInfo, init?: RequestInit) {
            return fetch(input, {
              ...init,
              headers: {
                ...init?.headers,
                'X-Request-Id': uuid(),
                ...(keycloak?.token ? { Authorization: `Bearer ${keycloak.token}` } : {}),
              },
            });
          },
          wsClient: createClient({
            url: wsUrl,
          }),
        })}
      >
        <GraphiQLInterface
          isHeadersEditorEnabled
          defaultEditorToolsVisibility
          onEditQuery={(query) => setParams({ query })}
          onEditHeaders={(headers) => setParams({ headers })}
        >
          <GraphiQL.Logo>
            <div style={{ display: 'flex', alignItems: 'center' }}>
              {keycloak?.authenticated && (
                <button
                  onClick={() => keycloak?.logout()}
                  type="button"
                  className="graphiql-un-styled graphiql-tab-close"
                >
                  Logout
                </button>
              )}
            </div>
          </GraphiQL.Logo>
        </GraphiQLInterface>
      </GraphiQLProvider>
    </div>
  );
}

export default withAuthenticated(GraphiQLPage);
