# File: /Mkpmfile
# Project: @platform/next
# File Created: 19-01-2024 06:33:59
# Author: Clay Risser
# -----
# BitSpur (c) Copyright 2024
#
# Licensed under the GNU Affero General Public License (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.gnu.org/licenses/agpl-3.0.en.html
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# You can be released from the requirements of the license by purchasing
# a commercial license. Buying such a license is mandatory as soon as you
# develop commercial activities involving this software without disclosing
# the source code of your own applications.

include $(MKPM)/mkpm
DEFAULT_ENV ?= $(PROJECT_ROOT)/.env
include $(MKPM)/gnu
include $(MKPM)/dotenv
include $(MKPM)/chain
include $(MKPM)/yarn
include $(PROJECT_ROOT)/shared.mk

export NEXTAUTH_SECRET := $(SECRET)
export NEXTAUTH_URL := $(NEXT_BASE_URL)

NEXT ?= $(YARN) next
NEXT_PORT ?= 5000

ACTIONS += deps
$(ACTION)/deps: $(PROJECT_ROOT)/package.json package.json
ifneq (,$(SUBPROC))
	@$(MAKE) -C $(PROJECT_ROOT) -f $(PROJECT_ROOT)/Mkpmfile \~deps DEPS_ARGS=$(DEPS_ARGS)
else
	@$(YARN) workspaces focus $(DEPS_ARGS)
endif
	@$(call done,$@)

ACTIONS += format~deps
$(ACTION)/format: $(call git_deps,\.((json)|(md)|([jt]sx?))$$)
#	-@$(call eslint_format,$?)
	-@$(call prettier,$?,$(FORMAT_ARGS))
	@$(call done,$@)

ACTIONS += spellcheck~format
$(ACTION)/spellcheck: $(call git_deps,\.(md)$$)
	-@$(call cspell,$?,$(SPELLCHECK_ARGS))
	@$(call done,$@)

ACTIONS += lint~spellcheck
$(ACTION)/lint: $(call git_deps,\.([jt]sx?)$$)
	-@$(call eslint,$?,$(LINT_ARGS))
	@$(call done, $@)

ACTIONS += test~lint
$(ACTION)/test: $(call git_deps,\.([jt]sx?)$$)
	-@$(call jest,$?,$(TEST_ARGS))
	@$(call done, $@)

ACTIONS += build~test
$(ACTION)/build: $(call git_deps,\.([jt]sx?)$$)
	@$(NEXT) build $(BUILD_ARGS)
	@$(call done,$@)

ACTIONS += export~test
$(ACTION)/export: $(call git_deps,\.([jt]sx?)$$)
	@NEXT_STATIC=1 $(NEXT) build
	@NEXT_STATIC=1 $(NEXT) export
	@$(call done,$@)

.PHONY: dev +dev
dev: | ~deps gql/~generate +dev
+dev:
	@$(NEXT) dev -p $(NEXT_PORT)

.PHONY: start +start
start: | ~deps gql/~generate +build +start
+start:
	@$(NEXT) start -p $(NEXT_PORT)

.PHONY: gql/%
gql/%:
	@$(MKPM_MAKE) -C $(PROJECT_ROOT)/gql $*

.PHONY: clean
clean:
	-@$(MKCACHE_CLEAN)
	-@$(JEST) --clearCache $(NOFAIL)
	-@$(WATCHMAN) watch-del-all $(NOFAIL)
	-@$(GIT) clean -fxd \
		$(MKPM_GIT_CLEAN_FLAGS) \
		$(YARN_GIT_CLEAN_FLAGS) \
		$(NOFAIL)

-include $(call chain)
