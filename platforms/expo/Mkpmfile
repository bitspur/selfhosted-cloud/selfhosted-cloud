# File: /Mkpmfile
# Project: @platform/expo
# File Created: 19-01-2024 06:33:59
# Author: Clay Risser
# -----
# BitSpur (c) Copyright 2024
#
# Licensed under the GNU Affero General Public License (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.gnu.org/licenses/agpl-3.0.en.html
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# You can be released from the requirements of the license by purchasing
# a commercial license. Buying such a license is mandatory as soon as you
# develop commercial activities involving this software without disclosing
# the source code of your own applications.

include $(MKPM)/mkpm
DOTENV ?= $(PROJECT_ROOT)/.env
include $(MKPM)/gnu
include $(MKPM)/chain
include $(MKPM)/dotenv
include $(MKPM)/yarn
include $(PROJECT_ROOT)/shared.mk

ACTIONS += deps
$(ACTION)/deps: $(PROJECT_ROOT)/package.json package.json
ifneq (,$(SUBPROC))
	@$(MAKE) -C $(PROJECT_ROOT) -f $(PROJECT_ROOT)/Mkpmfile \~deps DEPS_ARGS=$(DEPS_ARGS)
else
	@$(YARN) workspaces focus $(DEPS_ARGS)
endif
	@$(call done,$@)

ACTIONS += format~deps
$(ACTION)/format: $(call git_deps,\.((json)|(md)|([jt]sx?))$$)
#	-@$(call eslint_format,$?)
	-@$(call prettier,$?,$(FORMAT_ARGS))
	@$(call done,$@)

ACTIONS += spellcheck~format
$(ACTION)/spellcheck: $(call git_deps,\.(md)$$)
	-@$(call cspell,$?,$(SPELLCHECK_ARGS))
	@$(call done,$@)

ACTIONS += lint~spellcheck
$(ACTION)/lint: $(call git_deps,\.([jt]sx?)$$)
	-@$(call eslint,$?,$(LINT_ARGS))
	@$(call done,$@)

ACTIONS += test~lint
$(ACTION)/test: $(call git_deps,\.([jt]sx?)$$)
	-@$(call jest,$?,$(TEST_ARGS))
	@$(call done,$@)

EXPO_PORT ?= 8081
.PHONY: dev +dev
dev: | ~deps +dev
+dev:
	@$(EXPO) start -c -p $(EXPO_PORT) $(DEV_ARGS)

.PHONY: dev@web +dev@web
dev@web: | ~deps +dev@web
+dev@web:
	@$(EXPO) start -c -p $(EXPO_PORT) --web

.PHONY: dev@android +dev@android
dev@android: | ~deps +dev@android
+dev@android:
	@$(EXPO) start -c -p $(EXPO_PORT) --android

.PHONY: dev@ios +dev@ios
dev@ios: | ~deps dev@ios
+dev@ios:
	@$(EXPO) start -c -p $(EXPO_PORT) --ios

.PHONY: clean
clean:
	-@$(MKCACHE_CLEAN)
	-@$(JEST) --clearCache $(NOFAIL)
	-@$(WATCHMAN) watch-del-all $(NOFAIL)
	-@$(GIT) clean -fXd \
		$(MKPM_GIT_CLEAN_FLAGS) \
		$(YARN_GIT_CLEAN_FLAGS) \
		$(NOFAIL)

-include $(call chain)
