/**
 * File: /screens/user/index.tsx
 * Project: app
 * File Created: 19-01-2024 06:33:59
 * Author: Clay Risser
 * -----
 * BitSpur (c) Copyright 2024
 *
 * Licensed under the GNU Affero General Public License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.gnu.org/licenses/agpl-3.0.en.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * You can be released from the requirements of the license by purchasing
 * a commercial license. Buying such a license is mandatory as soon as you
 * develop commercial activities involving this software without disclosing
 * the source code of your own applications.
 */

import React from 'react';
import { Button, Paragraph, YStack } from 'ui';
import { ChevronLeft } from '@tamagui/lucide-icons';
import { createParam } from 'solito';
import { useLink } from 'solito/link';
import { useTranslation } from 'multiplatform.one';
import { withDefaultLayout } from 'app/layouts/Default';

const { useParam } = createParam<{ id: string }>();

function UserScreen() {
  const [id] = useParam('id');
  const linkProps = useLink({ href: '/' });
  const { t } = useTranslation();

  return (
    <YStack f={1} jc="center" ai="center" space>
      <Paragraph ta="center" fow="800">{`User ID: ${id}`}</Paragraph>
      <Button {...linkProps} icon={ChevronLeft}>
        {t('screens.user.goHome')}
      </Button>
    </YStack>
  );
}

export default withDefaultLayout(UserScreen);
