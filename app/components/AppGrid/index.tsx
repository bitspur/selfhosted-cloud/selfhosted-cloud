/**
 * File: /components/AppGrid/index.tsx
 * Project: app
 * File Created: 12-02-2024 07:38:25
 * Author: Clay Risser
 * -----
 * BitSpur (c) Copyright 2024
 *
 * Licensed under the GNU Affero General Public License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.gnu.org/licenses/agpl-3.0.en.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * You can be released from the requirements of the license by purchasing
 * a commercial license. Buying such a license is mandatory as soon as you
 * develop commercial activities involving this software without disclosing
 * the source code of your own applications.
 */

import React, { useState } from 'react';
import { YStack, SimpleImage, H5 } from 'ui';
import { AppSheet } from 'app/components/AppSheet';
import { useRouter } from 'solito/router';

export interface AppGridProps {
  logo: string;
  name: string;
  // style?: React.CSSProperties;
}

export const AppGrid = ({ name, logo, ...props }: AppGridProps) => {
  const [sheetVisible, setSheetVisible] = useState(false);
  const router = useRouter();

  const handleRightClick = (event) => {
    if (event.button === 2) {
      event.preventDefault();
      setSheetVisible(true);
    }
  };

  const handleAppAction = (action: string) => {
    return router.push(`/${name}/${action}`);
  };

  return (
    // <XStack m={10} p={10} ai="center" height={120} width={120} jc="center" {...props}>
    //   <YStack
    //     flexWrap="wrap"
    //     jc="center"
    //     ai="center"
    //     // @ts-ignore
    //     onContextMenu={handleRightClick}
    //   >
    //     <SimpleImage src={logo} alt={name} height={70} width={70} />
    //     <H5 color="red" overflow="visible" mt="$3">
    //       {name}
    //     </H5>
    //   </YStack>
    //   {sheetVisible && <AppSheet setVisible={setSheetVisible} onAction={handleAppAction} />}
    // </XStack>
    <YStack {...props}>
      <YStack
        gap="$2"
        width={274}
        height={176}
        justifyContent="center"
        alignItems="center"
        // @ts-ignore
        onContextMenu={handleRightClick}
      >
        <YStack
          height={100}
          width={100}
          justifyContent="center"
          alignItems="center"
          borderRadius="$4"
          backgroundColor="$backgroundFocus"
          hoverStyle={{ backgroundColor: '$backgroundPress' }}
        >
          <SimpleImage src={logo} alt={name} height={70} width={70} />
        </YStack>
        <H5 fontWeight="bold">{name}</H5>
      </YStack>
      {sheetVisible && <AppSheet setVisible={setSheetVisible} onAction={handleAppAction} />}
    </YStack>
  );
};
