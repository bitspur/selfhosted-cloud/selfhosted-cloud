/**
 * File: /components/AppSheet/index.tsx
 * Project: app
 * File Created: 23-02-2024 06:02:57
 * Author: Clay Risser
 * -----
 * BitSpur (c) Copyright 2024
 *
 * Licensed under the GNU Affero General Public License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.gnu.org/licenses/agpl-3.0.en.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * You can be released from the requirements of the license by purchasing
 * a commercial license. Buying such a license is mandatory as soon as you
 * develop commercial activities involving this software without disclosing
 * the source code of your own applications.
 */

import React, { useState } from 'react';
import { Sheet, Tabs, YStack, SizableText } from 'ui';
import { SimpleTabs } from 'app/components/SimpleTabs';

export interface AppSheetProps {
  setVisible: (visible: boolean) => void;
  onAction: (action: string) => void;
}

export const AppSheet = ({ setVisible, onAction }: AppSheetProps) => {
  const [position, setPosition] = useState(0);

  const tabsData = ['Config', 'Users', 'Backups', 'Advanced', 'Start', 'Stop', 'Uninstall'];

  const horizontalTabs = () => (
    <>
      {tabsData.map((tabName, index) => (
        <Tabs.Tab key={index} value={`tab${index + 1}`} onPress={() => onAction(tabName)} minWidth="$15">
          <SizableText fontFamily="$body">{tabName}</SizableText>
        </Tabs.Tab>
      ))}
    </>
  );

  return (
    <Sheet
      modal
      open={true}
      onOpenChange={setVisible}
      snapPoints={[40]}
      position={position}
      onPositionChange={setPosition}
      dismissOnSnapToBottom
    >
      <Sheet.Overlay />
      <Sheet.Frame ai="center" jc="center" backgroundColor="$gray3Light">
        <Sheet.Handle />
        <YStack ai="center" jc="center" space>
          <SimpleTabs tabs={[horizontalTabs()]} />
        </YStack>
      </Sheet.Frame>
    </Sheet>
  );
};
