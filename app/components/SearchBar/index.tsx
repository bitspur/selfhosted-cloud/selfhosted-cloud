/**
 * File: /components/SearchBar/index.tsx
 * Project: app
 * File Created: 12-02-2024 07:38:25
 * Author: Clay Risser
 * -----
 * BitSpur (c) Copyright 2024
 *
 * Licensed under the GNU Affero General Public License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.gnu.org/licenses/agpl-3.0.en.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * You can be released from the requirements of the license by purchasing
 * a commercial license. Buying such a license is mandatory as soon as you
 * develop commercial activities involving this software without disclosing
 * the source code of your own applications.
 */

import React from 'react';
import { Search } from '@tamagui/lucide-icons';
import { Button, Input, XStack } from 'ui';

export interface SearchBarProps {
  text?: string;
  onChangeText?: (text: string) => void;
}

export const SearchBar = ({ text, onChangeText }: SearchBarProps) => {
  const [searchValue, setSearchValue] = React.useState(text || '');

  const handleChangeText = (text: string) => {
    setSearchValue(text);
    if (onChangeText) onChangeText(text);
  };

  return (
    <XStack
      borderBlockColor="light"
      borderRadius={30}
      backgroundColor="$gray10Dark"
      borderStyle="solid"
      borderWidth={2}
      height={50}
      ai="center"
      $md={{ width: 190 }}
    >
      <Button
        height={40}
        hoverStyle={{
          backgroundColor: '$gray10Dark',
        }}
        focusStyle={{
          backgroundColor: '$gray10Dark',
          outlineColor: '$gray10Dark',
        }}
        backgroundColor="$gray10Dark"
        borderRadius={40}
        size={40}
        icon={<Search size={15} />}
      />
      <Input
        height={30}
        value={searchValue}
        onChangeText={handleChangeText}
        fontSize={16}
        backgroundColor="$backgroundTransparent"
        borderColor="$colorTransparent"
        borderWidth={0}
        placeholder="Search"
        placeholderTextColor="$gray50"
        focusStyle={{
          outlineWidth: 0,
        }}
      />
    </XStack>
  );
};
