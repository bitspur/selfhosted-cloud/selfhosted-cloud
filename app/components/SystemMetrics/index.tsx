/**
 * File: /components/SystemMetrics/index.tsx
 * Project: app
 * File Created: 12-02-2024 07:38:25
 * Author: Clay Risser
 * -----
 * BitSpur (c) Copyright 2024
 *
 * Licensed under the GNU Affero General Public License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.gnu.org/licenses/agpl-3.0.en.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * You can be released from the requirements of the license by purchasing
 * a commercial license. Buying such a license is mandatory as soon as you
 * develop commercial activities involving this software without disclosing
 * the source code of your own applications.
 */

import { Paragraph, XStack } from 'ui';
import React from 'react';
import { Activity, BatteryMedium, Cpu, MemoryStick } from '@tamagui/lucide-icons';

export const SystemMetrics = () => {
  return (
    <XStack ai="flex-start" paddingVertical="$1" paddingHorizontal="$2" backgroundColor="$gray7Light">
      <XStack jc="space-between" space>
        <Activity size="$2" />
        <Paragraph color="red">62%</Paragraph>
        <BatteryMedium size="$2" />
        <Paragraph color="red">12%</Paragraph>
      </XStack>
      <XStack space>
        <XStack space>
          <MemoryStick size="$2" />
          <Paragraph color="red">12%</Paragraph>
        </XStack>
        <XStack space>
          <Cpu size="$2" />
          <Paragraph color="red">34%</Paragraph>
        </XStack>
      </XStack>
    </XStack>
  );
};
