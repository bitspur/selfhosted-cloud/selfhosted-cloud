/**
 * File: /components/SimpleTabs/TabsContent.stories.tsx
 * Project: app
 * File Created: 15-02-2024 11:16:30
 * Author: Clay Risser
 * -----
 * BitSpur (c) Copyright 2024
 *
 * Licensed under the GNU Affero General Public License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.gnu.org/licenses/agpl-3.0.en.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * You can be released from the requirements of the license by purchasing
 * a commercial license. Buying such a license is mandatory as soon as you
 * develop commercial activities involving this software without disclosing
 * the source code of your own applications.
 */

import React from 'react';
import { SimpleTabs, TabsContent } from './index';
import { H5, SizableText, Tabs } from 'ui';

export default {
  title: 'components/SimpleTabs',
  component: SimpleTabs,
  parameters: { status: { type: 'beta' } },
};

const horizontalTabs = () => (
  <>
    <Tabs.Tab flex={1} value="tab1">
      <SizableText fontFamily="$body">Profile</SizableText>
    </Tabs.Tab>
    <Tabs.Tab flex={1} value="tab2">
      <SizableText fontFamily="$body">Connections</SizableText>
    </Tabs.Tab>
    <Tabs.Tab flex={1} value="tab3">
      <SizableText fontFamily="$body">Notifications</SizableText>
    </Tabs.Tab>
  </>
);

const verticalTabs = () => (
  <>
    <Tabs.Tab flex={1} value="tab1">
      <SizableText fontFamily="$body">Profile</SizableText>
    </Tabs.Tab>
    <Tabs.Tab flex={1} value="tab2">
      <SizableText fontFamily="$body">Connections</SizableText>
    </Tabs.Tab>
    <Tabs.Tab flex={1} value="tab3">
      <SizableText fontFamily="$body">Notifications</SizableText>
    </Tabs.Tab>
  </>
);

export const horizontal = () => (
  <SimpleTabs tabs={[horizontalTabs()]} defaultValue="tab1">
    <TabsContent value="tab1">
      <H5>Profile</H5>
    </TabsContent>

    <TabsContent value="tab2">
      <H5>Connections</H5>
    </TabsContent>

    <TabsContent value="tab3">
      <H5>Notifications</H5>
    </TabsContent>
  </SimpleTabs>
);

export const vertical = () => (
  <SimpleTabs tabs={[verticalTabs()]} orientation="vertical">
    <TabsContent value="tab1">
      <H5>Profile</H5>
    </TabsContent>

    <TabsContent value="tab2">
      <H5>Connections</H5>
    </TabsContent>

    <TabsContent value="tab3">
      <H5>Notifications</H5>
    </TabsContent>
  </SimpleTabs>
);
