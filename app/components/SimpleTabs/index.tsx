/**
 * File: /components/SimpleTabs/index.tsx
 * Project: app
 * File Created: 12-02-2024 07:38:25
 * Author: Clay Risser
 * -----
 * BitSpur (c) Copyright 2024
 *
 * Licensed under the GNU Affero General Public License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.gnu.org/licenses/agpl-3.0.en.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * You can be released from the requirements of the license by purchasing
 * a commercial license. Buying such a license is mandatory as soon as you
 * develop commercial activities involving this software without disclosing
 * the source code of your own applications.
 */

import React from 'react';
import type { TabsContentProps, TabsProps, TabsListProps } from 'ui';
import { Separator, Tabs, YStack, isWeb } from 'ui';

type SimpleTabsProps = TabsProps & {
  tabs: React.ReactNode[];
  orientation?: 'horizontal' | 'vertical';
  tabStyle?: TabsListProps;
  contentStyle?: TabsContentProps;
};

export function SimpleTabs({ tabs, orientation, tabStyle, children, ...props }: SimpleTabsProps) {
  const orientationView = orientation || 'horizontal';

  const tabsList = tabs && tabs.length > 0 ? tabs : [];

  return (
    <YStack
      flex={1}
      ai="stretch"
      jc="center"
      paddingHorizontal="$4"
      {...(isWeb && {
        position: 'unset' as any,
      })}
    >
      <Tabs
        orientation={orientationView}
        flexDirection={orientationView === 'horizontal' ? 'column' : 'row'}
        width="100%"
        height="auto"
        overflow="hidden"
        borderColor="$borderColor"
        {...props}
        jc="center"
        ai="center"
      >
        <Tabs.List
          disablePassBorderRadius="bottom"
          aria-label="Manage your account"
          {...tabStyle}
          separator={<Separator vertical={orientationView === 'vertical'} />}
        >
          {tabsList.map((tab, index) => (
            <React.Fragment key={index}>{tab}</React.Fragment>
          ))}
        </Tabs.List>
        <Separator vertical={orientationView === 'vertical'} />
        {children}
      </Tabs>
    </YStack>
  );
}

export const TabsContent = (props: TabsContentProps) => {
  return (
    <Tabs.Content
      backgroundColor="$background"
      padding="$2"
      alignItems="center"
      justifyContent="center"
      flex={1}
      {...props}
    >
      {props.children}
    </Tabs.Content>
  );
};
