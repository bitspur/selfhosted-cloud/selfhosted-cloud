/**
 * File: /organisms/Header/index.tsx
 * Project: app
 * File Created: 23-02-2024 06:02:57
 * Author: Clay Risser
 * -----
 * BitSpur (c) Copyright 2024
 *
 * Licensed under the GNU Affero General Public License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.gnu.org/licenses/agpl-3.0.en.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * You can be released from the requirements of the license by purchasing
 * a commercial license. Buying such a license is mandatory as soon as you
 * develop commercial activities involving this software without disclosing
 * the source code of your own applications.
 */

import { Profile } from 'app/components/Profile';
import React from 'react';
import { XStack, Paragraph, H5, SimpleImage, useAssets } from 'ui';

export const Header = () => {
  const [Ram, Disk, Processor] = useAssets([
    require('../../assets/ram.svg'),
    require('../../assets/disk.svg'),
    require('../../assets/processor.svg'),
  ]);

  return (
    // <XStack jc="flex-end" backgroundColor="$gray7Light" ai="center" space width="100%" height={40}>
    //   <XStack flexWrap="nowrap" flexShrink={1} ai="center" jc="space-around" ml="$2">
    //     <Paragraph color="$background" fontSize={12} fontFamily="$body" pr="$4">
    //       Welcome back, Admin
    //     </Paragraph>
    //     <Profile names={['Admin']} />
    //   </XStack>
    // </XStack>
    <XStack
      backgroundColor="$backgroundFocus"
      alignItems="center"
      justifyContent="space-between"
      padding="$4"
      height={40}
    >
      <XStack>
        <H5>19:10</H5>
      </XStack>
      <XStack alignItems="center" gap="$4">
        <XStack alignItems="center" gap="$2">
          <SimpleImage src={Disk} alt="" width={24} height={24} />
          <H5 textTransform="capitalize" fontWeight="$11">
            0 byte
          </H5>
        </XStack>
        <XStack alignItems="center" gap="$2">
          <SimpleImage src={Ram} alt="" width={24} height={24} />
          <H5 textTransform="capitalize" fontWeight="$11">
            556 zMB
          </H5>
        </XStack>
        <XStack alignItems="center" gap="$2">
          <SimpleImage src={Processor} alt="" width={24} height={246} />
          <H5 textTransform="capitalize" fontWeight="$11">
            1 GB
          </H5>
        </XStack>
      </XStack>
    </XStack>
  );
};
