/*
 *  File: /config/public.js
 *  Project: app
 *  File Created: 19-01-2024 06:33:59
 *  Author: Clay Risser
 *  -----
 *  BitSpur (c) Copyright 2024
 *
 *  Licensed under the GNU Affero General Public License (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://www.gnu.org/licenses/agpl-3.0.en.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *  You can be released from the requirements of the license by purchasing
 *  a commercial license. Buying such a license is mandatory as soon as you
 *  develop commercial activities involving this software without disclosing
 *  the source code of your own applications.
 */

module.exports = {
  API_BASE_URL: process.env.API_BASE_URL,
  CROSS_STORAGE_HUB_URL: process.env.CROSS_STORAGE_HUB_URL,
  DEBUG: process.env.DEBUG,
  KEYCLOAK_BASE_URL: process.env.KEYCLOAK_BASE_URL,
  KEYCLOAK_CLIENT_ID: process.env.KEYCLOAK_CLIENT_ID,
  KEYCLOAK_ENABLED: process.env.KEYCLOAK_ENABLED,
  KEYCLOAK_REALM: process.env.KEYCLOAK_REALM,
  SENTRY_DSN: process.env.SENTRY_DSN,
};

delete process.env.DEBUG;
