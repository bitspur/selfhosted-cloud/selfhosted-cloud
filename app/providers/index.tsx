/**
 * File: /providers/index.tsx
 * Project: app
 * File Created: 19-01-2024 06:33:59
 * Author: Clay Risser
 * -----
 * BitSpur (c) Copyright 2024
 *
 * Licensed under the GNU Affero General Public License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.gnu.org/licenses/agpl-3.0.en.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * You can be released from the requirements of the license by purchasing
 * a commercial license. Buying such a license is mandatory as soon as you
 * develop commercial activities involving this software without disclosing
 * the source code of your own applications.
 */

import React, { Suspense } from 'react';
import type { GlobalKeycloakProviderProps } from './keycloak';
import type { GlobalTamaguiProviderProps } from './tamagui';
import type { PropsWithChildren } from 'react';
import type { TamaguiInternalConfig } from 'ui';
import { GlobalApolloProvider } from './apollo';
import { GlobalKeycloakProvider } from './keycloak';
import { GlobalTamaguiProvider } from './tamagui';
import { GlobalNavigationProvider } from './navigation';

export type GlobalProviderKeycloak = Omit<GlobalKeycloakProviderProps, 'disabled' | 'children'>;

export type GlobalProviderProps = PropsWithChildren &
  Omit<GlobalTamaguiProviderProps, 'config'> & {
    keycloak?: GlobalProviderKeycloak;
    tamaguiConfig?: TamaguiInternalConfig;
  };

export function GlobalProvider({ children, keycloak, tamaguiConfig, ...props }: GlobalProviderProps) {
  return (
    <GlobalKeycloakProvider disabled={!keycloak} {...keycloak}>
      <GlobalApolloProvider>
        <GlobalTamaguiProvider config={tamaguiConfig} {...props}>
          <Suspense>
            <GlobalNavigationProvider>{children}</GlobalNavigationProvider>
          </Suspense>
        </GlobalTamaguiProvider>
      </GlobalApolloProvider>
    </GlobalKeycloakProvider>
  );
}

export * from './apollo/apollo';
export * from './keycloak';
export * from './navigation';
export * from './tamagui';
