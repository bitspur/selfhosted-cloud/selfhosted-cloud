/**
 * File: /providers/keycloak.tsx
 * Project: app
 * File Created: 19-01-2024 06:33:59
 * Author: Clay Risser
 * -----
 * BitSpur (c) Copyright 2024
 *
 * Licensed under the GNU Affero General Public License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.gnu.org/licenses/agpl-3.0.en.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * You can be released from the requirements of the license by purchasing
 * a commercial license. Buying such a license is mandatory as soon as you
 * develop commercial activities involving this software without disclosing
 * the source code of your own applications.
 */

import React from 'react';
import type { KeycloakProviderProps } from '@multiplatform.one/keycloak';
import { KeycloakProvider } from '@multiplatform.one/keycloak';
import { config } from 'app/config';

export interface GlobalKeycloakProviderProps extends KeycloakProviderProps {}

export function GlobalKeycloakProvider({ children, debug, ...props }: KeycloakProviderProps) {
  if (config.get('KEYCLOAK_ENABLED') !== '1') return <>{children}</>;
  return (
    <KeycloakProvider
      {...props}
      debug={
        false
        // typeof debug !== 'undefined' ? debug : config.get('DEBUG') === '1'
      }
    >
      {children}
    </KeycloakProvider>
  );
}
