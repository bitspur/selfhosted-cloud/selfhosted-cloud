FROM registry.gitlab.com/bitspur/rock8s/images/debian-node:18.18.2-bookworm AS builder

WORKDIR /tmp/app

COPY package.json .yarnrc.yml yarn.lock *.mk .env.default mkpm Mkpmfile mkpm.json ./
COPY .mkpm/cache.tar.gz .mkpm/cache.tar.gz
COPY .yarn .yarn
COPY api/package.json api/*.mk api/Mkpmfile ./api/
COPY app/package.json app/*.mk app/Mkpmfile ./app/
COPY platforms/expo/package.json platforms/expo/*.mk platforms/expo/Mkpmfile ./platforms/expo/
COPY platforms/next/package.json platforms/next/*.mk platforms/next/Mkpmfile ./platforms/next/
COPY ui/package.json ui/*.mk ui/Mkpmfile ./ui/
RUN git config --global init.defaultBranch main && git init && git add -A
RUN ./mkpm +deps
COPY . .
RUN ./mkpm api/+generate
RUN ./mkpm api/+build
RUN ./mkpm next/+build
RUN yarn workspaces focus app api ui '@platform/next' --production && \
    rm -rf .yarn .mkpm .git yarn.lock *.mk Mkpmfile default.env .yarnrc.yml \
    platforms/expo platforms/storybook platforms/storybook-native

FROM node:18.18.2-bookworm as app
WORKDIR /tmp/app
COPY --from=builder /tmp/app/.env ./.env
COPY --from=builder /tmp/app/node_modules ./node_modules
COPY --from=builder /tmp/app/package.json ./package.json
COPY --from=builder /tmp/app/app/config/*.js ./app/config/
COPY --from=builder /tmp/app/app/i18n/config.js ./app/i18n/config.js
COPY --from=builder /tmp/app/app/i18n/locales ./app/i18n/locales
COPY --from=builder /tmp/app/app/package.json ./app/package.json
COPY --from=builder /tmp/app/ui/package.json ./ui/package.json
COPY --from=builder /tmp/app/api/bin ./api/bin
COPY --from=builder /tmp/app/api/dist ./api/dist
COPY --from=builder /tmp/app/api/package.json ./api/package.json
COPY --from=builder /tmp/app/api/prisma/dist ./api/prisma/dist
COPY --from=builder /tmp/app/api/prisma/migrations ./api/prisma/migrations
COPY --from=builder /tmp/app/api/prisma/schema.prisma ./api/prisma/schema.prisma
COPY --from=builder /tmp/app/platforms/next/.env ./platforms/next/.env
COPY --from=builder /tmp/app/platforms/next/.next ./platforms/next/.next
COPY --from=builder /tmp/app/platforms/next/next-i18next.config.js ./platforms/next/next-i18next.config.js
COPY --from=builder /tmp/app/platforms/next/next.config.js ./platforms/next/next.config.js
COPY --from=builder /tmp/app/platforms/next/package.json ./platforms/next/package.json
COPY --from=builder /tmp/app/platforms/next/tamaguiModules.js ./platforms/next/tamaguiModules.js
COPY --from=builder /tmp/app/platforms/next/transpileModules.js ./platforms/next/transpileModules.js

FROM node:18.18.2-bookworm
WORKDIR /opt/app
COPY --from=app /tmp/app ./
COPY docker/entrypoint.sh /usr/local/bin/entrypoint
COPY docker/supervisord.conf /etc/supervisor/conf.d/supervisord.conf
COPY docker/nginx.conf /etc/nginx/conf.d/default.conf
RUN chmod +x /usr/local/bin/entrypoint && \
    mkdir -p /var/log/supervisor && \
    apt-get update && apt-get install -y \
    docker.io \
    nginx \
    postgresql \
    sqlite3 \
    supervisor && \
    rm -rf \
    /var/lib/apt/lists/* \
    /etc/nginx/sites-enabled/*
EXPOSE \
    3100 \
    5555 \
    80
ENV \
    BASE_URL=http://localhost \
    CONTAINER=1 \
    DATA_DIR=/var/data \
    DEBUG=0 \
    KEYCLOAK_ADMIN_PASSWORD= \
    KEYCLOAK_ADMIN_USERNAME=admin \
    KEYCLOAK_BASE_URL=http://keycloak:8080 \
    KEYCLOAK_CLIENT_ID= \
    KEYCLOAK_CLIENT_SECRET= \
    KEYCLOAK_ENABLED=1 \
    KEYCLOAK_REALM=master \
    KEYCLOAK_REGISTER=1 \
    LOG_FILE_NAME= \
    METRICS_PORT=3100 \
    NODE_ENV=production \
    POSTGRES_DATABASE=postgres \
    POSTGRES_HOSTNAME=postgres \
    POSTGRES_PASSWORD=postgres \
    POSTGRES_PORT=5432 \
    POSTGRES_USERNAME=postgres \
    PRISMA_DATABASE_ENGINE=sqlite \
    PRISMA_MIGRATE=1 \
    PRISMA_SEED=1 \
    PRISMA_STUDIO=1 \
    PRISMA_STUDIO_PORT=5555 \
    REDIS=0 \
    REDIS_HOST=redis \
    REDIS_PORT=6379
ENTRYPOINT ["/usr/local/bin/entrypoint"]
