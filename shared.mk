# File: /shared.mk
# Project: selfhosted-cloud
# File Created: 19-01-2024 06:33:59
# Author: Clay Risser
# -----
# BitSpur (c) Copyright 2024
#
# Licensed under the GNU Affero General Public License (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.gnu.org/licenses/agpl-3.0.en.html
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# You can be released from the requirements of the license by purchasing
# a commercial license. Buying such a license is mandatory as soon as you
# develop commercial activities involving this software without disclosing
# the source code of your own applications.

BABEL ?= $(YARN) babel
BABEL_NODE ?= $(YARN) babel-node
BROWSERSLIST_BINARY ?= $(YARN) browserslist
BUILD_STORYBOOK ?= $(YARN) build-storybook
CHANGESET ?= $(YARN) changeset
CLOC ?= cloc
CSPELL ?= $(YARN) cspell
ESLINT ?= $(YARN) eslint
EXPO ?= $(YARN) expo
GM ?= command gm
JEST ?= $(YARN) jest
LOKI ?= $(YARN) loki
NODE ?= node
PRETTIER ?= $(YARN) prettier
TSC ?= $(YARN) tsc
TSUP ?= $(YARN) tsup
WATCHMAN ?= watchman

export POSTGRES_URL ?= \
	postgresql://$(POSTGRES_PASSWORD):$(POSTGRES_USERNAME)@$(POSTGRES_HOSTNAME):$(POSTGRES_PORT)/$(POSTGRES_DATABASE)?sslmode=prefer

export TAMAGUI_IGNORE_BUNDLE_ERRORS := solito/image,solito/link
